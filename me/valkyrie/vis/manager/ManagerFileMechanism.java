package me.valkyrie.vis.manager;

/**
 * Created by Zeb on 7/12/2016.
 */
public interface ManagerFileMechanism {

    void save() throws Exception;

    void load() throws Exception;

}
