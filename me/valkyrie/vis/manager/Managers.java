package me.valkyrie.vis.manager;

import me.valkyrie.vis.manager.managers.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Zeb on 7/12/2016.
 */
public class Managers {

    private static ModuleManager moduleManager;
    private static ValueManager valueManager;
    private static InfoManager infoManager;
    private static CommandManager commandManager;
    private static FriendManager friendManager;
    private static PluginManager pluginManager;
    private static HudThemeManager hudThemeManager;

    public static void init(){
        pluginManager = new PluginManager();
        commandManager = new CommandManager();
        valueManager = new ValueManager();
        moduleManager = new ModuleManager();
        infoManager = new InfoManager();
        friendManager = new FriendManager();
        hudThemeManager = new HudThemeManager();
        Managers.getPluginManager().enable();
    }

    public static FriendManager getFriendManager() {
        return friendManager;
    }

    public static InfoManager getInfoManager() {
        return infoManager;
    }

    public static CommandManager getCommandManager() {
        return commandManager;
    }

    public static ValueManager getValueManager() {
        return valueManager;
    }

    public static ModuleManager getModuleManager() {
        return moduleManager;
    }

    public static PluginManager getPluginManager() {
        return pluginManager;
    }

    public static HudThemeManager getHudThemeManager() {
        return hudThemeManager;
    }
}
