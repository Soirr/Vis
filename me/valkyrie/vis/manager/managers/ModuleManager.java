package me.valkyrie.vis.manager.managers;

import me.valkyrie.vis.command.CommandBuilder;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventManager;
import me.valkyrie.vis.event.events.game.KeyPressEvent;
import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.modules.combat.*;
import me.valkyrie.vis.module.modules.movement.*;
import me.valkyrie.vis.module.modules.other.*;
import me.valkyrie.vis.module.modules.player.*;
import me.valkyrie.vis.module.modules.render.*;
import me.valkyrie.vis.module.modules.world.*;
import org.lwjgl.input.Keyboard;

/**
 * Created by Zeb on 7/12/2016.
 */
public class ModuleManager extends Manager<Module> {

    public ModuleManager(){
        addContent(new Module[]{
                new FullbrightMod(),
                new JesusMod(),
                new SprintMod(),
                new VelocityMod(),
                new NoRotateMod(),
                new AutoTuneMod(),
                new AirStrafeMod(),
                new ClickGuiMod(),
                new NotePlayerMod(),
                new BowAimbotMod(),
                new NoSlowdownMod(),
                new ChestStealerMod(),
                new InventoryMod(),
                new FlyMod(),
                new AutoCheatMod(),
                new KillauraMod(),
                new SneakMod(),
                new FastMineMod(),
                new SpeedMod(),
                new NametagsMod(),
                new CriticalsMod(),
                new BlinkMod(),
                new FastUseMod(),
                new AutoHealMod(),
                new RetardMod(),
                new EspMod(),
                new ZootMod(),
                new PhaseMod()
        });

        new CommandBuilder("Bind")
                .withAliases(new String[]{"b"})
                .withUsages(new String[]{"'module' 'key'"})
                .withCommandExecutor(arguments -> {
                    if(arguments.size() != 2){
                        return "invalid_usage";
                    }

                    String name = arguments.get(0);
                    String key = arguments.get(1);

                    Module module = getModule(name);

                    if(module == null){
                        return "&cModule not found.";
                    }

                    if(key.equalsIgnoreCase("none")){
                        module.getModuleInfo().setKey(Keyboard.KEY_NONE);
                    }else{
                        if(Keyboard.getKeyIndex(key.toUpperCase()) == 0){
                            return "&cKey not found.";
                        }else{
                            module.getModuleInfo().setKey(Keyboard.getKeyIndex(key.toUpperCase()));
                        }
                    }
                    Managers.getInfoManager().save();

                    return "&fSet key for &a" + module.getModuleInfo().getDisplayName() + " &fto &a" + key.toLowerCase() + "&f.";
                }).build();

        new CommandBuilder("Rename")
                .withAliases(new String[]{"rname"})
                .withUsages(new String[]{"'module' 'name'"})
                .withCommandExecutor(arguments -> {
                    if(arguments.size() != 2){
                        return "invalid_usage";
                    }

                    String name = arguments.get(0);
                    String key = arguments.get(1);

                    Module module = getModule(name);

                    if(module == null){
                        return "Module not found.";
                    }

                    String prevName = module.getModuleInfo().getDisplayName();

                    module.getModuleInfo().setDisplayName(key);

                    Managers.getInfoManager().save();

                    return "Set name for &a" + prevName + " &fto &a" + key.toLowerCase() + "&f.";
                }).build();

        new CommandBuilder("Visible")
                .withAliases(new String[]{"v"})
                .withUsages(new String[]{"'module' 'visible'"})
                .withCommandExecutor(arguments -> {
                    if(arguments.size() != 2){
                        return "invalid_usage";
                    }

                    String name = arguments.get(0);
                    String state = arguments.get(1);

                    Module module = getModule(name);

                    if(module == null){
                        return "Module not found.";
                    }


                    if(state.equalsIgnoreCase("true")){
                        module.getModuleInfo().setVisible(true);
                    }else if(state.equalsIgnoreCase("false")){
                        module.getModuleInfo().setVisible(false);
                    }else{
                        return "invalid_usage";
                    }

                    Managers.getInfoManager().save();

                    return "Set visibility for &a" + module.getModuleInfo().getDisplayName() + " &fto &a" + module.getModuleInfo().isVisible() + "&f.";
                }).build();

        new CommandBuilder("Toggle")
                .withAliases(new String[]{"t"})
                .withUsages(new String[]{"'module'"})
                .withCommandExecutor(arguments -> {
                    if(arguments.size() != 1){
                        return "invalid_usage";
                    }

                    String name = arguments.get(0);

                    Module module = getModule(name);

                    if(module == null){
                        return "Module not found.";
                    }

                    module.setState(!module.getState());
                    Managers.getInfoManager().save();

                    return "no_message";
                }).build();

        EventManager.subscribe(this);
    }

    public Module getModule(String name){
        for(Module module : getContent()){
            if(module.getModuleInfo().getName().equalsIgnoreCase(name)) return module;
        }

        for(Module module : getContent()){
            if(module.getModuleInfo().getDisplayName().equalsIgnoreCase(name)) return module;
        }
        return null;
    }

    @Override
    public void removeContent(Module... content) {
        for(Module t : content){
            getContent().remove(t);
        }

        for(Module t : content){
            t.setState(false);
        }
    }

    @EventHandler
    public void onKeyPress(KeyPressEvent event){
        for(Module module : getContent()){
            if(module.getModuleInfo().getKey() == event.getKey()){
                module.setState(!module.getState());
                Managers.getInfoManager().save();
            }
        }
    }

}
