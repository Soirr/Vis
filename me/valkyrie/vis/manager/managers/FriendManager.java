package me.valkyrie.vis.manager.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.valkyrie.vis.command.CommandBuilder;
import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.manager.ManagerFileMechanism;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.Friend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Created by Zeb on 7/27/2016.
 */
public class FriendManager extends Manager<Friend> {

    public FriendManager(){
        this.attachFileMechanism(new ManagerFileMechanism() {

            @Override
            public void save() throws Exception {
                PrintWriter exception = new PrintWriter(new FileWriter(getFile()));
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                String json = gson.toJson(Managers.getFriendManager());
                exception.println(json);
                exception.close();
            }

            @Override
            public void load() throws Exception {
                BufferedReader bufferedreader = new BufferedReader(new FileReader(getFile()));

                FriendManager manager = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedreader, FriendManager.class);

                for(Friend friend : manager.getContent()){
                    try {
                        Managers.getFriendManager().addContent(friend);
                    }catch (Exception e){}
                }

                bufferedreader.close();
            }

        }, "friends");

        if(Managers.getCommandManager().getCommand("Friend") == null) {
            new CommandBuilder("Friend")
                    .withAliases(new String[]{"fr"})
                    .withUsages(new String[]{"add 'name' 'nick'", "add 'name'", "remove 'name'"})
                    .withCommandExecutor(arguments -> {
                        if (arguments.size() != 2 && arguments.size() != 3) {
                            return "invalid_usage";
                        }

                        String mode = arguments.get(0);
                        String name = arguments.get(1);

                        String nick = arguments.size() == 3 ? arguments.get(2) : name;

                        if (mode.equalsIgnoreCase("add")) {
                            if (Managers.getFriendManager().getFriend(name) != null) {
                                return "Friend already exists.";
                            }

                            Managers.getFriendManager().addContent(new Friend(name, nick));
                            Managers.getFriendManager().save();
                            return "Added &a" + nick + "&f to your friends list.";
                        } else if (mode.equalsIgnoreCase("remove")) {
                            if (Managers.getFriendManager().getFriend(name) == null) {
                                return "Friend not found.";
                            }

                            Managers.getFriendManager().removeContent(getFriend(name));
                            Managers.getFriendManager().save();
                            return "Remove &a" + nick + "&f to your friends list.";
                        } else {
                            return "invalid_usage";
                        }
                    }).build();
        }
    }

    public Friend getFriend(String name){
        for(Friend friend : Managers.getFriendManager().getContent()){
            if(friend.getName().equalsIgnoreCase(name) || friend.getNick().equalsIgnoreCase(name)) return friend;
        }

        return null;
    }

}
