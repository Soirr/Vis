package me.valkyrie.vis.manager.managers;

import me.valkyrie.vis.gui.hud.HudTheme;
import me.valkyrie.vis.gui.hud.custom.CustomHudTheme;
import me.valkyrie.vis.gui.hud.direkt.DirektHudTheme;
import me.valkyrie.vis.gui.hud.light.LightHudTheme;
import me.valkyrie.vis.gui.hud.vis.VisHudTheme;
import me.valkyrie.vis.manager.Manager;

/**
 * Created by Zeb on 8/20/2016.
 */
public class HudThemeManager extends Manager<HudTheme> {

    public HudThemeManager() {
        addContent(new VisHudTheme(), new DirektHudTheme(), new CustomHudTheme(), new LightHudTheme());
    }
}
