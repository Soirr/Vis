package me.valkyrie.vis.manager.managers;

import com.google.gson.GsonBuilder;
import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.valkyrie.vis.event.EventMethodListener;
import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.plugin.Plugin;
import me.valkyrie.vis.plugin.PluginInfo;
import net.minecraft.client.Minecraft;

import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by Zeb on 8/8/2016.
 */
public class PluginManager extends Manager<Plugin> {

    private Map<Plugin, PluginInfo> pluginInfoMap = new HashMap();

    //Might rewrite this, just for compatibility.
    private Map<Plugin, Boolean> pluginStateMap = new HashMap();

    private File dir;

    public PluginManager(){
        File visDir = new File(Minecraft.getMinecraft().mcDataDir + "/Vis");

        if (!visDir.exists()) visDir.mkdir();

        dir = new File(visDir + "/plugins/");

        if(dir == null){
            return;
        }

        dir.mkdir();

        try{
            findPlugins();
        }catch (IOException e){
            e.printStackTrace();
        }

        getContent().sort((Plugin plugin1, Plugin plugin2) ->{
            return pluginInfoMap.get(plugin2).getLoadPriority().getPriorityValue() - pluginInfoMap.get(plugin1).getLoadPriority().getPriorityValue();
        });
    }

    public void findPlugins() throws IOException{
        for(File file : dir.listFiles()){
            if(file.getName().toLowerCase().endsWith(".jar")){
                JarFile jarFile = new JarFile(file);

                PluginInfo pluginInfo = getPluginInfo(jarFile);

                if(pluginInfo == null) return;
                System.out.println("Loaded plugin with plugin info : " + pluginInfo.toString() + ".");
                try {
                    pluginInfoMap.put(loadPlugin(file, pluginInfo), pluginInfo);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    public PluginInfo getPluginInfo(JarFile jarFile) throws IOException{

        JarEntry fileEntry = jarFile.getJarEntry("plugin.json");

        if(fileEntry == null) return null;

        InputStreamReader streamReader = new InputStreamReader(jarFile.getInputStream(fileEntry));
        BufferedReader reader = new BufferedReader(streamReader);

        PluginInfo pluginInfo = new GsonBuilder().setPrettyPrinting().create().fromJson(reader, PluginInfo.class);

        return pluginInfo;
    }

    public Plugin loadPlugin(File file, PluginInfo pluginInfo) throws Exception {
        ClassLoader classLoader = URLClassLoader.newInstance(new URL[] {file.toURL()});

        Plugin plugin = (Plugin) classLoader.loadClass(pluginInfo.getMainClass()).newInstance();
        addContent(plugin);
        return plugin;
    }

    public void enable(){
        getContent().stream().forEach(plugin -> {
            plugin.enable();
            pluginStateMap.put(plugin, true);
        });
    }

    public void disable(){
        getContent().stream().forEach(plugin -> {
            plugin.disable();
            pluginStateMap.put(plugin, false);
        });
    }

    public void enable(Plugin plugin){
        plugin.enable();
        pluginStateMap.put(plugin, true);
    }

    public void disable(Plugin plugin){
        plugin.disable();
        pluginStateMap.put(plugin, false);
    }

    public void reload(){
        disable();
        getContent().clear();

        pluginInfoMap.clear();
        pluginStateMap.clear();

        try{
            findPlugins();
        }catch (IOException e){
            e.printStackTrace();
        }

        enable();
    }

    public Plugin getPlugin(String name){
        for(Plugin plugin : pluginInfoMap.keySet()){
            if(pluginInfoMap.get(plugin).getName().equalsIgnoreCase(name)) return plugin;
        }

        return null;
    }

    public PluginInfo getPluginInfo(String name){
        for(Plugin plugin : pluginInfoMap.keySet()){
            if(pluginInfoMap.get(plugin).getName().equalsIgnoreCase(name)) return pluginInfoMap.get(plugin);
        }

        return null;
    }

    public Map<Plugin, PluginInfo> getPluginInfoMap() {
        return pluginInfoMap;
    }

    public Map<Plugin, Boolean> getPluginStateMap() {
        return pluginStateMap;
    }
}
