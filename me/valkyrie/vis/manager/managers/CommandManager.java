package me.valkyrie.vis.manager.managers;

import me.valkyrie.vis.command.Command;
import me.valkyrie.vis.command.CommandBuilder;
import me.valkyrie.vis.command.commands.ColorCommand;
import me.valkyrie.vis.command.commands.HudCommand;
import me.valkyrie.vis.command.commands.PluginCommand;
import me.valkyrie.vis.command.commands.PoisonCommand;
import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.util.chat.ChatBuilder;
import me.valkyrie.vis.util.chat.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Zeb on 7/27/2016.
 */
public class CommandManager extends Manager<Command> {

    public CommandManager(){
        addContent(new Command[]{
                new ColorCommand(),
                new PoisonCommand(),
                new HudCommand(),
                new PluginCommand()
        });
    }

    public void handle(String message){
        String[] parts = message.split("\\s+");

        if(parts.length == 0) return;

        Command command = null;

        for(Command cmd : getContent()){
            if(parts[0].equalsIgnoreCase(cmd.getName())){
                command = cmd;
            }

            for(String alias : cmd.getAliases()){
                if(parts[0].equalsIgnoreCase(alias)){
                    command = cmd;
                }
            }
        }

        if(command == null){
            new ChatBuilder().appendPrefix().appendText("Command not found").send();
            return;
        }

        String response = "";

        if(parts.length > 1){
            List<String> args = new ArrayList<String>(Arrays.asList(parts));
            args.remove(0);
            response = command.handle(args).replaceAll("&", "\247");
        }else{
            response = command.handle(new ArrayList<String>()).replaceAll("&", "\247");
        }

        if(response.equalsIgnoreCase("invalid_usage")){
            new ChatBuilder().appendPrefix().appendText("Invalid Usage. Usage : ").send();

            for(String usage : command.getUsages()){
                new ChatBuilder().appendText("  - ", ChatColor.GRAY).appendText(command.getName() + " " + usage).send();
            }

            return;
        }

        if(response.equalsIgnoreCase("no_message")) return;

        new ChatBuilder().appendPrefix().appendText(response).send();
    }

    public Command getCommand(String name){
        for(Command command : getContent()){
            if(command.getName().equalsIgnoreCase(name)) return command;
        }
        return null;
    }

}
