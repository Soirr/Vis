package me.valkyrie.vis.manager.managers;

import me.tojatta.api.value.Value;
import me.valkyrie.value.ValueFileHandler;
import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.manager.ManagerFileMechanism;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 7/23/2016.
 */
public class ValueManager extends Manager<Value> {

    private me.tojatta.api.value.ValueManager valueManager;
    private ValueFileHandler valueFileHandler;

    public ValueManager(){
        name = "values";
        valueManager = new me.tojatta.api.value.ValueManager();
        valueFileHandler = new ValueFileHandler(valueManager, getFile());

        attachFileMechanism(new ManagerFileMechanism() {

            @Override
            public void save() throws Exception {
                valueFileHandler.save();
            }

            @Override
            public void load() throws Exception {
                valueFileHandler.load();
            }

        }, name);
    }

    @Override
    public List<Value> getContent() {
        List<Value> values = new ArrayList();

        for(Value value : valueManager.getValues().keySet()){
            values.add(value);
        }

        return values;
    }

    public void register(Object object){
        valueManager.register(object);
    }

    public me.tojatta.api.value.ValueManager getBaseValueManager() {
        return valueManager;
    }

    public ValueFileHandler getValueFileHandler() {
        return valueFileHandler;
    }
}
