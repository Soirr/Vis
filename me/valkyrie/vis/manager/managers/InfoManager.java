package me.valkyrie.vis.manager.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.valkyrie.value.ValueFileHandler;
import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.manager.ManagerFileMechanism;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.ModuleMode;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Created by Zeb on 7/26/2016.
 */
public class InfoManager extends Manager<ModuleInfo> {

    public InfoManager(){
        attachFileMechanism(new ManagerFileMechanism() {

            @Override
            public void save() throws Exception {
                getContent().clear();

                PrintWriter exception = new PrintWriter(new FileWriter(getFile()));
                Gson gson = new GsonBuilder().setPrettyPrinting().create();

                for(Module module : Managers.getModuleManager().getContent()){
                    addContent(module.getModuleInfo());
                }

                String json = gson.toJson(Managers.getInfoManager());
                exception.println(json);
                exception.close();
                getContent().clear();
            }

            @Override
            public void load() throws Exception {
                BufferedReader bufferedreader = new BufferedReader(new FileReader(getFile()));

                InfoManager manager = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedreader, InfoManager.class);

                for(ModuleInfo moduleInfo : manager.getContent()){
                    try {
                        Module module = Managers.getModuleManager().getModule(moduleInfo.getName());

                        module.getModuleInfo().setKey(moduleInfo.getKey());
                        module.getModuleInfo().setColor(moduleInfo.getColor());
                        module.getModuleInfo().setDisplayName(moduleInfo.getDisplayName());
                        module.setState(moduleInfo.getState());
                        module.getModuleInfo().setVisible(moduleInfo.isVisible());

                        for(ModuleMode moduleMode : module.getModes()){
                            if(moduleMode.getName().equalsIgnoreCase(moduleInfo.getMode())){
                                module.setMode(moduleMode);
                            }
                        }
                    }catch (Exception e){}
                }

                bufferedreader.close();
            }

        }, "info");
    }

}
