package me.valkyrie.vis.manager;

import com.google.gson.annotations.Expose;
import net.minecraft.client.Minecraft;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 7/12/2016.
 */
public class Manager<T> {

    private List<T> content = new ArrayList();

    @Expose //In case you try to save the manager with Gson. ;)
    private ManagerFileMechanism fileMechanism = null;

    @Expose
    protected String name;


    public void addContent(T... content){
        for(T t : content){

            /*
                Note: Might turn the content parameter to a Collection from a standard array so instead of looping
                addAll(content) can be called.
             */

            this.content.add(t);
        }
    }

    public void removeContent(T... content){
        for(T t : content){
            this.content.remove(t);
        }
    }

    protected void attachFileMechanism(ManagerFileMechanism fileMechanism, String name){
        this.fileMechanism = fileMechanism;
        this.name = name;
    }

    public final void save(){
        if(this.fileMechanism != null){
            try {
                this.fileMechanism.save();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public final void load(){
        if(this.fileMechanism != null){
            try {
                this.fileMechanism.load();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public List<T> getContent() {
        return content;
    }

    public ManagerFileMechanism getFileMechanism() {
        return fileMechanism;
    }

    public File getFile(){
        try {
            File dir = new File(Minecraft.getMinecraft().mcDataDir + "/Vis");

            if (!dir.exists()) dir.mkdir();

            File file = new File(dir + "/" + name + ".json");

            if (!file.exists()) file.createNewFile();

            return file;
        }catch (Exception e){
            return null;
        }
    }

    public String getName() {
        return name;
    }
}
