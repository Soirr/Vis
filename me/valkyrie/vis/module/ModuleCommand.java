package me.valkyrie.vis.module;

import me.tojatta.api.value.Value;
import me.valkyrie.vis.command.Command;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.util.chat.ChatBuilder;
import me.valkyrie.vis.util.chat.ChatColor;

import java.util.List;

/**
 * Created by Zeb on 8/1/2016.
 */
public class ModuleCommand extends Command {

    private Module module;

    public ModuleCommand(Module module) {
        super(module.getModuleInfo().getName(), new String[]{"mode 'mode'", "mode list", "'mode' 'name' 'value'", "value 'name' 'value'", "values"});
        this.module = module;
    }

    @Override
    public String handle(List<String> arguments) {
        if (!(arguments.size() == 2 || arguments.size() == 1 || arguments.size() == 3)) return "invalid_usage";

        if (arguments.get(0).equalsIgnoreCase("mode") && arguments.size() == 2) {
            String mode = arguments.get(1);

            if (mode.equalsIgnoreCase("list")) {
                if (!module.doesUseModes()) return "This module doesn't use modes.";

                new ChatBuilder().appendPrefix().appendText("Modes :").send();

                for (ModuleMode moduleMode : module.getModes()) {
                    new ChatBuilder().appendText("  - ", ChatColor.GRAY).appendText(moduleMode.getName(), ChatColor.WHITE).send();
                }

                return "no_message";
            } else {

                if (!module.doesUseModes()) return "This module doesn't use modes.";

                for (ModuleMode moduleMode : module.getModes()) {
                    if (moduleMode.getName().equalsIgnoreCase(mode)) {
                        module.setMode(moduleMode);
                        return "Set mode to &a" + moduleMode.getName() + "&f.";
                    }
                }

                return "Mode not found.";
            }
        } else if (arguments.get(0).equalsIgnoreCase("values") && arguments.size() == 1) {
            if (!module.doesUseModes()) return "This module doesn't have any values.";

            new ChatBuilder().appendPrefix().appendText("Values :").send();

            for (Value value : module.getValues()) {
                new ChatBuilder().appendText("  - ", ChatColor.GRAY).appendText(value.getLabel(), ChatColor.WHITE).send();
            }

            for(ModuleMode moduleMode : module.getModes()) {
                for(Object object : moduleMode.getValues()){
                    Value value = (Value) object;
                    new ChatBuilder().appendText("  - ", ChatColor.GRAY).appendText(value.getLabel() + " in ", ChatColor.WHITE).appendText(moduleMode.getName(), ChatColor.GREEN).send();
                }
            }

            return "no_message";
        }else if(module.getMode(arguments.get(0)) != null && arguments.size() == 3){
            String arg = arguments.get(2);

            ModuleMode moduleMode = module.getMode(arguments.get(0));

            if (moduleMode.getValues().isEmpty()) return "This module has no values.";


            Value value = moduleMode.getValue(arguments.get(1));

            if (value == null) return "Value not found.";

            Object val = getValue(arg, value);

            if (val == null) return "invalid_usage";

            value.setValue(val);
            Managers.getValueManager().save();
            return "Set &a" + value.getLabel() + "&f to &a" + value.getValue().toString() + "&f.";
        } else if(arguments.size() == 2){
            String arg = arguments.get(1);
            if (module.getValues().isEmpty()) return "This module has no values.";


            Value value = module.getValue(arguments.get(0));

            if (value == null) return "Value not found.";

            Object val = getValue(arg, value);

            if (val == null) return "invalid_usage";

            value.setValue(val);
            Managers.getValueManager().save();
            return "Set &a" + value.getLabel() + "&f to &a" + value.getValue().toString() + "&f.";
        }

        return "invalid_usage";
    }

    public Object getValue(String v, Value value){
        switch (value.getValue().getClass().getSimpleName().toLowerCase()){
            case "boolean":
                if(v.equalsIgnoreCase("true")){
                    return true;
                }else if(v.equalsIgnoreCase("false")){
                    return false;
                }else{
                    return null;
                }
            case "double":
                try{
                    return Double.parseDouble(v);
                }catch (Exception e){
                    return null;
                }
            case "integer":
                try{
                    return Integer.parseInt(v);
                }catch (Exception e){
                    return null;
                }
            case "float":
                try{
                    return Float.parseFloat(v);
                }catch (Exception e){
                    return null;
                }
            case "string": return v;
            default: return null;
        }
    }

    @Override
    public String[] getAliases() {
        return new String[]{module.getModuleInfo().getDisplayName()};
    }
}
