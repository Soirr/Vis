package me.valkyrie.vis.module;

import me.tojatta.api.value.Value;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.manager.Managers;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Zeb on 7/12/2016.
 */
public class ModuleMode<T> {

    public Minecraft mc;
    public EntityPlayerSP p;

    private String name;
    protected T parent;

    private List<Value> values = new ArrayList();

    public ModuleMode(T parent, String name) {
        this.parent = parent;
        this.name = name;

        Managers.getValueManager().register(this);

        for(Value value : Managers.getValueManager().getContent()){
            if(value.getObject().getClass().getName().equals(getClass().getName())){
                values.add(value);
            }
        }
    }

    public void onEnable(){}

    public void onDisable(){}

    public String getName() {
        return name;
    }

    public T getParent() {
        return parent;
    }

    public List<Value> getValues() {
        return values;
    }

    public Value getValue(String label){
        for(Value value : values){
            if(value.getLabel().equalsIgnoreCase(label)) return value;
        }

        return null;
    }
}
