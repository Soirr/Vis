package me.valkyrie.vis.module;

/**
 * Created by Zeb on 7/20/2016.
 */
public enum RenderPosition {

    TOP_RIGHT,
    BOTTOM_RIGHT

}
