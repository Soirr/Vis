package me.valkyrie.vis.module;

import com.google.gson.annotations.Expose;

import java.awt.*;

/**
 * Created by Zeb on 7/12/2016.
 */
public class ModuleInfo {

    private String name;

    private String displayName;
    private Color color;
    private int key;

    private boolean visible = true;
    private boolean state = false;

    private String mode = "none";

    public ModuleInfo(String name, Color color, int key) {
        this.name = name;
        this.displayName = name;
        this.color = color;
        this.key = key;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Color getColor() {
        return color;
    }

    public int getKey() {
        return key;
    }

}
