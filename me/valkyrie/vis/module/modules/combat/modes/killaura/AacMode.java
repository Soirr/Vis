package me.valkyrie.vis.module.modules.combat.modes.killaura;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.combat.KillauraMod;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.MathUtil;
import me.valkyrie.vis.util.RotationUtil;
import me.valkyrie.vis.util.TimerUtil;
import me.valkyrie.vis.util.chat.ChatBuilder;
import me.valkyrie.vis.util.entity.EntityUtil;
import me.valkyrie.vis.util.entity.EntityValidator;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MathHelper;

/**
 * Created by Zeb on 8/3/2016.
 */
@Bypasses(anticheats = {AntiCheat.AAC})
public class AacMode extends ModuleMode<KillauraMod> {

    private boolean attack = false;

    private EntityLivingBase target;
    private int ticks = 0;

    private float lYaw = 0;
    private float lPitch = 0;

    public AacMode(KillauraMod parent){
        super(parent, "AAC");
    }

    @Override
    public void onEnable() {
        lYaw = p.rotationYaw;
        lPitch = p.rotationPitch;
    }

    @EventHandler
    public void onMotion(MotionEvent event) {
        if(event.getType() == EventType.PRE){
            target = EntityUtil.getClosestEntity(8, new EntityValidator() {
                @Override
                public boolean isValid(EntityLivingBase entity) {
                    return !entity.isInvisible() && entity.isAboveLand();
                }
            });

            if(target == null) return;


        }else{
            if(ticks > 0){
                ticks--;
                return;
            }

            if(ticks == 0 && target != null){
                p.swingItem();
                p.sendQueue.addToSendQueue(new C02PacketUseEntity(target, C02PacketUseEntity.Action.ATTACK));
                target = null;
                ticks = MathUtil.getRandomInRange(4, 10);
            }
        }
    }

    public static float getYawChangeToEntity(Entity entity, float yaw){
        Minecraft mc = Minecraft.getMinecraft();
        double deltaX = entity.posX - mc.thePlayer.posX;
        double deltaZ = entity.posZ - mc.thePlayer.posZ;
        double yawToEntity;
        if((deltaZ < 0.0D) && (deltaX < 0.0D)){
            yawToEntity = 90.0D + Math.toDegrees(Math.atan(deltaZ / deltaX));
        }else{
            if((deltaZ < 0.0D) && (deltaX > 0.0D)){
                yawToEntity = -90.0D
                        + Math.toDegrees(Math.atan(deltaZ / deltaX));
            }else{
                yawToEntity = Math.toDegrees(-Math.atan(deltaX / deltaZ));
            }
        }

        return MathHelper
                .wrapAngleTo180_float(-(yaw - (float) yawToEntity));
    }

    public static float getPitchChangeToEntity(Entity entity, float pitch){
        Minecraft mc = Minecraft.getMinecraft();
        double deltaX = entity.posX - mc.thePlayer.posX;
        double deltaZ = entity.posZ - mc.thePlayer.posZ;
        double deltaY = entity.posY - 1.6D + entity.getEyeHeight() - 0.24
                - mc.thePlayer.posY;
        double distanceXZ = MathHelper.sqrt_double(deltaX * deltaX + deltaZ
                * deltaZ);

        double pitchToEntity = -Math.toDegrees(Math.atan(deltaY / distanceXZ));

        return -MathHelper.wrapAngleTo180_float(pitch
                - (float) pitchToEntity);
    }

}
