package me.valkyrie.vis.module.modules.combat.modes.killaura;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.combat.KillauraMod;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.RotationUtil;
import me.valkyrie.vis.util.TimerUtil;
import me.valkyrie.vis.util.entity.EntityUtil;
import net.minecraft.entity.EntityLivingBase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 8/3/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class SwitchMode extends ModuleMode<KillauraMod> {

    private List<EntityLivingBase> entities = new ArrayList();
    private EntityLivingBase target;
    private TimerUtil time = new TimerUtil();

    public SwitchMode(KillauraMod parent){
        super(parent, "Switch");
    }

    @EventHandler
    public void onMotion(MotionEvent event) {
        if(event.getType() == EventType.PRE){
            if(entities.isEmpty()){
                entities = parent.targetCheckManager.sortEntities(EntityUtil.getLivingEntitiesInRange(parent.range, parent.entityValidator));
            }else{
                target = entities.get(0);
            }

            if(target == null) return;

            float[] rotations = RotationUtil.getRotations(target);

            event.setYaw(rotations[0]);
            event.setPitch(rotations[1]);
        }else{
            if(parent.canBlock() && parent.autoblock && EntityUtil.getClosestEntity(parent.block, parent.entityValidator) != null){
                mc.playerController.sendUseItem(p, mc.theWorld, p.getHeldItem());
            }

            long delay = (Math.round(((20d - (double) parent.speed) / 20d) * 1000));

            if(time.hasReached(delay) && target != null){
                parent.attack(target);
                entities.remove(target);
                target = null;
                time.reset();
            }
        }
    }

}
