package me.valkyrie.vis.module.modules.combat.modes.killaura;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.combat.KillauraMod;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.RotationUtil;
import me.valkyrie.vis.util.TimerUtil;
import me.valkyrie.vis.util.entity.EntityUtil;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.play.client.*;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

/**
 * Created by Zeb on 8/3/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class TickMode extends ModuleMode<KillauraMod> {

    private int ticks = 0;
    private EntityLivingBase target;
    private TimerUtil time = new TimerUtil();

    public TickMode(KillauraMod parent){
        super(parent, "Tick");
    }

    @EventHandler
    public void onMotion(MotionEvent event) {
        if(ticks > 0){
            ticks--;
            return;
        }

        if(event.getType() == EventType.PRE){
            target = EntityUtil.getClosestEntity(parent.range, parent.entityValidator);

            if(target == null) return;

            float[] rotations = RotationUtil.getRotations(target);

            event.setYaw(rotations[0]);
            event.setPitch(rotations[1]);
        }else{
            if(parent.canBlock() && parent.autoblock && EntityUtil.getClosestEntity(parent.block, parent.entityValidator) != null){
                mc.playerController.sendUseItem(p, mc.theWorld, p.getHeldItem());
            }

            if(ticks == 0 && target != null){
                attack(target);
                target = null;
                ticks = 10;
            }
        }
    }

    public void attack(EntityLivingBase entity){
        boolean blocking = p.isBlocking();

        if(blocking) p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(
                C07PacketPlayerDigging.Action.RELEASE_USE_ITEM,
                new BlockPos(0, 0, 0), EnumFacing.fromAngle(-255.0D)));

        p.swingItem();
        p.sendQueue.addToSendQueue(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));
        p.sendQueue.addToSendQueue(new C0APacketAnimation());
        p.sendQueue.addToSendQueue(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));


        if(blocking) p.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(
                new BlockPos(0, 0, 0), 255, p.inventory
                .getCurrentItem(), 0.0F, 0.0F, 0.0F));
    }

}
