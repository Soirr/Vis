package me.valkyrie.vis.module.modules.combat.modes.killaura;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.combat.KillauraMod;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.RotationUtil;
import me.valkyrie.vis.util.TimerUtil;
import me.valkyrie.vis.util.entity.EntityUtil;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * Created by Zeb on 8/3/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class SingleMode extends ModuleMode<KillauraMod> {

    private boolean attack = false;

    private EntityLivingBase target;
    private TimerUtil time = new TimerUtil();

    public SingleMode(KillauraMod parent){
        super(parent, "Single");
    }

    @EventHandler
    public void onMotion(MotionEvent event) {
        if(event.getType() == EventType.PRE){
            target = EntityUtil.getClosestEntity(parent.range, parent.entityValidator);

            if(target == null) return;

            float[] rotations = RotationUtil.getRotations(target);

            event.setYaw(rotations[0]);
            event.setPitch(rotations[1]);

        }else{
            long delay = (Math.round(((20d - (double) parent.speed) / 20d) * 1000));

            if(time.hasReached(delay) && target != null){
                parent.attack(target);
                target = null;
                time.reset();
            }
//
//            if(parent.canBlock() && parent.autoblock && EntityUtil.getClosestEntity(parent.block, parent.entityValidator) != null){
//                mc.playerController.sendUseItem(p, mc.theWorld, p.getHeldItem());
//            }
        }
    }

}
