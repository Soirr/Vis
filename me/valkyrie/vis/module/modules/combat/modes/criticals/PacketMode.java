package me.valkyrie.vis.module.modules.combat.modes.criticals;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.combat.CriticalsMod;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * Created by Zeb on 7/29/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class PacketMode extends ModuleMode<CriticalsMod> {

    public PacketMode(CriticalsMod parent) {
        super(parent, "Packet");
    }

    @EventHandler
    public void onEntityAttack(PacketEvent event){
        if(event.getType() == PacketEvent.EventPacketType.SEND && event.getPacket() instanceof C02PacketUseEntity && p.onGround){
            if(((C02PacketUseEntity)event.getPacket()).getAction() == C02PacketUseEntity.Action.ATTACK){
                p.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(p.posX, p.posY + 0.05D, p.posZ, false));
                p.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(p.posX, p.posY, p.posZ, false));
                p.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(p.posX, p.posY + 0.012511D, p.posZ, false));
                p.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(p.posX, p.posY, p.posZ, false));
            }
        }
    }
}
