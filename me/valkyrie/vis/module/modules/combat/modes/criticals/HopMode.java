package me.valkyrie.vis.module.modules.combat.modes.criticals;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.combat.CriticalsMod;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.entity.Entity;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.ChatComponentText;

/**
 * Created by Zeb on 8/2/2016.
 */
@Bypasses(anticheats = {AntiCheat.WATCHDOG})
public class HopMode extends ModuleMode<CriticalsMod> {

    public HopMode(CriticalsMod parent) {
        super(parent, "Hop");
    }

    @EventHandler
    public void onEntityAttack(PacketEvent event){
        if(event.getType() == PacketEvent.EventPacketType.SEND && event.getPacket() instanceof C02PacketUseEntity && p.onGround){
            if(((C02PacketUseEntity)event.getPacket()).getAction() == C02PacketUseEntity.Action.ATTACK){
                p.motionY = 0.35;
                p.onGround = false;
            }
        }
    }

}
