package me.valkyrie.vis.module.modules.combat;

import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.modules.combat.modes.criticals.HopMode;
import me.valkyrie.vis.module.modules.combat.modes.criticals.PacketMode;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/29/2016.
 */
public class CriticalsMod extends Module{

    public CriticalsMod(){
        super(new ModuleInfo("Criticals", new Color(215,33,126), Keyboard.KEY_NONE), Category.COMBAT);
        addMode(new PacketMode(this), new HopMode(this));
    }

}
