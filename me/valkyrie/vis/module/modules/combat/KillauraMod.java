package me.valkyrie.vis.module.modules.combat;

import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.modules.combat.killaura.TargetCheckManager;
import me.valkyrie.vis.module.modules.combat.modes.killaura.AacMode;
import me.valkyrie.vis.module.modules.combat.modes.killaura.SingleMode;
import me.valkyrie.vis.module.modules.combat.modes.killaura.SwitchMode;
import me.valkyrie.vis.module.modules.combat.modes.killaura.TickMode;
import me.valkyrie.vis.util.TimerUtil;
import me.valkyrie.vis.util.entity.EntityUtil;
import me.valkyrie.vis.util.RotationUtil;
import me.valkyrie.vis.util.entity.EntityValidator;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemSword;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/23/2016.
 */
public class KillauraMod extends Module {

    @NumberValue(label = "Range", minimum = "1", maximum = "8")
    public double range = 4.1;

    @NumberValue(label = "Blockrange", minimum = "1", maximum = "8")
    public double block = 8;

    @NumberValue(label = "Speed", minimum = "1", maximum = "20")
    public int speed = 18;

    @BooleanValue(label = "AutoBlock")
    public boolean autoblock = true;

    @BooleanValue(label = "SmartTarget")
    public boolean smarttarget = true;

    @NumberValue(label = "GroupSize", minimum = "2", maximum = "5")
    public int groupSize = 5;

    public EntityValidator entityValidator;
    public TargetCheckManager targetCheckManager;

    public KillauraMod(){
        super(new ModuleInfo("Killaura", new Color(200,40,40), Keyboard.KEY_NONE), Category.COMBAT);

        addMode(new SingleMode(this), new SwitchMode(this), new TickMode(this), new AacMode(this));

        targetCheckManager = new TargetCheckManager();
        entityValidator = (EntityLivingBase entity)  -> { return (!entity.isInvisible() && Managers.getFriendManager().getFriend(entity.getName()) == null); };
    }

    public void attack(EntityLivingBase entity){
        boolean blocking = p.isBlocking();

        if(blocking) p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(
                C07PacketPlayerDigging.Action.RELEASE_USE_ITEM,
                new BlockPos(0, 0, 0), EnumFacing.fromAngle(-255.0D)));

        p.swingItem();
        p.sendQueue.addToSendQueue(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));

        if(blocking) p.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(
                new BlockPos(0, 0, 0), 255, p.inventory
                .getCurrentItem(), 0.0F, 0.0F, 0.0F));
    }

    public boolean canBlock(){
        return (p.getHeldItem() != null && p.getHeldItem().getItem() != null && p.getHeldItem().getItem() instanceof ItemSword && !p.isBlocking());
    }

}
