package me.valkyrie.vis.module.modules.combat;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.MathUtil;
import me.valkyrie.vis.util.entity.PredictUtil;
import me.valkyrie.vis.util.RotationUtil;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemBow;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/20/2016.
 *
 * Credit to StlMissouri for the base bowaimbot.
 * https:github.com/harrygallagher4/Cobalt/blob/master/src/main/java/com/stlmissouri/cobalt/module/mods/BowAimbotMod.java
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class BowAimbotMod extends Module {

    public BowAimbotMod(){
        super(new ModuleInfo("BowAimbot", new Color(251,184,41), Keyboard.KEY_NONE), Category.COMBAT);
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        if(event.getType() == EventType.POST) return;

        if(p.getItemInUseDuration() == 0) return;

        if(p.getHeldItem() == null || !(p.getHeldItem().getItem() instanceof ItemBow)) return;


        int use = p.getItemInUseDuration();

        float progress = use / 20.0F;
        progress = (progress * progress + progress * 2.0F) / 3.0F;

        progress = MathHelper.clamp_float(progress, 0 ,1);

        double v = progress * 3.0F;
        //Static MC gravity
        double g = 0.05F;

        EntityLivingBase target = getClosestToCrosshair();

        if(target == null) return;



        float pitch = (float) -Math.toDegrees(getLaunchAngle(target, v, g));

        if(Double.isNaN(pitch))
            return;


        Vec3 pos = PredictUtil.predictPos(target, 10);

        double difX = pos.xCoord - mc.thePlayer.posX, difZ = pos.zCoord - mc.thePlayer.posZ;
        float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;

        event.setYaw(yaw);
        event.setPitch(pitch);
    }

    private EntityLivingBase getClosestToCrosshair(){
        float dist = Float.MAX_VALUE;
        EntityLivingBase target = null;

        for(Object object : mc.theWorld.loadedEntityList){
            if(!(object instanceof EntityLivingBase)) continue;

            EntityLivingBase entity = (EntityLivingBase) object;

            if(!(entity.isEntityAlive() && !entity.isInvisible() && entity != p && p.canEntityBeSeen(entity))) continue;

            float yaw = RotationUtil.getRotations(entity)[0];
            float pitch = RotationUtil.getRotations(entity)[1];

            float dif = (float) Math.sqrt(
                    MathUtil.getAngleDifference(yaw, p.rotationYaw) * MathUtil.getAngleDifference(yaw, p.rotationYaw) +
                    MathUtil.getAngleDifference(pitch, p.rotationPitch) * MathUtil.getAngleDifference(pitch, p.rotationPitch)
            );

            if(dif < dist){
                dist = dif;
                target = entity;
            }
        }

        return target;
    }

    /**
     * Gets launch angle required to hit a target with the specified velocity and gravity
     *
     * @param targetEntity Target entity
     * @param v            Projectile velocity
     * @param g            World gravity
     * @return
     */
    private float getLaunchAngle(EntityLivingBase targetEntity, double v, double g) {
        double yDif = ((targetEntity.posY + (targetEntity.getEyeHeight() / 2)) - (mc.thePlayer.posY + mc.thePlayer.getEyeHeight()));
        double xDif = (targetEntity.posX - mc.thePlayer.posX);
        double zDif = (targetEntity.posZ - mc.thePlayer.posZ);

        /**
         * Pythagorean theorem to merge x/z
         *           /|
         *          / |
         * xCoord  /  | zDif
         *        /   |
         *       /    |
         *      /_____|
         * (player) xDif
         */
        double xCoord = Math.sqrt((xDif * xDif) + (zDif * zDif));

        return theta(v, g, xCoord, yDif);
    }

    /**
     * Calculates launch angle to hit a specified point based on supplied parameters
     *
     * @param v Projectile velocity
     * @param g World gravity
     * @param x x-coordinate
     * @param y y-coordinate
     * @return angle of launch required to hit point x,y
     * <p/>
     * Whoa there! You just supplied us with a method to hit a 2D point, but Minecraft is a 3D game!
     * <p/>
     * Yeah. Unfortunately this is 100x easier to do than write a method to find the 3D point,
     * so we can just merge the x/z axis of Minecraft into one (using the pythagorean theorem).
     * Have a look at getLaunchAngle to see how that's done
     */
    private float theta(double v, double g, double x, double y) {
        double yv = 2 * y * (v * v);
        double gx = g * (x * x);
        double g2 = g * (gx + yv);
        double insqrt = (v * v * v * v) - g2;
        double sqrt = Math.sqrt(insqrt);

        double numerator = (v * v) + sqrt;
        double numerator2 = (v * v) - sqrt;

        double atan1 = Math.atan2(numerator, g * x);
        double atan2 = Math.atan2(numerator2, g * x);

        /**
         * Ever heard of a quadratic equation? We're gonna have to have two different results
         * here, duh! It's probably best to launch at the smaller angle because that will
         * decrease the total flight time, thus leaving less room for error. If you're just
         * trying to impress your friends you could probably fire it at the maximum angle, but
         * for the sake of simplicity, we'll use the smaller one here.
         */
        return (float) Math.min(atan1, atan2);
    }

}
