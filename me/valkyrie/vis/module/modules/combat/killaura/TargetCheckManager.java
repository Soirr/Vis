package me.valkyrie.vis.module.modules.combat.killaura;

import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.module.modules.combat.killaura.checks.ArmorCheck;
import me.valkyrie.vis.module.modules.combat.killaura.checks.DistanceCheck;
import me.valkyrie.vis.module.modules.combat.killaura.checks.HealthCheck;
import me.valkyrie.vis.module.modules.combat.killaura.checks.WeaponCheck;
import net.minecraft.entity.EntityLivingBase;
import org.lwjgl.Sys;

import java.util.*;

/**
 * Created by Zeb on 8/3/2016.
 */
public class TargetCheckManager extends Manager<TargetCheck> {

    public TargetCheckManager() {
        addContent(new ArmorCheck(), new DistanceCheck(), new WeaponCheck(), new HealthCheck());
    }

    private int getPriorityForEntity(EntityLivingBase entity){
        int priority = 0;

        for (TargetCheck targetCheck : getContent()) {
            priority += targetCheck.check(entity);
        }

        return priority;
    }

    public List<EntityLivingBase> sortEntities(List<EntityLivingBase> entities){

        Map<EntityLivingBase, Integer> priorityMap = new HashMap();

        entities.stream().forEach(entity -> {
            priorityMap.put(entity, getPriorityForEntity(entity));
        });

        entities.sort((EntityLivingBase e1, EntityLivingBase e2) ->{
            return priorityMap.get(e1) - priorityMap.get(e2);
        });

        return entities;
    }

}
