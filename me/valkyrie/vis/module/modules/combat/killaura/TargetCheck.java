package me.valkyrie.vis.module.modules.combat.killaura;

import net.minecraft.entity.EntityLivingBase;

/**
 * Created by Zeb on 7/23/2016.
 */
public interface TargetCheck {

    int check(EntityLivingBase entity);

}
