package me.valkyrie.vis.module.modules.combat.killaura.checks;

import me.valkyrie.vis.module.modules.combat.killaura.TargetCheck;
import net.minecraft.entity.EntityLivingBase;

/**
 * Created by Zeb on 8/3/2016.
 */
public class HealthCheck implements TargetCheck {

    private static double threshold = 0.2;

    @Override
    public int check(EntityLivingBase entity) {
        return (int) Math.round((entity.getMaxHealth() - entity.getHealth()) * threshold);
    }
}
