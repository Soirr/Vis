package me.valkyrie.vis.module.modules.combat.killaura.checks;

import me.valkyrie.vis.module.modules.combat.killaura.TargetCheck;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.EntityLivingBase;

/**
 * Created by Zeb on 7/23/2016.
 */
public class DistanceCheck implements TargetCheck {

    private static final double threshold = 1.75;


    @Override
    public int check(EntityLivingBase entity) {
        EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;
        return (int) Math.round(player.getDistanceToEntity(entity) * threshold);
    }
}
