package me.valkyrie.vis.module.modules.combat;

import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventHandlerPriority;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemSoup;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 8/17/2016.
 */
public class AutoHealMod extends Module {

    @NumberValue(label = "Modifier", minimum = "1", maximum = "20")
    private int health = 6;

    @BooleanValue(label = "Upwards")
    private boolean potUp = true;

    @BooleanValue(label = "Soups")
    private boolean soups = true;

    @BooleanValue(label = "Drop")
    private boolean drop = true;

    private boolean preform = false;

    private int slot = -1;
    private boolean soup = false;
    private int cooldownTicks = 0;

    public AutoHealMod(){
        super(new ModuleInfo("AutoHeal", new Color(70, 109, 193), Keyboard.KEY_V), Category.COMBAT);
    }

    @EventHandler(priority = EventHandlerPriority.HIGH)
    public void onMotion(MotionEvent event){
        if(cooldownTicks > 0){
            cooldownTicks--;
            return;
        }

        if(event.getType() == EventType.PRE){
            update();

            if(cooldownTicks == 0 && p.getHealth() <= health && slot != -1){
                event.breakEvent();
                if(potUp && p.onGround) p.jump();
                event.setPitch(potUp ? -90 : 90);
                preform = true;
            }
        }else{
            if(preform){
                if (slot < 9) {
                    p.sendQueue.addToSendQueue(new C09PacketHeldItemChange(slot));
                    p.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(p.inventory.getCurrentItem()));
                    p.sendQueue.addToSendQueue(new C09PacketHeldItemChange(p.inventory.currentItem));
                }else{
                    mc.playerController.windowClick(p.inventoryContainer.windowId, slot, 5, 2, p);
                    p.sendQueue.addToSendQueue(new C09PacketHeldItemChange(5));
                    p.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(p.inventory.getCurrentItem()));
                    if(drop) this.mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.DROP_ITEM, BlockPos.ORIGIN, EnumFacing.DOWN));
                    p.sendQueue.addToSendQueue(new C09PacketHeldItemChange(p.inventory.currentItem));
                }
                cooldownTicks = potUp ? 40 : 10;
                event.breakEvent();
                preform = false;
            }
        }
    }

    public void update(){
        int count = 0;

        slot = -1;

        for(int i = 0; i <= 35; i++){
            if (p.inventory.mainInventory[i] != null) {
                ItemStack stack = p.inventory.mainInventory[i];
                Item item = stack.getItem();
                if (item instanceof ItemPotion) {
                    ItemPotion potion = (ItemPotion)item;
                    if (potion.getEffects(stack) != null) {
                        for (Object o : potion.getEffects(stack)) {
                            PotionEffect effect = (PotionEffect)o;
                            if (effect.getPotionID() == Potion.heal.id && ItemPotion.isSplash(stack.getItemDamage())) {
                                slot = i;
                                count++;
                                soup = false;
                            }
                        }
                    }
                }else if(item instanceof ItemSoup && soups){
                    slot = i;
                    count++;
                    soup = true;
                }
            }
        }

        setSuffix(count + "");
    }

}
