package me.valkyrie.vis.module.modules.combat;

import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.note.parsers.NoteFileParser;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.RotationUtil;
import me.valkyrie.vis.util.entity.EntityUtil;
import me.valkyrie.vis.util.entity.EntityValidator;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import org.lwjgl.input.Keyboard;

import java.io.File;

import java.awt.*;

/**
 * Created by Zeb on 7/12/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class VelocityMod extends Module {

    @NumberValue(label = "Modifier", minimum = "0", maximum = "100")
    private int modifier = 0;

    @BooleanValue(label = "Vertical")
    private boolean vertical = true;

    public VelocityMod(){
        super(new ModuleInfo("Velocity", new Color(206,83,77), Keyboard.KEY_V), Category.COMBAT);
        setSuffix(modifier + ".0%"); // FUCK OFF OK IT LOOKS GOOD
    }

    @EventHandler
    public void onPacket(PacketEvent event){
        setSuffix(modifier + ".0%");
        if(p == null) return;

        if(event.getPacket() instanceof S12PacketEntityVelocity){
            S12PacketEntityVelocity packet = (S12PacketEntityVelocity) event.getPacket();

            if(packet.func_149412_c() != p.getEntityId()) return;

            event.setCancelled(true);

            packet.x *= ((double) modifier / 100D);
            if(vertical) packet.y *= ((double) modifier / 100D);
            packet.z *= ((double) modifier / 100D);

            //Kinda unnecessary only to stop fire from doing vertical shit.
            p.motionX += (packet.x / 8000d);
            p.motionY += (packet.y / 8000d);
            p.motionZ += (packet.z / 8000d);
        }
    }

}
