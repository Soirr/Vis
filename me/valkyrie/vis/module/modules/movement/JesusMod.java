package me.valkyrie.vis.module.modules.movement;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventHandlerPriority;
import me.valkyrie.vis.event.events.entity.CollisionEvent;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.block.BlockLiquid;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/12/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class JesusMod extends Module {

    public JesusMod(){
        super(new ModuleInfo("Jesus", new Color(132, 215, 231), Keyboard.KEY_NONE), Category.MOVEMENT);
    }

    @EventHandler
    public void onCollision(CollisionEvent event){
        if(p == null) return;

        if(!(event.getBlock() instanceof BlockLiquid)) return;
        if(p.inWater() || p.isSneaking() || p.fallDistance > 3 || event.getEntity() != p) return;

        BlockPos pos = event.getPos();
        event.setBoundingBox(new AxisAlignedBB(pos.getX(), pos.getY(), pos.getZ(), pos.getX() + 1, pos.getY() + 0.99, pos.getZ() + 1));
    }

    @EventHandler(priority = EventHandlerPriority.HIGH)
    public void onPacket(PacketEvent event){
        if(p == null || mc.theWorld == null) return;

        if(p.inWater() && !p.isSneaking() && !Keyboard.isKeyDown(mc.gameSettings.keyBindJump.getKeyCode())) p.motionY = 0.1;

        if(event.getPacket() instanceof C03PacketPlayer && !p.isInWater() && p.isAboveWater() && !p.isAboveLand()){
            C03PacketPlayer packet = (C03PacketPlayer) event.getPacket();

            packet.setY((p.ticksExisted % 2 == 0 ? 0.02 : 0) + p.posY);
        }
    }

}
