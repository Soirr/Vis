package me.valkyrie.vis.module.modules.movement;

import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.event.events.player.PlayerSlowdownEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.block.BlockSlime;
import net.minecraft.block.BlockSoulSand;
import net.minecraft.block.BlockWeb;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/20/2016.
 */
@Bypasses(anticheats = {AntiCheat.WATCHDOG})
public class FlyMod extends Module {

    @NumberValue(label = "Speed", minimum = "1", maximum = "10")
    public float speed = 4;

    public FlyMod(){
        super(new ModuleInfo("Fly", new Color(96, 154, 175), Keyboard.KEY_NONE), Category.MOVEMENT);
    }
    @EventHandler
    public void onMotion(MotionEvent event){
        float hSpeed = 0.2f * speed;
        float vSpeed = 0.1f * speed;


        p.motionY = 0;

        if(mc.gameSettings.keyBindSneak.getIsKeyPressed()){
            p.motionY = -vSpeed;
        }else if(mc.gameSettings.keyBindJump.getIsKeyPressed()){
            p.motionY = vSpeed;
        }

        if(p.moveForward != 0 || p.moveStrafing != 0) p.setSpeed(hSpeed);
    }

}
