package me.valkyrie.vis.module.modules.movement;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/12/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class SprintMod extends Module {

    public SprintMod(){
        super(new ModuleInfo("Sprint", new Color(195,255,104), Keyboard.KEY_NONE), Category.MOVEMENT);
    }

    @EventHandler
    public void onPlayerUpdate(PlayerUpdateEvent event){
        if(p.moveForward > 0 && p.getFoodStats().getFoodLevel() > 5 && !p.isSneaking()){
            p.setSprinting(true);
        }else{
            p.setSprinting(false);
        }
    }

}
