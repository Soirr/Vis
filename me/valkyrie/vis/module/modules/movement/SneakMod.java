package me.valkyrie.vis.module.modules.movement;

import me.valkyrie.vis.module.*;
import me.valkyrie.vis.module.modules.movement.modes.sneak.ManualMode;
import me.valkyrie.vis.module.modules.movement.modes.sneak.SilentMode;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 8/15/2016.
 */

public class SneakMod extends Module {

    public SneakMod(){
        super(new ModuleInfo("Sneak", new Color(66,255, 82), Keyboard.KEY_NONE), Category.MOVEMENT);

        addMode(new ManualMode(this), new SilentMode(this));
    }


}
