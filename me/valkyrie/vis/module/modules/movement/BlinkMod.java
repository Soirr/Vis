package me.valkyrie.vis.module.modules.movement;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventHandlerPriority;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;
import net.minecraft.util.Vec3;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.*;

/**
 * Created by Zeb on 8/10/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class BlinkMod extends Module {

    private Queue<C03PacketPlayer> queue = new LinkedList();

    public BlinkMod(){
        super(new ModuleInfo("Blink", new Color(245,197,238), Keyboard.KEY_NONE), Category.MOVEMENT);
    }

    @Override
    public void onDisable() {
        while (!queue.isEmpty()) p.sendQueue.addToSendQueue(queue.remove());
        setSuffix("");
    }

    @EventHandler(priority = EventHandlerPriority.LOW)
    public void onPacket(PacketEvent event){
        if(event.getPacket() instanceof C03PacketPlayer && (p.posX != p.lastTickPosX || p.posY != p.lastTickPosY || p.posZ != p.lastTickPosZ)){
            event.setCancelled(true);
            queue.offer((C03PacketPlayer) event.getPacket());
            setSuffix(queue.size() + "");
        }
    }

}
