package me.valkyrie.vis.module.modules.movement;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.event.events.player.PlayerSlowdownEvent;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.modules.movement.modes.phase.OldMode;
import me.valkyrie.vis.module.modules.movement.modes.phase.RelogMode;
import net.minecraft.block.BlockSlime;
import net.minecraft.block.BlockSoulSand;
import net.minecraft.block.BlockWeb;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/20/2016.
 */
public class PhaseMod extends Module {

    public PhaseMod(){
        super(new ModuleInfo("Phase", new Color(193, 146, 57), Keyboard.KEY_NONE), Category.MOVEMENT);
        addMode(new RelogMode(this), new OldMode(this));
    }

}
