package me.valkyrie.vis.module.modules.movement.modes.phase;

import io.netty.buffer.Unpooled;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.entity.CollisionEvent;
import me.valkyrie.vis.event.events.player.PlayerPushoutEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.movement.PhaseMod;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.client.gui.GuiDisconnected;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.network.NetHandlerLoginClient;
import net.minecraft.network.EnumConnectionState;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.handshake.client.C00Handshake;
import net.minecraft.network.login.client.C00PacketLoginStart;
import net.minecraft.network.play.client.C17PacketCustomPayload;
import net.minecraft.util.ChatComponentTranslation;
import sun.nio.ch.Net;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Zeb on 8/2/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class RelogMode extends ModuleMode<PhaseMod> {

    public RelogMode(PhaseMod parent){
        super(parent, "Relog");
    }

    @EventHandler
    public void onCollide(CollisionEvent event){
        if(p.isCollidedHorizontally
                && event.getEntity() == p
                && event.getBoundingBox() != null
                && event.getPos().getY() >= p.getEntityBoundingBox().minY) event.setBoundingBox(null);
    }

    @EventHandler
    public void onPlayerPushout(PlayerPushoutEvent event){
        event.setCancelled(true);
    }

    @Override
    public void onEnable(){
        double x = -(Math.sin(p.getDirection()) * 0.06);
        double z = (Math.cos(p.getDirection()) * 0.06);
        p.setPosition(p.posX + x, p.posY, p.posZ + z);
    }

}
