package me.valkyrie.vis.module.modules.movement.modes.phase;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.entity.CollisionEvent;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.event.events.player.PlayerPushoutEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.movement.PhaseMod;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * Created by Zeb on 8/2/2016.
 *
 * Credit to Soirr <3
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class OldMode extends ModuleMode<PhaseMod> {

    public OldMode(PhaseMod parent){
        super(parent, "Old");
    }

    @EventHandler
    public void onCollide(CollisionEvent event){
        if(p.isCollidedHorizontally
                && event.getEntity() == p
                && event.getBoundingBox() != null
                && event.getPos().getY() >= p.getEntityBoundingBox().minY) event.setBoundingBox(null);
    }

    @EventHandler
    public void onPushOut(PlayerPushoutEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        if(event.getType() == EventType.POST && p.isCollidedHorizontally && p.onGround){
            double multiplier = 0.3;

            double mx = Math.cos(Math.toRadians(mc.thePlayer.rotationYaw + 90.0F));
            double mz = Math.sin(Math.toRadians(mc.thePlayer.rotationYaw + 90.0F));

            double x = (mc.thePlayer.movementInput.moveForward * multiplier * mx + mc.thePlayer.movementInput.moveStrafe * multiplier * mz);
            double z = (mc.thePlayer.movementInput.moveForward * multiplier * mz - mc.thePlayer.movementInput.moveStrafe * multiplier * mx);
            p.sendQueue.addToSendQueue((new C03PacketPlayer.C04PacketPlayerPosition(p.posX + (x), p.posY, p.posZ + (z), false)));

            for(int i = 0; i < 10; i++){
                p.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(p.posX, Double.MAX_VALUE, p.posZ, false));
            }

            p.setPosition(p.posX + x, p.posY, p.posZ + z);
        }
    }

}
