package me.valkyrie.vis.module.modules.movement.modes.speed;

import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MoveEvent;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.movement.SpeedMod;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.block.Block;
import net.minecraft.potion.Potion;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;

import java.util.Iterator;

/**
 * Created by Zeb on 8/1/2016.
 *
 * Credit to Aris for the base bhop.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class JumpMode extends ModuleMode<SpeedMod> {

    @NumberValue(label = "Boost", minimum = "1.35", maximum = "16")
    private double boost = 4.5;

    public double moveSpeed = 0.2873;
    private int state = 0;
    private int ticksInAir = 0;
    private int groundTicks = 0;
    private double distance = 0;

    public JumpMode(SpeedMod parent){
        super(parent, "Jump");
    }

    @Override
    public void onEnable() {
        moveSpeed = getBaseMoveSpeed() * 0.65;
    }

    @EventHandler
    public void onUpdate(PlayerUpdateEvent event){
        if(event.getEventType() == EventType.PRE) {
            distance = Math.sqrt((p.posX - p.prevPosX) * (p.posX - p.prevPosX) + (p.posZ - p.prevPosZ) * (p.posZ - p.prevPosZ));

            if(p.moveStrafing != 0 || p.moveForward != 0) {

                if (isColliding(p.getEntityBoundingBox().clone().offset(0, -0.5, 0)) && p.motionY < -0){
                    ticksInAir++;
                }

                if (ticksInAir > 0 && ticksInAir <= 29 && isColliding(p.getEntityBoundingBox().clone().offset(0, -0.5, 0))) {
                    p.motionY = -0.005;
                }

                if (ticksInAir >= 2) {
                    p.motionX = 0;
                    p.motionZ = 0;
                }
            }
        }
    }

    @EventHandler
    public void onMove(MoveEvent event){
        if(mc.theWorld == null || p.isAboveWater() || p.inWater()){
            moveSpeed = getBaseMoveSpeed() * 0.65;
            return;
        }

        if(p.onGround){
            ticksInAir = 0;
            groundTicks++;
        }else{
            groundTicks = 0;
        }

        if(p.onGround && groundTicks < 6){
            event.x *= 0.3;
            event.z *= 0.3;
            return;
        }

        if(p.moveForward != 0 || p.moveStrafing != 0){
            switch (state) {
                case 1:
                    state = 2;
                    moveSpeed = (p.moveForward > 0 ? boost : boost / 1.4) * getBaseMoveSpeed();
                    break;
                case 2:
                    state = 3;
                    p.motionY = 0.424;
                    event.y = 0.424;
                    moveSpeed *= 2.149;
                    break;
                case 3:
                    state = 4;
                    moveSpeed = distance - (0.66) * (distance - getBaseMoveSpeed());
                    break;
                default:
                    if(!mc.theWorld.getCollidingBoundingBoxes(p, p.getEntityBoundingBox().offset(0.0, p.motionY, 0.0)).isEmpty()){
                        state = 1;
                    }

                    moveSpeed = (distance - distance / 159.0);
                    break;
            }

            moveSpeed = Math.max(moveSpeed, getBaseMoveSpeed());

            event.x = -(Math.sin(p.getDirection()) * moveSpeed);
            event.z = (Math.cos(p.getDirection()) * moveSpeed);
        }else{
            mc.timer.timerSpeed = 1f;
            moveSpeed = getBaseMoveSpeed();
        }
    }

    private double getBaseMoveSpeed() {
        return 0.2873 * 1 + (p.isPotionActive(Potion.moveSpeed) ? 0.2 * (p.getActivePotionEffect(Potion.moveSpeed).getAmplifier() + 1) : 0);
    }

    private boolean isColliding(AxisAlignedBB bb)
    {
        boolean colliding = false;
        Iterator iterator = this.mc.theWorld.getCollidingBoundingBoxes(this.p, bb).iterator();
        while (iterator.hasNext())
        {
            AxisAlignedBB boundingBox = (AxisAlignedBB)iterator.next();
            colliding = true;
        }
        return colliding;
    }

    public Block getBlock(AxisAlignedBB bb)
    {
        int y = (int)bb.minY;
        for (int x = MathHelper.floor_double(bb.minX); x < MathHelper.floor_double(bb.maxX) + 1; x++)
        {
            for (int z = MathHelper.floor_double(bb.minZ); z < MathHelper.floor_double(bb.maxZ) + 1; z++)
            {
                Block block = this.mc.theWorld.getBlockState(new BlockPos(x, y, z)).getBlock();
                if (block != null)
                {
                    return block;
                }
            }
        }
        return null;
    }

}
