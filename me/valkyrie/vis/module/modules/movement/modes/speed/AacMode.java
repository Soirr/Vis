package me.valkyrie.vis.module.modules.movement.modes.speed;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.movement.SpeedMod;
import me.valkyrie.vis.util.AntiCheat;

/**
 * Created by Zeb on 7/28/2016.
 */
@Bypasses(anticheats = {AntiCheat.AAC})
public class AacMode extends ModuleMode<SpeedMod> {

    public AacMode(SpeedMod parent){
        super(parent, "AAC");
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        double move = (Math.abs(p.moveForward) + Math.abs(p.moveStrafing));

        if(event.getType() == EventType.PRE && move != 0 && !p.inWater() && !p.isAboveWater()){
            if(p.onGround) {
                p.setSpeed(0.4f);
                p.motionY = 0.4;
            }else{
                p.setSpeed(0.35f);
            }
        }
    }

}
