package me.valkyrie.vis.module.modules.movement.modes.speed;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.event.events.player.MoveEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.movement.SpeedMod;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.chat.ChatBuilder;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockIce;
import net.minecraft.block.BlockPackedIce;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.Vec3;

import java.awt.*;
import java.net.URL;

/**
 * Created by Zeb on 7/28/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class OffsetMode extends ModuleMode<SpeedMod> {

    private int cooldown = 0;
    private boolean offset = false;

    public OffsetMode(SpeedMod parent){
        super(parent, "Offset");
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        double move = (Math.abs(p.moveForward) + Math.abs(p.moveStrafing));

        if(p.isCollidedHorizontally) return;

        float speed = offset ? p.getSpeed() * 3.4f : 0.35f;

        int scale = 3;

        speed /= scale;

        BlockPos pos = new BlockPos(p.posX - (Math.sin(p.getDirection()) * speed), p.posY - 1, p.posZ + (Math.cos(p.getDirection()) * speed));

        speed *= scale;

        if(mc.theWorld.getBlockState(pos).getBlock() instanceof BlockAir){
            cooldown = 20;
        }

        if(p.inWater() || p.isAboveWater()){
            cooldown = 10;
            return;
        }

        if(cooldown > 0){
            cooldown--;
            return;
        }

        if(!p.onGround){
            if(p.getSpeed() > 0.25) p.setSpeed(0.25f);
            cooldown = 6;
            offset = true;
        }

        if(event.getType() == EventType.PRE && move != 0 && p.onGround && !mc.gameSettings.keyBindJump.getIsKeyPressed()){
            if(!(mc.theWorld.getBlockState(p.getPosition().add(0,-1,0)).getBlock() instanceof BlockIce
                    || mc.theWorld.getBlockState(p.getPosition().add(0,-1,0)).getBlock() instanceof BlockPackedIce)) {
                p.setSpeed(speed);
            }else {
                p.setSpeed(0.5f);
            }

            if(!offset) {
                AxisAlignedBB boundingBox = p.getEntityBoundingBox().clone();
                AxisAlignedBB boundingBox2 = p.getEntityBoundingBox().clone();
                AxisAlignedBB boundingBox3 = p.getEntityBoundingBox().clone();

                boundingBox.offset(p.motionX, 0.4, p.motionZ);
                boundingBox2.offset(-(p.motionX * 1), 0.4, -(p.motionZ * 1));
                boundingBox3.offset(0, 0.4, 0);

                if(mc.theWorld.getCollidingBoundingBoxes(p, boundingBox).isEmpty()
                        && mc.theWorld.getCollidingBoundingBoxes(p, boundingBox2).isEmpty()
                        && mc.theWorld.getCollidingBoundingBoxes(p, boundingBox3).isEmpty()
                        && !p.isUnderBlock()
                        && !p.isUnderBlock(new Vec3(p.motionX, 0, p.motionZ))
                        && !p.isUnderBlock(new Vec3(-p.motionX, 0, -p.motionZ))){
                    event.setY(event.getY() + 0.4);
                }else{
                    event.setY(event.getY() + 0.2);
                }
            }

            offset = !offset;
        }
    }

}
