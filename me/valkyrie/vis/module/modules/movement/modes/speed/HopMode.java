package me.valkyrie.vis.module.modules.movement.modes.speed;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MoveEvent;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.movement.SpeedMod;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.potion.Potion;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MovementInput;

/**
 * Created by Zeb on 8/1/2016.
 *
 * Credit to Aris for the base bhop.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class HopMode extends ModuleMode<SpeedMod> {

    public double moveSpeed = 0.2873;
    int state = 0;
    int rest = 0;
    double distance = 0;

    public HopMode(SpeedMod parent){
        super(parent, "Hop");
    }

    @Override
    public void onEnable() {
        moveSpeed = getBaseMoveSpeed() * 0.65;
    }

    @EventHandler
    public void onUpdate(PlayerUpdateEvent event){
        if(event.getEventType() == EventType.PRE && rest == 0)
            distance = Math.sqrt((p.posX - p.prevPosX)*(p.posX - p.prevPosX) + (p.posZ - p.prevPosZ)*(p.posZ - p.prevPosZ));
    }

    @EventHandler
    public void onMove(MoveEvent event){
        if(mc.theWorld == null || p.isAboveWater() || p.inWater()){
            moveSpeed = getBaseMoveSpeed() * 0.65;
            return;
        }

        if(p.moveForward != 0 || p.moveStrafing != 0){
            switch (state) {
                case 1:
                    state = 2;
                    if(!p.onGround) {
                        moveSpeed = 1.35 * getBaseMoveSpeed();
                    }else{
                        state = 3;
                        p.motionY = 0.4;
                        event.y = 0.4;
                        moveSpeed *= 2.149;
                    }
                    break;
                case 2:
                    state = 3;
                    p.motionY = 0.4;
                    event.y = 0.4;
                    moveSpeed *= 2.149;
                    break;
                case 3:
                    state = 4;
                    moveSpeed = distance - (0.66) * (distance - getBaseMoveSpeed());
                    break;
                default:
                    if(!mc.theWorld.getCollidingBoundingBoxes(p, p.getEntityBoundingBox().offset(0.0, p.motionY, 0.0)).isEmpty()){
                        state = 1;
                    }

                    moveSpeed = (distance - distance / 159.0);
                    break;
            }

            moveSpeed = Math.max(moveSpeed, getBaseMoveSpeed());

            event.x = -(Math.sin(p.getDirection()) * moveSpeed);
            event.z = (Math.cos(p.getDirection()) * moveSpeed);
        }else{
            mc.timer.timerSpeed = 1f;
            moveSpeed = getBaseMoveSpeed();
        }
    }

    private double getBaseMoveSpeed() {
        return 0.2873 * 1 + (p.isPotionActive(Potion.moveSpeed) ? 0.2 * (p.getActivePotionEffect(Potion.moveSpeed).getAmplifier() + 1) : 0);
    }

}
