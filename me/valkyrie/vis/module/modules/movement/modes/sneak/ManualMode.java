package me.valkyrie.vis.module.modules.movement.modes.sneak;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.movement.SneakMod;
import me.valkyrie.vis.util.AntiCheat;
import org.lwjgl.input.Keyboard;

/**
 * Created by Zeb on 8/15/2016.
 */

@Bypasses(anticheats = {AntiCheat.ALL})
public class ManualMode extends ModuleMode<SneakMod> {

    public ManualMode(SneakMod parent) {
        super(parent, "Manual");
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        mc.gameSettings.keyBindSneak.pressed = true;
    }

    @Override
    public void onDisable() {
        mc.gameSettings.keyBindSneak.pressed = Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.getKeyCode());
    }
}
