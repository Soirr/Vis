package me.valkyrie.vis.module.modules.movement.modes.sneak;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.movement.SneakMod;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import org.lwjgl.input.Keyboard;

/**
 * Created by Zeb on 8/15/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class SilentMode extends ModuleMode<SneakMod> {

    public SilentMode(SneakMod parent) {
        super(parent, "Silent");
    }

    @Override
    public void onDisable() {
        if(!Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.getKeyCode())) p.sendQueue.getNetworkManager().sendPacket(new C0BPacketEntityAction(p, C0BPacketEntityAction.Action.STOP_SNEAKING));
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        p.sendQueue.getNetworkManager().sendPacket(new C0BPacketEntityAction(p, event.getType() == EventType.PRE ? C0BPacketEntityAction.Action.STOP_SNEAKING : C0BPacketEntityAction.Action.START_SNEAKING));
    }

}
