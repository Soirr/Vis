package me.valkyrie.vis.module.modules.movement;

import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.modules.movement.modes.speed.*;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/28/2016.
 */
public class SpeedMod extends Module {

    public SpeedMod(){
        super(new ModuleInfo("Speed", new Color(74,195,208), Keyboard.KEY_NONE), Category.MOVEMENT);
        addMode(new OffsetMode(this), new HopMode(this), new JumpMode(this), new LongJumpMode(this), new AacMode(this));
    }

}
