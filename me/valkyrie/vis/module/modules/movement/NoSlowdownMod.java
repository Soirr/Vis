package me.valkyrie.vis.module.modules.movement;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.event.events.player.PlayerSlowdownEvent;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.block.BlockSlime;
import net.minecraft.block.BlockSoulSand;
import net.minecraft.block.BlockWeb;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.NVComputeProgram5;

import java.awt.*;

/**
 * Created by Zeb on 7/20/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class NoSlowdownMod extends Module {

    public NoSlowdownMod(){
        super(new ModuleInfo("NoSlowdown", new Color(175,112,125), Keyboard.KEY_NONE), Category.MOVEMENT);
    }

    @EventHandler
    public void onSlowdown(PlayerSlowdownEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        if((p.moveForward != 0 || p.moveStrafing != 0) &&
                (mc.theWorld.getBlockState(p.getPosition().add(0,-1,0)).getBlock() instanceof BlockSoulSand ||
                        mc.theWorld.getBlockState(p.getPosition().add(0,-1,0)).getBlock() instanceof BlockSlime ||
                        mc.theWorld.getBlockState(p.getPosition()).getBlock() instanceof BlockWeb))
            p.setSpeed(((0.167f) * (mc.theWorld.getBlockState(p.getPosition()).getBlock() instanceof BlockWeb ? 1.8f : 1)) / (p.isSneaking() ? 1.6f : 1));

        if(p.isBlocking()){
            if(event.getType() == EventType.PRE){
                p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM,new BlockPos(0, 0, 0), EnumFacing.fromAngle(-255.0D)));
            }else{
                p.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(new BlockPos(0, 0, 0), 255, p.inventory.getCurrentItem(), 0.0F, 0.0F, 0.0F));
            }
        }
    }

}
