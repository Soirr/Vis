package me.valkyrie.vis.module.modules.movement;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/13/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class AirStrafeMod extends Module {

    public AirStrafeMod(){
        super(new ModuleInfo("AirStrafe", new Color(51,205,75), Keyboard.KEY_NONE), Category.MOVEMENT);
    }

    @EventHandler
    public void onUpdate(PlayerUpdateEvent event){
        if(!p.onGround){
            if(p.moveForward != 0 || p.moveStrafing != 0){
               p.setSpeed(p.getSpeed());
            }else{
                p.setSpeed(0);
            }
        }
    }

}
