package me.valkyrie.vis.module.modules.render;

import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.modules.render.modes.esp.ChamsMode;
import me.valkyrie.vis.module.modules.render.modes.esp.FillMode;
import me.valkyrie.vis.module.modules.render.modes.esp.OutlineMode;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 8/5/2016.
 */
public class EspMod extends Module {

    @NumberValue(label = "Red", minimum = "0", maximum = "255")
    public int red = 31;

    @NumberValue(label = "Green", minimum = "0", maximum = "255")
    public int green = 108;

    @NumberValue(label = "Blue", minimum = "0", maximum = "255")
    public int blue = 156;

    @BooleanValue(label = "Mobs")
    public boolean mobs = false;

    @BooleanValue(label = "Players")
    public boolean players = true;

    public EspMod(){
        super(new ModuleInfo("Esp", new Color(101, 133, 213), Keyboard.KEY_NONE), Category.RENDER);
        addMode(new OutlineMode(this), new FillMode(this), new ChamsMode(this));
    }

}
