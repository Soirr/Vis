package me.valkyrie.vis.module.modules.render;

import com.sun.javafx.geom.Vec3f;
import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.render.NametagRenderEvent;
import me.valkyrie.vis.event.events.render.RenderWorldEvent;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.MathUtil;
import me.valkyrie.vis.util.render.RenderUtil;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 7/28/2016.
 */
public class NametagsMod extends Module {

    @BooleanValue(label = "Health")
    private boolean health = true;

    @BooleanValue(label = "Armor")
    private boolean armor = true;

    @BooleanValue(label = "ZoomOnly")
    private boolean zoomOnly = false;

    public NametagsMod(){
        super(new ModuleInfo("Nametags", new Color(255,252,127), Keyboard.KEY_NONE), Category.RENDER);
    }

    @EventHandler
    public void onTagRender(NametagRenderEvent event){
        if(event.getEntity() instanceof EntityOtherPlayerMP) event.setCancelled(true);
    }

    @EventHandler
    public void onWorldRender(RenderWorldEvent event){
        GlStateManager.pushMatrix();
        for(Object o : mc.getMinecraft().theWorld.loadedEntityList){
            if(o instanceof EntityOtherPlayerMP){
                EntityOtherPlayerMP ep = (EntityOtherPlayerMP) o;

                double x = ep.lastTickPosX + (ep.posX - ep.lastTickPosX)
                        * this.mc.timer.renderPartialTicks
                        - this.mc.getRenderManager().renderPosX;
                double y = ep.lastTickPosY + (ep.posY - ep.lastTickPosY)
                        * this.mc.timer.renderPartialTicks
                        - this.mc.getRenderManager().renderPosY;
                double z = ep.lastTickPosZ + (ep.posZ - ep.lastTickPosZ)
                        * this.mc.timer.renderPartialTicks
                        - this.mc.getRenderManager().renderPosZ;

                drawNametag(ep, new Vec3f((float) x,(float) y,(float) z));
            }
        }
        GlStateManager.popMatrix();
    }

    public void drawNametag(EntityPlayer entity, Vec3f pos){
        FontRenderer font = mc.fontRendererObj;

        String name = Managers.getFriendManager().getFriend(entity.getName()) != null ? "\2473" + Managers.getFriendManager().getFriend(entity.getName()).getNick() : entity.getName();

        String healthTag = "\247f" + MathUtil.roundOff(entity.getHealth()/2, 1) + "\2474❤";

        RenderManager renderManager = this.mc.getRenderManager();
        float scale = (float) MathHelper.clamp_double(p.getDistanceToEntity(entity) / 4, 1, 1000000000);
        scale /= 50.0F;

        scale *= 0.8;

        GL11.glPushMatrix();
        GL11.glTranslatef(pos.x, pos.y + (entity.isSneaking() ? 1.5f : 1.8f) + 0.2f, pos.z);
        GL11.glNormal3f(0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
        GL11.glScalef(-scale, -scale, scale);
        GL11.glDisable(2896);
        GL11.glDisable(2929);
        GL11.glEnable(3042);
        GL11.glBlendFunc(770, 771);

        if(health) name += " " + healthTag;

        int width = (int) (font.getStringWidth(name)) / 2;

        Gui.drawRect(-width -2,
                -font.FONT_HEIGHT - 2, width + 2, 2, new Color(10, 10, 10,150).getRGB());
        font.drawStringWithShadow(name,
                -width,
                -(font.FONT_HEIGHT) + 0.5f, Color.WHITE.getRGB());

        if(armor && (zoomOnly ? mc.gameSettings.ofKeyBindZoom.getIsKeyPressed() : true)) {
            GlStateManager.translate(0, -5, 0);
            renderArmor(entity, 0, (-(this.mc.fontRendererObj.FONT_HEIGHT + 1) - 14));
            GlStateManager.translate(0, 5, 0);
        }

        GL11.glEnable(2929);
        GL11.glDisable(3042);
        GL11.glPopMatrix();
    }

    public void renderArmor(EntityPlayer player, int x, int y) {
        ItemStack[] items = player.getInventory();
        ItemStack inHand = player.getCurrentEquippedItem();
        ItemStack boots = items[0];
        ItemStack leggings = items[1];
        ItemStack body = items[2];
        ItemStack helm = items[3];

        ItemStack[] stuff = null;

        if(inHand != null){
            stuff = new ItemStack[]{inHand, helm,body,leggings,boots};
        }else{
            stuff = new ItemStack[]{helm,body,leggings,boots};
        }

        List<ItemStack> stacks = new ArrayList();

        for(ItemStack i : stuff){
            if(i != null && i.getItem() != null){
                stacks.add(i);
            }
        }

        int width = (16 * stacks.size())/2;

        x -= width;

        GL11.glEnable(GL11.GL_POLYGON_OFFSET_FILL);
        GL11.glPolygonOffset(1, -2000000);
        GlStateManager.disableLighting();

        for(ItemStack stack : stacks){
            renderItem(stack, x, y);

            x += 16;
        }

        GlStateManager.enableLighting();
        GL11.glPolygonOffset(1, 2000000);
        GL11.glDisable(GL11.GL_POLYGON_OFFSET_FILL);
    }

    public void renderItem(ItemStack stack, int x, int y){
        EnchantEntry[] enchants = new EnchantEntry[]{
                new EnchantEntry(Enchantment.field_180310_c, "P"),
                new EnchantEntry(Enchantment.thorns, "Th"),
                new EnchantEntry(Enchantment.field_180314_l, "Sh"),
                new EnchantEntry(Enchantment.fireAspect, "Fr"),
                new EnchantEntry(Enchantment.field_180313_o, "Kb"),
                new EnchantEntry(Enchantment.unbreaking, "Un")
        };

        GlStateManager.pushMatrix();

        RenderHelper.enableGUIStandardItemLighting();
        mc.getRenderItem().zLevel = -100;
        mc.getRenderItem().renderItemAboveHead(stack, x, y);
        mc.getRenderItem().renderItemOverlayIntoGUI(mc.fontRendererObj, stack, x, y, (String) null);

        for(EnchantEntry enchant : enchants){
            int level = EnchantmentHelper.getEnchantmentLevel(enchant.getEnchant().effectId, stack);

            if(level > 0){
                float scale = 0.4f;
                GlStateManager.translate(x, y, 0);
                GlStateManager.scale(scale, scale, scale);
                GlStateManager.disableLighting();
                mc.fontRendererObj.drawString("\247f" + enchant.getName() + " \247a" + level, 1, 1, Color.WHITE.getRGB());
                GlStateManager.scale(1/scale, 1/scale, 1/scale);
                GlStateManager.translate(-x, -y, 0);
                y += (mc.fontRendererObj.FONT_HEIGHT + 1) * scale;
            }
        }

        mc.getRenderItem().zLevel = 0;

        RenderHelper.disableStandardItemLighting();
        GlStateManager.enableAlpha();
        GlStateManager.disableBlend();
        GlStateManager.disableLighting();

        GlStateManager.popMatrix();
    }

    public static class EnchantEntry {

        private Enchantment enchant;
        private String name;

        public EnchantEntry(Enchantment enchant, String name) {
            this.enchant = enchant;
            this.name = name;
        }

        public Enchantment getEnchant() {
            return enchant;
        }
        public String getName() {
            return name;
        }

    }

}
