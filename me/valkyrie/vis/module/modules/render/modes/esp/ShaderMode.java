package me.valkyrie.vis.module.modules.render.modes.esp;

import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.render.EspMod;

/**
 * Created by Zeb on 8/15/2016.
 */
public class ShaderMode extends ModuleMode<EspMod> {

    public ShaderMode(EspMod parent) {
        super(parent, "Shader");
    }



}
