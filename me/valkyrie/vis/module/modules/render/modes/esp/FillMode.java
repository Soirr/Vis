package me.valkyrie.vis.module.modules.render.modes.esp;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventHandlerPriority;
import me.valkyrie.vis.event.events.render.RenderEntityEvent;
import me.valkyrie.vis.event.events.render.RenderWorldEvent;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.render.EspMod;
import me.valkyrie.vis.util.render.MCStencil;
import me.valkyrie.vis.util.render.RenderUtil;
import me.valkyrie.vis.util.render.Stencil;
import me.valkyrie.vis.util.render.shader.Shader;
import me.valkyrie.vis.util.render.shader.ShaderType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Created by Zeb on 8/14/2016.
 */
public class FillMode extends ModuleMode<EspMod> {

    private Shader shader;

    public FillMode(EspMod parent) {
        super(parent, "Fill");

        shader = new Shader("Fill", ShaderType.FRAGMENT);
    }

    @EventHandler(priority = EventHandlerPriority.HIGH)
    public void onRenderWorld(RenderWorldEvent event){
        mc = Minecraft.getMinecraft();
        shader.bind();
        GlStateManager.disableDepth();

        for (Object obj : mc.theWorld.loadedEntityList) {
            Entity entity = (Entity) obj;

            if (entity != mc.thePlayer && (!parent.mobs ? entity instanceof EntityPlayer : true)) {
                GL11.glPushMatrix();

                double posX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * mc.timer.renderPartialTicks;
                double posY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * mc.timer.renderPartialTicks;
                double posZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * mc.timer.renderPartialTicks;

                GL11.glTranslated(posX - RenderManager.renderPosX, posY - RenderManager.renderPosY, posZ - RenderManager.renderPosZ);
                Render entityRender = this.mc.getRenderManager().getEntityRenderObject(entity);
                if (entityRender != null) {
                    float distance = this.mc.thePlayer.getDistanceToEntity(entity);
                    if (entity instanceof EntityOtherPlayerMP) {
                        RenderPlayer.extraLayers = false;
                        RendererLivingEntity.renderLayers = false;
                        entityRender.doRender(entity, 0.0, 0.0, 0.0, 0.0f, mc.timer.renderPartialTicks);
                        RendererLivingEntity.renderLayers = true;
                        RenderPlayer.extraLayers = true;
                    }
                }
                GL11.glPopMatrix();
            }
        }

        GlStateManager.enableDepth();
        shader.unbind();

        for (Object obj : mc.theWorld.loadedEntityList) {
            Entity entity = (Entity) obj;

            if (entity != mc.thePlayer && (!parent.mobs ? entity instanceof EntityPlayer : true)) {
                GL11.glPushMatrix();

                double posX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * mc.timer.renderPartialTicks;
                double posY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * mc.timer.renderPartialTicks;
                double posZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * mc.timer.renderPartialTicks;

                GL11.glTranslated(posX - RenderManager.renderPosX, posY - RenderManager.renderPosY, posZ - RenderManager.renderPosZ);
                Render entityRender = this.mc.getRenderManager().getEntityRenderObject(entity);
                if (entityRender != null) {
                    float distance = this.mc.thePlayer.getDistanceToEntity(entity);
                    if (entity instanceof EntityOtherPlayerMP) {
                        entityRender.doRender(entity, 0.0, 0.0, 0.0, 0.0f, mc.timer.renderPartialTicks);
                    }
                }
                GL11.glPopMatrix();
            }
        }

        GlStateManager.disableLighting();
    }

}
