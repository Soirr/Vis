package me.valkyrie.vis.module.modules.render.modes.esp;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventHandlerPriority;
import me.valkyrie.vis.event.events.render.RenderWorldEvent;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.render.EspMod;
import me.valkyrie.vis.util.render.MCStencil;
import me.valkyrie.vis.util.render.RenderUtil;
import me.valkyrie.vis.util.render.Stencil;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Created by Zeb on 8/12/2016.
 *
 * Not sure where this came from, I took it from soirr and trevor.
 */
public class OutlineMode extends ModuleMode<EspMod> {

    public OutlineMode(EspMod parent) {
        super(parent, "Outline");
    }

    @EventHandler(priority = EventHandlerPriority.HIGH)
    public void onRenderWorld(RenderWorldEvent event){
        for (Object obj : mc.theWorld.loadedEntityList) {
            Entity entity = (Entity) obj;

            if (entity != mc.thePlayer && (!parent.mobs ? entity instanceof EntityPlayer : true)) {
                GL11.glPushMatrix();

                double posX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * mc.timer.renderPartialTicks;
                double posY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * mc.timer.renderPartialTicks;
                double posZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * mc.timer.renderPartialTicks;

                GL11.glTranslated(posX - RenderManager.renderPosX, (posY - Math.pow(10, 5)) - RenderManager.renderPosY, posZ - RenderManager.renderPosZ);
                Render entityRender = this.mc.getRenderManager().getEntityRenderObject(entity);
                if (entityRender != null) {
                    float distance = this.mc.thePlayer.getDistanceToEntity(entity);
                    if (entity instanceof EntityLivingBase) {
                        GlStateManager.disableLighting();
                        RendererLivingEntity.renderLayers = false;
                        entityRender.doRender(entity, 0.0, 0.0, 0.0, 0.0f, mc.timer.renderPartialTicks);
                        RendererLivingEntity.renderLayers = true;
                        GlStateManager.enableLighting();
                    }
                }
                GL11.glPopMatrix();
            }
        }

        Color color = new Color(parent.red,parent.green,parent.blue);
        RenderUtil.setColor(color);
        MCStencil.checkSetupFBO();
        int list = GL11.glGenLists(1);
        Stencil.getInstance().startLayer();
        GL11.glPushMatrix();
        this.mc.entityRenderer.setupCameraTransform(mc.timer.renderPartialTicks, 0);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        Stencil.getInstance().setBuffer(true);
        GL11.glNewList(list, GL11.GL_COMPILE);
        GlStateManager.enableLighting();
        for (Object obj : mc.theWorld.loadedEntityList) {
            Entity entity = (Entity) obj;

            if (entity != mc.thePlayer && (!parent.mobs ? entity instanceof EntityPlayer : true)) {
                GL11.glPushMatrix();
                GL11.glDisable(GL11.GL_DEPTH_TEST);
                GL11.glLineWidth(3.5f);
                GL11.glEnable(GL11.GL_BLEND);
                GL11.glEnable(GL11.GL_LINE_SMOOTH);
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                GlStateManager.disableLighting();

                double posX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * mc.timer.renderPartialTicks;
                double posY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * mc.timer.renderPartialTicks;
                double posZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * mc.timer.renderPartialTicks;

                GL11.glTranslated(posX - RenderManager.renderPosX, posY - RenderManager.renderPosY, posZ - RenderManager.renderPosZ);
                Render entityRender = this.mc.getRenderManager().getEntityRenderObject(entity);
                if (entityRender != null) {
                    float distance = this.mc.thePlayer.getDistanceToEntity(entity);
                    if (entity instanceof EntityLivingBase) {
                        GlStateManager.disableLighting();
                        RenderUtil.setColor(color);
                        RendererLivingEntity.renderLayers = false;
                        entityRender.doRender(entity, 0.0, 0.0, 0.0, 0.0f, mc.timer.renderPartialTicks);
                        RendererLivingEntity.renderLayers = true;
                        GlStateManager.enableLighting();
                    }
                }
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                GL11.glPopMatrix();
            }
        }
        GL11.glEndList();
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
        GL11.glCallList(list);
        Stencil.getInstance().setBuffer(false);
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
        GL11.glCallList(list);
        Stencil.getInstance().cropInside();
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
        GL11.glCallList(list);
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
        Stencil.getInstance().stopLayer();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDeleteLists(list, 1);
        GL11.glPopMatrix();
        GlStateManager.disableLighting();
    }

}
