package me.valkyrie.vis.module.modules.render.modes.esp;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.render.RenderEntityEvent;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.render.EspMod;
import me.valkyrie.vis.util.render.shader.Shader;
import me.valkyrie.vis.util.render.shader.ShaderType;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.opengl.GL11;

/**
 * Created by Zeb on 8/14/2016.
 */
public class ChamsMode extends ModuleMode<EspMod> {

    public ChamsMode(EspMod parent) {
        super(parent, "Chams");
    }

    @EventHandler
    public void onEntityRender(RenderEntityEvent event){
        if((event.getEntity() instanceof EntityOtherPlayerMP && parent.players) || (!(event.getEntity() instanceof EntityPlayer) && parent.mobs)) {
            switch (event.getType()) {
                case PRE:
                    GL11.glEnable(GL11.GL_POLYGON_OFFSET_FILL);
                    GL11.glPolygonOffset(1, -2000000);
                    break;
                case POST:
                    GL11.glPolygonOffset(1, 2000000);
                    GL11.glDisable(GL11.GL_POLYGON_OFFSET_FILL);
                    break;
            }
        }
    }
}
