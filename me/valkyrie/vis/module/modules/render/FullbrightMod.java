package me.valkyrie.vis.module.modules.render;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventManager;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import me.valkyrie.vis.event.events.render.RenderBrightnessEvent;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/12/2016.
 */
public class FullbrightMod extends Module {

    public FullbrightMod(){
        super(new ModuleInfo("Fullbright", new Color(255,252,127), Keyboard.KEY_NONE), Category.RENDER);

        EventManager.subscribe(new IDontKnowWhyIMadeThisAClassButImLazy());
    }

    public class IDontKnowWhyIMadeThisAClassButImLazy {

        private float brightness = mc.gameSettings.gammaSetting;

        @EventHandler
        public void onBrightnessRender(RenderBrightnessEvent event) {
            event.setBrightness(brightness);
        }

        @EventHandler
        public void onUpdate(PlayerUpdateEvent event){
            if(event.getEventType() == EventType.PRE) return;

            if(getState()){
                brightness *= 1.4f;
            }else{
                brightness /= 1.4f;
            }

            brightness = MathHelper.clamp_float(brightness, mc.gameSettings.gammaSetting, 100);
        }

    }

}