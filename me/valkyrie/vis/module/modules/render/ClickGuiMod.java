package me.valkyrie.vis.module.modules.render;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.gui.clickgui.GuiClickGuiWrapper;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/13/2016.
 */
public class ClickGuiMod extends Module {

    public ClickGuiMod(){
        super(new ModuleInfo("ClickGui", new Color(106, 99,213), Keyboard.KEY_RSHIFT), Category.OTHER);
    }

    @Override
    public void onEnable() {
        Vis.clickGui.init();
        mc.displayGuiScreen(new GuiClickGuiWrapper());
    }
}
