package me.valkyrie.vis.module.modules.world;

import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.modules.render.modes.esp.ChamsMode;
import me.valkyrie.vis.module.modules.render.modes.esp.FillMode;
import me.valkyrie.vis.module.modules.render.modes.esp.OutlineMode;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 8/5/2016.
 */
public class FastMineMod extends Module {

    @NumberValue(label = "Speed", minimum = "0", maximum = "1")
    public double speed = 0.8;

    public FastMineMod(){
        super(new ModuleInfo("FastMine", new Color(133, 213, 126), Keyboard.KEY_NONE), Category.WORLD);
    }

    @EventHandler
    public void onMotion(MotionEvent event){
         if(mc.playerController.curBlockDamageMP > speed){
             mc.playerController.curBlockDamageMP = 1;
         }

        mc.playerController.blockHitDelay = 0;
    }

}
