package me.valkyrie.vis.module.modules.other;

import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.valkyrie.note.util.Noteblock;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.*;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.TimerUtil;
import me.valkyrie.vis.util.chat.ChatBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.network.login.server.S02PacketLoginSuccess;
import net.minecraft.network.play.client.C14PacketTabComplete;
import net.minecraft.network.play.server.*;
import net.minecraft.util.BlockPos;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 7/21/2016.
 */
public class AutoCheatMod extends Module {

    private AntiCheat antiCheat = AntiCheat.NONE;

    private boolean listen = false;
    private TimerUtil time = new TimerUtil();

    @BooleanValue(label = "Adapt")
    private boolean adapt = false;

    public AutoCheatMod(){
        super(new ModuleInfo("AutoCheat", new Color(96,222,5), Keyboard.KEY_NONE), Category.OTHER);
        setRenderPosition(RenderPosition.BOTTOM_RIGHT);
    }

    @Override
    public void onEnable(){
        if(mc.isSingleplayer()){
            setSuffix("None");
            return;
        }

        if(p != null){
            antiCheat = AntiCheat.NONE;
            p.sendQueue.addToSendQueue(new C14PacketTabComplete("/"));
            listen = true;
            time.reset();
        }
    }

    @EventHandler
    public void onPacket(PacketEvent event){
        if(mc != null && p != null && mc.theWorld != null) {
            if (event.getType() == PacketEvent.EventPacketType.RECEIVE && event.getPacket() instanceof S3APacketTabComplete && listen && !time.hasReached(10 * 1000)) {
                S3APacketTabComplete packet = (S3APacketTabComplete) event.getPacket();

                String[] args = packet.func_149630_c();
                List<String> plugins = new ArrayList();

                for (String string : args) {
                    String[] parts = string.split(":");
                    if (parts.length > 1) {
                        String plugin = parts[0].replaceAll("/", "");

                        if(!plugins.contains(plugin)) plugins.add(plugin);
                    }
                }

                plugins.stream().forEach(plugin -> {

                    switch (plugin.toLowerCase()) {
                        case "nocheatplus":
                            antiCheat = AntiCheat.NCP;
                            setSuffix("NCP");
                            listen = false;
                            adapt();
                            break;
                        case "aac":
                            listen = false;
                            antiCheat = AntiCheat.AAC;
                            setSuffix("AAC");
                            adapt();
                            break;
                    }

                });
            }

            if (event.getPacket() instanceof S44PacketWorldBorder && !mc.isSingleplayer()) {
                setSuffix("None");
                if (mc.getCurrentServerData().serverIP.contains("hypixel")) {
                    antiCheat = AntiCheat.WATCHDOG;
                    setSuffix("WATCHDOG");
                }else{
                    antiCheat = AntiCheat.NONE;
                    p.sendQueue.addToSendQueue(new C14PacketTabComplete("/"));
                    listen = true;
                    time.reset();
                }
            }
        }
    }

    public void adapt(){
        if(!adapt) return;

        boolean disabled = false;

        for(Module module : Managers.getModuleManager().getContent()){
            if(module.doesUseModes()){
                boolean found = false;

                if(found) break;

                ModuleMode moduleMode = module.getMode();

                if (moduleMode.getClass().isAnnotationPresent(Bypasses.class)) {
                    Bypasses bypassable = moduleMode.getClass().getAnnotation(Bypasses.class);

                    boolean bypass = false;

                    for (AntiCheat antiCheat : bypassable.anticheats()) {
                        if (antiCheat == this.antiCheat || antiCheat == AntiCheat.ALL) bypass = true;
                    }

                    if (!bypass) {
                        disabled = true;
                        for(ModuleMode mode : module.getModes()){
                            if (mode.getClass().isAnnotationPresent(Bypasses.class)) {
                                Bypasses bypassable2 = mode.getClass().getAnnotation(Bypasses.class);

                                boolean bypass2 = false;

                                for (AntiCheat antiCheat2 : bypassable2.anticheats()) {
                                    if (antiCheat2 == this.antiCheat || antiCheat2 == AntiCheat.ALL) bypass2 = true;
                                }

                                if (bypass2){
                                    module.setMode(mode);
                                    found = true;
                                }
                            }
                        }

                        if(!found){
                            disabled = true;
                            module.setState(false);
                        }
                    }
                }
            }else {
                if (module.getClass().isAnnotationPresent(Bypasses.class) && module.getState()) {
                    Bypasses bypassable = module.getClass().getAnnotation(Bypasses.class);

                    boolean bypass = false;

                    for (AntiCheat antiCheat : bypassable.anticheats()) {
                        if (antiCheat == this.antiCheat || antiCheat == AntiCheat.ALL) bypass = true;
                    }

                    if (!bypass) {
                        disabled = true;
                        module.setState(false);
                    }
                }
            }
        }

        if(disabled){
            new ChatBuilder()
                    .appendPrefix()
                    .appendText("Some modules don't bypass on this anticheat ( " + antiCheat.toString() + "), so we disabled them. To stop this, disable \247aadapt \247fin \247aAutoCheat\247f.")
                    .send();
        }
    }

    @Override
    public void onDisable() {
        setSuffix("None");
    }


}
