package me.valkyrie.vis.module.modules.other;

import me.tojatta.api.value.impl.annotations.StringValue;
import me.valkyrie.note.Instrument;
import me.valkyrie.note.SongManager;
import me.valkyrie.note.exceptions.InvalidNoteFileException;
import me.valkyrie.note.types.NoteSong;
import me.valkyrie.note.util.Noteblock;
import me.valkyrie.vis.Vis;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.game.TickEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.modules.other.modes.noteplayer.ClassicMode;
import me.valkyrie.vis.module.modules.other.modes.noteplayer.FullMode;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.chat.ChatBuilder;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 7/16/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class NotePlayerMod extends Module {

    private String name = "";

    public List<Noteblock> noteBlocks = new ArrayList();

    private String lastSong = "Test";

    @StringValue(label = "Song")
    public String song = "Test";

    private SongManager songManager;

    public NoteSong noteSong;

    private File dir;

    public NotePlayerMod(){
        super(new ModuleInfo("NotePlayer", new Color(89, 110,156), Keyboard.KEY_NONE), Category.OTHER);

        dir = new File(Vis.dir + "/songs");

        if (!dir.exists()) dir.mkdir();

        songManager = new SongManager();

        File song = null;

        for(File file : dir.listFiles()){
            System.out.println("Found song " + file.getName());

            String name = stripExtension(file.getName());

            if(name.equalsIgnoreCase(this.song)){
                song = file;
                break;
            }
        }

        if(song == null){
            lastSong = this.song;
            return;
        }

        try {
            noteSong = songManager.parse(song);
        }catch (InvalidNoteFileException exception){
            exception.printStackTrace();
        }

        lastSong = this.song;

        addMode(new ClassicMode(this), new FullMode(this));
    }

    @EventHandler
    public void onTick(TickEvent event){
//        if(song != lastSong){
//
//            File song = null;
//
//            for(String fName : dir.list()){
//                File file = new File(fName);
//
//                String name = stripExtension(file.getName());
//
//                if(name.equalsIgnoreCase(this.song)){
//                    song = file;
//                    break;
//                }
//            }
//
//            if(song == null){
//                lastSong = this.song;
//                return;
//            }
//
//            try {
//                noteSong = songManager.parse(song);
//            }catch (InvalidNoteFileException exception){
//                exception.printStackTrace();
//            }
//
//            lastSong = this.song;
//        }
    }

    public Noteblock findNoteBlockWithNote(int note){
        for(Noteblock noteBlock : noteBlocks){
            if(noteBlock.getPitch() == note) return noteBlock;
        }

        return null;
    }

    public Noteblock findNoteBlockWithNote(int instrument, int note){
        for(Noteblock noteBlock : noteBlocks){
            System.out.println(instrument + ", " + note + " : " + noteBlock.getLayer() + ", " + noteBlock.getPitch());
            if(noteBlock.getPitch() == note && noteBlock.getLayer() == instrument) return noteBlock;
        }

        return null;
    }

    /*
        Ripped this from a stacktrace thread. No regrets.
    */
    static String stripExtension (String str) {
        if (str == null) return null;
        int pos = str.lastIndexOf(".");
        if (pos == -1) return str;

        return str.substring(0, pos);
    }

}
