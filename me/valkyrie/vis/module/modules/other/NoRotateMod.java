package me.valkyrie.vis.module.modules.other;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/13/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class NoRotateMod extends Module {

    public NoRotateMod(){
        super(new ModuleInfo("NoRotate", new Color(72,179,213), Keyboard.KEY_NONE), Category.OTHER);
    }

    @EventHandler
    public void onPacket(PacketEvent event){
        if(event.getPacket() instanceof S08PacketPlayerPosLook){
            S08PacketPlayerPosLook packet = (S08PacketPlayerPosLook) event.getPacket();

            if(p != null) {
                packet.field_148936_d = p.rotationYaw;
                packet.field_148937_e = p.rotationPitch;
            }
        }
    }

}
