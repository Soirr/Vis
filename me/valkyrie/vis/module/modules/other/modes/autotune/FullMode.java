package me.valkyrie.vis.module.modules.other.modes.autotune;

import me.valkyrie.note.Instrument;
import me.valkyrie.note.util.Noteblock;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.other.AutoTuneMod;
import me.valkyrie.vis.util.RotationUtil;
import me.valkyrie.vis.util.chat.ChatBuilder;
import net.minecraft.block.BlockHardenedClay;
import net.minecraft.block.BlockNote;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.server.S24PacketBlockAction;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Zeb on 8/18/2016.
 */
public class FullMode extends ModuleMode<AutoTuneMod> {

    private Map<Instrument, Integer> pitchMap = new HashMap();
    private List<Noteblock> noteBlocks = new ArrayList();
    private Map<Noteblock, Integer> mapTicks = new HashMap();
    private AutoTuneMod.Stage currentStage = AutoTuneMod.Stage.DISCOVER;

    private int index = 0;
    private int ticksDone = 0;

    public FullMode(AutoTuneMod parent) {
        super(parent, "Full");
    }

    public void onEnable() {
        for(Instrument instrument : Instrument.values()){
            pitchMap.put(instrument, 0);
        }

        currentStage = AutoTuneMod.Stage.DISCOVER;
        noteBlocks.clear();
        mapTicks.clear();
        index = 0;
        ticksDone = 0;

        for(int z = 9; z >= 0; z--){
            for(int x = 9; x >= 0; x--){
                for(int y = 0; y < 6; y++){
                    int pX = (int) Math.floor(p.posX) + 4;
                    int pY = (int) Math.floor(p.posY) - 1;
                    int pZ = (int) Math.floor(p.posZ) + 4;
                    BlockPos pos = new BlockPos(pX - x, pY + y, pZ - z);

                    IBlockState blockState = mc.theWorld.getBlockState(pos);

                    if(blockState != null && blockState.getBlock() != null && blockState.getBlock() instanceof BlockNote){
                        BlockPos under = new BlockPos(pX - x, pY + y - 1, pZ - z);

                        IBlockState underState = mc.theWorld.getBlockState(under);

                        if(underState != null && underState.getBlock() != null){
                            Material material = underState.getBlock().getMaterial();
                            Instrument instrument = Instrument.PIANO;

                            if (material == Material.rock)
                            {
                                instrument = Instrument.BASS_DRUM;
                            }

                            if (material == Material.sand)
                            {
                                instrument = Instrument.SNARE_DRUM;
                            }

                            if (material == Material.glass)
                            {
                                instrument = Instrument.CLICK;
                            }

                            if (material == Material.wood)
                            {
                                instrument = Instrument.BASS;
                            }

                            int pitch = pitchMap.get(instrument);

                            if(pitch <= 25){
                                pitchMap.replace(instrument, pitch + 1);
                                noteBlocks.add(new Noteblock(pos, instrument.ordinal(), pitch));
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        if(index >= noteBlocks.size() && currentStage == AutoTuneMod.Stage.DISCOVER){
            if(mapTicks.size() == 125){
                currentStage = AutoTuneMod.Stage.TUNE;
                index = 0;
            }
            return;
        }

        if(index >= noteBlocks.size() && currentStage == AutoTuneMod.Stage.TUNE){
            currentStage = AutoTuneMod.Stage.DISCOVER;
            parent.setState(false);
            index = 0;
            return;
        }

        Noteblock noteblock = noteBlocks.get(index);
        BlockPos pos = noteblock.getPos();

        if(noteblock == null) return;

        if(event.getType() == EventType.PRE){
            float[] rotations;
            if(noteblock.getLayer() == Instrument.PIANO.ordinal()) {
                rotations = RotationUtil.getRotations(new Vec3(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5));
            }else {
                rotations = RotationUtil.getRotations(new Vec3(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5));
            }


            event.setYaw(rotations[0]);
            event.setPitch(rotations[1]);
        }else{
            switch (currentStage){
                case DISCOVER:
                    if(noteblock.getLayer() == Instrument.PIANO.ordinal()) {
                        p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, pos, EnumFacing.DOWN));
                        p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.ABORT_DESTROY_BLOCK, pos, EnumFacing.DOWN));
                    }else {
                        p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, pos, EnumFacing.UP));
                        p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.ABORT_DESTROY_BLOCK, pos, EnumFacing.UP));
                    }
                    index++;
                    break;
                case TUNE:
                    for(int i = 0; i < (p.ticksExisted % 2 == 0 ? 2 : 1); i++) {
                        if (ticksDone < mapTicks.get(noteblock)) {
                            p.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(pos, 1, p.getHeldItem(), 0, 0, 0));
                            p.swingItem();
                            ticksDone++;
                        } else if (ticksDone == mapTicks.get(noteblock)) {
                            ticksDone = 0;
                            index++;
                            break;
                        }
                    }
                    break;
            }
        }
    }

    @EventHandler
    public void onPacket(PacketEvent event){
        if(event.getType() == PacketEvent.EventPacketType.RECEIVE){
            if(event.getPacket() instanceof S24PacketBlockAction){
                S24PacketBlockAction packet = (S24PacketBlockAction) event.getPacket();

                BlockPos pos = packet.func_179825_a();

                boolean found = false;

                for(Noteblock noteBlock : noteBlocks) {
                    if(noteBlock.getPos().toString().equalsIgnoreCase(pos.toString())){
                        int pitch = packet.getData2();

                        switch (currentStage) {
                            case DISCOVER:
                                int destPitch = noteBlock.getPitch();
                                int ticks = 0;

                                int t = pitch;

                                for(int i = 0; i < 100; i++){
                                    if(t == destPitch){
                                        break;
                                    }

                                    t = ((t + 1) % 25);
                                    ticks++;
                                }

                                mapTicks.put(noteBlock, ticks);
                                break;
                        }
                    }
                }
            }
        }
    }

}
