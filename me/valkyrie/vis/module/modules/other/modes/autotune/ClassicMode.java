package me.valkyrie.vis.module.modules.other.modes.autotune;

import me.valkyrie.note.util.Noteblock;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.other.AutoTuneMod;
import me.valkyrie.vis.util.RotationUtil;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.server.S24PacketBlockAction;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Zeb on 8/18/2016.
 */
public class ClassicMode extends ModuleMode<AutoTuneMod> {

    private List<Noteblock> noteBlocks = new ArrayList();

    private Map<Noteblock, Integer> mapTicks = new HashMap();

    private AutoTuneMod.Stage currentStage = AutoTuneMod.Stage.DISCOVER;

    private int index = 0;
    private int ticksDone = 0;

    public ClassicMode(AutoTuneMod parent) {
        super(parent, "Classic");
    }

    @Override
    public void onEnable() {
        currentStage = AutoTuneMod.Stage.DISCOVER;
        noteBlocks.clear();
        mapTicks.clear();
        index = 0;
        ticksDone = 0;
        for(int i = 0; i < 25; i++) {
            int startX = (int) Math.floor(mc.thePlayer.posX) - 2;
            int startY = (int) Math.floor(mc.thePlayer.posY) - 1;
            int startZ = (int) Math.floor(mc.thePlayer.posZ) - 2;
            int x = startX + i % 5;
            int z = startZ + i / 5;

            noteBlocks.add(new Noteblock(new BlockPos(x, startY, z),i));
        }
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        if(index >= noteBlocks.size() && currentStage == AutoTuneMod.Stage.DISCOVER){
            if(mapTicks.size() == 25){
                currentStage = AutoTuneMod.Stage.TUNE;
                index = 0;
            }
            return;
        }

        if(index >= noteBlocks.size() && currentStage == AutoTuneMod.Stage.TUNE){
            currentStage = AutoTuneMod.Stage.DISCOVER;
            parent.setState(false);
            index = 0;
            return;
        }

        Noteblock noteblock = noteBlocks.get(index);
        BlockPos pos = noteblock.getPos();

        if(noteblock == null) return;

        if(event.getType() == EventType.PRE){
            float[] rotations = RotationUtil.getRotations(new Vec3(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5));

            event.setYaw(rotations[0]);
            event.setPitch(rotations[1]);
        }else{
            switch (currentStage){
                case DISCOVER:
                    p.swingItem();
                    p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, pos, EnumFacing.UP));
                    p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.ABORT_DESTROY_BLOCK, pos, EnumFacing.UP));
                    index++;
                    break;
                case TUNE:
                    for(int i = 0; i < (p.ticksExisted % 2 == 0 ? 2 : 1); i++) {
                        if (ticksDone < mapTicks.get(noteblock)) {
                            p.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(pos, 1, p.getHeldItem(), 0, 0, 0));
                            p.swingItem();
                            ticksDone++;
                        } else if (ticksDone == mapTicks.get(noteblock)) {
                            ticksDone = 0;
                            index++;
                            break;
                        }
                    }
                    break;
            }
        }
    }

    @EventHandler
    public void onPacket(PacketEvent event){
        if(event.getType() == PacketEvent.EventPacketType.RECEIVE){
            if(event.getPacket() instanceof S24PacketBlockAction){
                S24PacketBlockAction packet = (S24PacketBlockAction) event.getPacket();

                BlockPos pos = packet.func_179825_a();

                boolean found = false;

                for(Noteblock noteBlock : noteBlocks) {
                    if(noteBlock.getPos().toString().equalsIgnoreCase(pos.toString())){
                        int pitch = packet.getData2();

                        switch (currentStage) {
                            case DISCOVER:
                                int destPitch = noteBlocks.indexOf(noteBlock);
                                int ticks = 0;

                                int t = pitch;

                                for(int i = 0; i < 100; i++){
                                    if(t == destPitch){
                                        break;
                                    }

                                    t = ((t + 1) % 25);
                                    ticks++;
                                }

                                mapTicks.put(noteBlock, ticks);
                                break;
                        }
                    }
                }
            }
        }
    }

}
