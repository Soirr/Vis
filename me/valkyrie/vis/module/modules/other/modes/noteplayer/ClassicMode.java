package me.valkyrie.vis.module.modules.other.modes.noteplayer;

import me.valkyrie.note.parsers.NoteFileParser;
import me.valkyrie.note.util.Noteblock;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.other.NotePlayerMod;
import me.valkyrie.vis.util.RotationUtil;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3;

import java.io.File;

/**
 * Created by Zeb on 8/23/2016.
 */
public class ClassicMode extends ModuleMode<NotePlayerMod> {

    public ClassicMode(NotePlayerMod parent){
        super(parent, "Classic");
    }

    @Override
    public void onEnable() {
        parent.noteBlocks.clear();
        for(int i = 0; i < 25; i++) {
            int startX = (int) Math.floor(mc.thePlayer.posX) - 2;
            int startY = (int) Math.floor(mc.thePlayer.posY) - 1;
            int startZ = (int) Math.floor(mc.thePlayer.posZ) - 2;
            int x = startX + i % 5;
            int z = startZ + i / 5;

            parent.noteBlocks.add(new Noteblock(new BlockPos(x, startY, z),i));
        }
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        if(event.getType() == EventType.POST) return;

        event.setCancelled(true);

        try {
            if (!parent.noteSong.isRest()) {
                while (!parent.noteSong.isRest()) {
                    int note = parent.noteSong.currentNote()[1];

                    Noteblock block = parent.findNoteBlockWithNote(note);
                    BlockPos pos = block.getPos();

                    if (block != null) {
                        float[] rotations = RotationUtil.getRotations(new Vec3(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5));

                        p.sendQueue.addToSendQueue(new C03PacketPlayer.C06PacketPlayerPosLook(p.posX, p.posY, p.posZ, rotations[0],rotations[1], p.onGround));
                        p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, pos, EnumFacing.UP));
                    }
                    parent.noteSong.next();
                }
            }

            parent.noteSong.next();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
