package me.valkyrie.vis.module.modules.other.modes.noteplayer;

import me.valkyrie.note.Instrument;
import me.valkyrie.note.parsers.NoteFileParser;
import me.valkyrie.note.util.Noteblock;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.other.NotePlayerMod;
import me.valkyrie.vis.util.RotationUtil;
import me.valkyrie.vis.util.chat.ChatBuilder;
import net.minecraft.block.BlockNote;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Zeb on 8/23/2016.
 */
public class FullMode extends ModuleMode<NotePlayerMod> {

    private Map<Instrument, Integer> pitchMap = new HashMap();

    public FullMode(NotePlayerMod parent){
        super(parent, "Full");
    }

    @Override
    public void onEnable() {
        parent.noteBlocks.clear();

        for(Instrument instrument : Instrument.values()){
            pitchMap.put(instrument, 0);
        }

        for(int z = 9; z >= 0; z--){
            for(int x = 9; x >= 0; x--){
                for(int y = 0; y < 6; y++){
                    int pX = (int) Math.floor(p.posX) + 4;
                    int pY = (int) Math.floor(p.posY) - 1;
                    int pZ = (int) Math.floor(p.posZ) + 4;
                    BlockPos pos = new BlockPos(pX - x, pY + y, pZ - z);

                    IBlockState blockState = mc.theWorld.getBlockState(pos);

                    if(blockState != null && blockState.getBlock() != null && blockState.getBlock() instanceof BlockNote){
                        BlockPos under = new BlockPos(pX - x, pY + y - 1, pZ - z);

                        IBlockState underState = mc.theWorld.getBlockState(under);

                        if(underState != null && underState.getBlock() != null){
                            Material material = underState.getBlock().getMaterial();
                            Instrument instrument = Instrument.PIANO;

                            if (material == Material.rock)
                            {
                                instrument = Instrument.BASS_DRUM;
                            }

                            if (material == Material.sand)
                            {
                                instrument = Instrument.SNARE_DRUM;
                            }

                            if (material == Material.glass)
                            {
                                instrument = Instrument.CLICK;
                            }

                            if (material == Material.wood)
                            {
                                instrument = Instrument.BASS;
                            }

                            int pitch = pitchMap.get(instrument);

                            if(pitch <= 25){
                                pitchMap.replace(instrument, pitch + 1);
                                parent.noteBlocks.add(new Noteblock(pos, instrument.ordinal(), pitch));
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        if(event.getType() == EventType.POST) return;

        event.setCancelled(true);

        try {
            if (!parent.noteSong.isRest()) {
                while (!parent.noteSong.isRest()) {
                    int[] set = parent.noteSong.currentNote();

                    if(set[0] > 4 || set[0] < 0) continue;


                    int note = set[1];



                    Noteblock block = parent.findNoteBlockWithNote(set[0], note);
                    BlockPos pos = block.getPos();

                    if (block != null) {
                        float[] rotations;

                        if(block.getLayer() == Instrument.PIANO.ordinal()) {
                            rotations = RotationUtil.getRotations(new Vec3(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5));
                        }else {
                            rotations = RotationUtil.getRotations(new Vec3(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5));
                        }

                        p.sendQueue.addToSendQueue(new C03PacketPlayer.C06PacketPlayerPosLook(p.posX, p.posY, p.posZ, rotations[0],rotations[1], p.onGround));
                        p.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, pos, EnumFacing.UP));
                    }
                    parent.noteSong.next();
                }
            }

            parent.noteSong.next();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
