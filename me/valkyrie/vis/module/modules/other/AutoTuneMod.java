package me.valkyrie.vis.module.modules.other;

import me.valkyrie.note.util.Noteblock;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.modules.other.modes.autotune.ClassicMode;
import me.valkyrie.vis.module.modules.other.modes.autotune.FullMode;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.RotationUtil;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.server.S24PacketBlockAction;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Zeb on 7/15/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class AutoTuneMod extends Module {


    public AutoTuneMod(){
        super(new ModuleInfo("AutoTune", new Color(31,108,156), Keyboard.KEY_NONE), Category.OTHER);
        addMode(new ClassicMode(this), new FullMode(this));
    }

    public enum Stage {

        DISCOVER,
        TUNE

    }

}
