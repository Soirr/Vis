package me.valkyrie.vis.module.modules.player.modes.retard;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventHandlerPriority;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.player.RetardMod;
import me.valkyrie.vis.util.MathUtil;
import me.valkyrie.vis.util.TimerUtil;

/**
 * Created by Zeb on 8/15/2016.
 */
public class BlitzthunderMode extends ModuleMode<RetardMod> {

    private String[] messages = new String[]{
            "Its not skidding if I give credit.",
            "lol precum isn't bad, its an achievement.",
            "umad",
            "#Lynx",
            "#Active",
            "I sell weed and guns, then make mine man clients.",
            "Lol you think ur tough. GANG GANG",
            "You didn't show up to our fight.",
            "[TRIGGERED]",
            "Its not someone else's code if I change a feature.",
            "ez",
            "1v1 me knerd",
            "Lol thats not my facebook",
            "Some kid hacked my youtube and weebly to put that email and facebook you think is mine there.",
            "Fnaf is a real mans game.",
            "Razer krakens are the best headphones on the market",
            "Pressa made 2 swats sucking each other and i filmed it",
            "i yesterday had a gangshoot in Los angeles",
            "I killed 3 of the nigs with my glock",
            "Zeb is a keemstar",
    };

    private TimerUtil time = new TimerUtil();
    private float yaw;

    public BlitzthunderMode(RetardMod parent) {
        super(parent, "Blitzthunder");
    }

    @Override
    public void onEnable() {
        yaw = p.rotationYaw;
    }

    @EventHandler(priority = EventHandlerPriority.HIGH)
    public void onMotion(MotionEvent event){
        if(event.getType() == EventType.POST) return;

        if(time.hasReached(2000)){
            time.reset();
            p.sendChatMessage(messages[MathUtil.getRandomInRange(0, messages.length-1)]);
        }

        yaw += 30;
        event.setYaw(yaw);
        event.setPitch(MathUtil.getRandomInRange(0,180)-90);
        p.swingItem();
    }

}
