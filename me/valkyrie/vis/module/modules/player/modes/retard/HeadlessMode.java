package me.valkyrie.vis.module.modules.player.modes.retard;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventHandlerPriority;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.player.RetardMod;

/**
 * Created by Zeb on 8/15/2016.
 */
public class HeadlessMode extends ModuleMode<RetardMod> {

    public HeadlessMode(RetardMod parent) {
        super(parent, "Headless");
    }

    @EventHandler(priority = EventHandlerPriority.HIGH)
    public void onMotion(MotionEvent event){
        if(event.getType() == EventType.POST) return;

        event.setPitch(180);
    }

}
