package me.valkyrie.vis.module.modules.player.modes.retard;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventHandlerPriority;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.module.modules.player.RetardMod;

/**
 * Created by Zeb on 8/15/2016.
 */
public class SpinMode extends ModuleMode<RetardMod> {

    private float yaw;

    public SpinMode(RetardMod parent) {
        super(parent, "Spin");
    }

    @Override
    public void onEnable() {
        yaw = p.rotationYaw;
    }

    @EventHandler(priority = EventHandlerPriority.HIGH)
    public void onMotion(MotionEvent event){
        if(event.getType() == EventType.POST) return;

        yaw += 60;
        event.setYaw(yaw);
    }

}
