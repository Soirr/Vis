package me.valkyrie.vis.module.modules.player;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.module.modules.combat.modes.killaura.SwitchMode;
import me.valkyrie.vis.module.modules.player.modes.retard.BlitzthunderMode;
import me.valkyrie.vis.module.modules.player.modes.retard.HeadlessMode;
import me.valkyrie.vis.module.modules.player.modes.retard.SpinMode;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.network.play.client.*;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 8/15/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class RetardMod extends Module {

    public RetardMod(){
        super(new ModuleInfo("Retard", new Color(213, 108, 121), Keyboard.KEY_NONE), Category.PLAYER);
        addMode(new SpinMode(this), new HeadlessMode(this), new BlitzthunderMode(this));
    }

    @EventHandler
    public void onPacket(PacketEvent event){
        if(event.getPacket() instanceof C08PacketPlayerBlockPlacement || event.getPacket() instanceof C07PacketPlayerDigging && !Managers.getModuleManager().getModule("Killaura").getState()){
            p.sendQueue.getNetworkManager().sendPacket(new C03PacketPlayer.C05PacketPlayerLook(p.rotationYaw, p.rotationPitch, p.onGround));
        }
    }

}
