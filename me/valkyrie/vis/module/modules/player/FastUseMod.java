package me.valkyrie.vis.module.modules.player;

import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemFood;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 7/13/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class FastUseMod extends Module {

    @NumberValue(label = "Speed", minimum = "8", maximum = "20")
    public int speed = 18;

    @NumberValue(label = "Packets", minimum = "1", maximum = "30")
    public int packets = 18;

    public FastUseMod(){
        super(new ModuleInfo("FastUse", new Color(77, 142, 213), Keyboard.KEY_NONE), Category.PLAYER);
    }

    @EventHandler
    public void onPlayerUpdate(PlayerUpdateEvent event){
        if(event.getEventType() == EventType.POST) return;

        if(p.getItemInUseDuration() >= speed && (p.getItemInUse().getItem() instanceof ItemFood || p.getItemInUse().getItem() instanceof ItemBow)){
            for (int i = 0; i <= packets; i++){
                p.sendQueue.addToSendQueue(new C03PacketPlayer(p.onGround));
            }

            if(p.getItemInUse().getItem() instanceof ItemBow) mc.playerController.onStoppedUsingItem(p);
        }
    }

}
