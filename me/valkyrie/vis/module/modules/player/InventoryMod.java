package me.valkyrie.vis.module.modules.player;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.events.game.PacketEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.network.play.client.C0DPacketCloseWindow;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 8/12/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class InventoryMod extends Module {

    public InventoryMod(){
        super(new ModuleInfo("MoreInventory", new Color(218, 167, 49), Keyboard.KEY_NONE), Category.PLAYER);
    }

    @EventHandler
    public void onPacket(PacketEvent event){
        if(event.getPacket() instanceof C0DPacketCloseWindow) event.setCancelled(true);
    }

}
