package me.valkyrie.vis.module.modules.player;

import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.MotionEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import net.minecraft.block.material.Material;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Created by Zeb on 8/12/2016.
 */
@Bypasses(anticheats = {AntiCheat.NCP, AntiCheat.WATCHDOG})
public class ZootMod extends Module {

    @NumberValue(label = "Packets", minimum = "10", maximum = "200")
    public int packets = 200;

    public ZootMod(){
        super(new ModuleInfo("Zoot", new Color(213, 176, 178), Keyboard.KEY_NONE), Category.PLAYER);
    }

    @EventHandler
    public void onMotion(MotionEvent event){
        if(event.getType() == EventType.PRE) return;

        if(p.getActivePotionEffect(Potion.blindness) != null) p.removePotionEffect(Potion.blindness.getId());
        if(p.getActivePotionEffect(Potion.confusion) != null) p.removePotionEffect(Potion.confusion.getId());

        if(((p.isBurning() && !p.isInsideOfMaterial(Material.lava)) || hasBadPot()) && p.onGround){
            for(int i = 0; i < packets; i++){
                p.sendQueue.getNetworkManager().sendPacket(new C03PacketPlayer(true));
            }
        }
    }

    public boolean hasBadPot(){
        Potion[] badEffects = new Potion[]{
                Potion.hunger,
                Potion.weakness,
                Potion.moveSlowdown,
                Potion.poison,
                Potion.digSlowdown,
                Potion.wither
        };

        for(Potion potion : badEffects){
            if(p.getActivePotionEffect(potion) != null) return true;
        }

        return false;
    }

}
