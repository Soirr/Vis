package me.valkyrie.vis.module.modules.player;

import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.game.TickEvent;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import me.valkyrie.vis.module.Bypasses;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.ModuleInfo;
import me.valkyrie.vis.util.AntiCheat;
import me.valkyrie.vis.util.MathUtil;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Zeb on 6/19/2016.
 */
@Bypasses(anticheats = {AntiCheat.ALL})
public class ChestStealerMod extends Module {

    private int slot;
    private int ticks = 0;

    @NumberValue(label = "Delay", minimum = "1", maximum = "20")
    public int delay = 1;

    @BooleanValue(label = "Random")
    public boolean random = false;

    public ChestStealerMod(){
        super(new ModuleInfo("ChestStealer", new Color(209, 218, 39), Keyboard.KEY_NONE), Category.PLAYER);
    }

    public boolean isEmpty(){
        if(p.openContainer != null && p.openContainer instanceof ContainerChest){
            ContainerChest container = (ContainerChest) p.openContainer;
            for(int i = 0; i < container.getLowerChestInventory().getSizeInventory(); ++i){
                ItemStack itemStack = container.getLowerChestInventory().getStackInSlot(i);

                if(itemStack != null) return false;
            }
        }

        return true;
    }

    public int getRandomSlot(){
        if(isEmpty()) return -1;

        if(p.openContainer != null && p.openContainer instanceof ContainerChest){
            ContainerChest container = (ContainerChest) p.openContainer;

            List<Integer> filled = new ArrayList<Integer>();

            for(int i = 0; i < container.getLowerChestInventory().getSizeInventory(); ++i){
                ItemStack itemStack = container.getLowerChestInventory().getStackInSlot(i);

                if(itemStack != null){
                    filled.add(i);
                }
            }

            int random = MathUtil.getRandomInRange(0, filled.size() - 1);

            int slot = filled.get(random);

            return slot;
        }

        return -1;
    }

    @EventHandler
    public void onTick(TickEvent event){
        if(p == null || mc.theWorld == null) return;

        if(p.openContainer != null && p.openContainer instanceof ContainerChest){
            ContainerChest container = (ContainerChest) p.openContainer;
            if(!isEmpty()){
                if(ticks == 0) {
                    if (!random) {
                        if(slot < container.getLowerChestInventory().getSizeInventory()){
                            mc.playerController.windowClick(container.windowId, slot, 0, 1, p);
                        }

                        slot++;
                    }else{
                        slot = getRandomSlot();
                        if(slot < container.getLowerChestInventory().getSizeInventory())
                            mc.playerController.windowClick(container.windowId, slot, 0, 1, p);
                    }

                    ticks = delay;
                }else{
                    if(ticks > 0) ticks--;
                }
            }
        }else{
            slot = 0;
        }
    }

}
