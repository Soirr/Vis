package me.valkyrie.vis.module;

import me.tojatta.api.value.Value;
import me.valkyrie.vis.event.EventManager;
import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.manager.Managers;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import org.lwjgl.Sys;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Zeb on 7/12/2016.
 */
public class Module {

    private ModuleMode mode = null;
    private List<ModuleMode> modes = new LinkedList();

    private ModuleInfo moduleInfo;
    private Category moduleType;

    private RenderPosition renderPosition = RenderPosition.TOP_RIGHT;

    private String suffix = "";

    protected Minecraft mc = Minecraft.getMinecraft();
    public EntityPlayerSP p;

    private List<Value> values = new LinkedList<Value>();

    private ModuleCommand moduleCommand = null;

    public Module(ModuleInfo moduleInfo, Category moduleType) {
        this.moduleInfo = moduleInfo;
        this.moduleType = moduleType;
        Managers.getValueManager().register(this);

        for(Value value : Managers.getValueManager().getContent()){
            if(value.getObject().getClass().getName().equals(getClass().getName())){
                values.add(value);
                if(moduleCommand == null){
                    moduleCommand = new ModuleCommand(this);
                    Managers.getCommandManager().addContent(moduleCommand);
                }
            }
        }
    }

    public RenderPosition getRenderPosition() {
        return renderPosition;
    }

    public void setRenderPosition(RenderPosition renderPosition) {
        this.renderPosition = renderPosition;
    }

    public boolean doesUseModes(){
        return modes.size() > 0;
    }

    public ModuleMode getMode(String name){
        if(!doesUseModes()) return null;

        for(ModuleMode moduleMode : getModes()){
            if(moduleMode.getName().equalsIgnoreCase(name)) return moduleMode;
        }

        return null;
    }

    public void addMode(ModuleMode... modes){
        for(ModuleMode mode : modes){
            if(!doesUseModes()) setMode(mode);

            this.modes.add(mode);
        }

        if(moduleCommand == null){
            moduleCommand = new ModuleCommand(this);
            Managers.getCommandManager().addContent(moduleCommand);
        }
    }

    public void setMode(ModuleMode mode){
        setSuffix("\247f" + mode.getName());


        if(this.mode != null){
            this.mode.onDisable();
            EventManager.unsubscribe(this.mode);
        }

        this.getModuleInfo().setMode(mode.getName());
        this.mode = mode;

        if(getState()) {
            mode.onEnable();
            EventManager.subscribe(mode);
        }
        if(Managers.getInfoManager() != null) Managers.getInfoManager().save();
    }

    public void onEnable(){}

    public void onDisable(){}

    public void setState(boolean state){
        if(mode != null){
            mode.mc = mc;
            mode.p = p;
        }

        if(state){
            onEnable();
            EventManager.subscribe(this);

            if(mode != null){
                mode.onEnable();
                EventManager.subscribe(mode);
            }
        }else{
            EventManager.unsubscribe(this);
            onDisable();

            if(mode != null){
                mode.onDisable();
                EventManager.unsubscribe(mode);
            }
        }

        moduleInfo.setState(state);
    }

    public boolean getState(){
        return moduleInfo.getState();
    }

    public ModuleMode getMode() {
        return mode;
    }

    public List<ModuleMode> getModes() {
        return modes;
    }

    public ModuleInfo getModuleInfo() {
        return moduleInfo;
    }

    public Category getCategory() {
        return moduleType;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = "\247F" + suffix;
    }

    public List<Value> getValues() {
        return values;
    }

    public Value getValue(String label){
        for(Value value : values){
            if(value.getLabel().equalsIgnoreCase(label)) return value;
        }

        return null;
    }
}
