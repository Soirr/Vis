package me.valkyrie.vis.module;

/**
 * Created by Zeb on 7/12/2016.
 */
public enum Category {

    COMBAT("Combat"),
    MOVEMENT("Movement"),
    PLAYER("Player"),
    RENDER("Render"),
    WORLD("World"),
    OTHER("Other");


    private String name;

    Category(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }

}
