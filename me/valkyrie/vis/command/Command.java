package me.valkyrie.vis.command;

import java.util.List;

/**
 * Created by Zeb on 7/27/2016.
 */
public class Command {

    private String name;
    private String[] aliases;
    private String[] usages;

    public Command(String name, String[] usages) {
        this.name = name;
        this.usages = usages;
        this.aliases = new String[]{};
    }

    public Command(String name, String[] aliases, String[] usages) {
        this.name = name;
        this.aliases = aliases;
        this.usages = usages;
    }

    public String handle(List<String> arguments){
        return "Command was unable to be handled.";
    }

    public String getName() {
        return name;
    }

    public String[] getAliases() {
        return aliases;
    }

    public String[] getUsages() {
        return usages;
    }
}
