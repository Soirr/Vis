package me.valkyrie.vis.command;

import me.valkyrie.vis.manager.Managers;

import java.util.List;

/**
 * Created by Zeb on 7/27/2016.
 */
public class CommandBuilder {

    private String name;
    private String[] aliases = new String[]{};
    private String[] usages = new String[]{};

    private CommandExecutor commandExecutor;

    public CommandBuilder(String name) {
        this.name = name;
    }

    public CommandBuilder withAliases(String[] aliases){
        this.aliases = aliases;
        return this;
    }

    public CommandBuilder withUsages(String[] usages){
        this.usages = usages;
        return this;
    }

    public CommandBuilder withCommandExecutor(CommandExecutor commandExecutor){
        this.commandExecutor = commandExecutor;
        return this;
    }

    public void build(){
        Managers.getCommandManager().addContent(new Command(name, aliases, usages){

            @Override
            public String handle(List<String> arguments) {
                return commandExecutor.handle(arguments);
            }

        });
    }

}
