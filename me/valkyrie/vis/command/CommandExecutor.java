package me.valkyrie.vis.command;

import java.util.List;

/**
 * Created by Zeb on 7/27/2016.
 */
public interface CommandExecutor {

    String handle(List<String> arguments);

}
