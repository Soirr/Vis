package me.valkyrie.vis.command.commands;

import me.valkyrie.vis.command.Command;
import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.util.chat.ChatBuilder;
import me.valkyrie.vis.util.chat.ChatColor;
import net.minecraft.event.ClickEvent;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by Zeb on 7/28/2016.
 */
public class ColorCommand extends Command {

    public ColorCommand() {
        super("Color", new String[]{"c"}, new String[]{"'module' 'r' 'g' 'b'", "'module' random"});
    }

    @Override
    public String handle(List<String> arguments) {

        if(arguments.size() != 4 && arguments.size() != 2) return "invalid_usage";

        String name = arguments.get(0);
        Module module = Managers.getModuleManager().getModule(name);

        if(module == null){
            return "Module not found.";
        }

        if(arguments.get(1).equalsIgnoreCase("random") && arguments.size() == 2){
            try{
                URL url = new URL("http://www.colourlovers.com/api/colors/random?format=json");
                URLConnection connection = url.openConnection();
                connection.setRequestProperty( "User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5" );

                InputStream response = connection.getInputStream();

                StringBuilder sb =new StringBuilder();
                BufferedReader br = new BufferedReader(new InputStreamReader(response));
                String read;

                while((read=br.readLine()) != null){
                    sb.append(read);
                }

                String string = sb.toString();

                JSONArray jsonArray = new JSONArray(string);

                JSONObject jsonObject = jsonArray.getJSONObject(0);

                JSONObject jsonColor = jsonObject.getJSONObject("rgb");

                Color color = new Color(jsonColor.getInt("red"),jsonColor.getInt("green"),jsonColor.getInt("blue"));

                new ChatBuilder()
                        .appendPrefix()
                        .appendText("Set color to '" + jsonObject.getString("title") + "'.", ChatColor.WHITE)
                        .appendText(jsonObject.getString("url").replaceAll("" + '\'',""), new ClickEvent(ClickEvent.Action.OPEN_URL, jsonObject.getString("url").replaceAll("" + '\'',"")), ChatColor.GREEN).send();
                module.getModuleInfo().setColor(color);
                Managers.getInfoManager().save();

                br.close();
                response.close();
            }catch(Exception e){
                e.printStackTrace();
            }
            return "no_message";
        }else if(arguments.size() == 4){
            try{
                int r = Integer.parseInt(arguments.get(1));
                int g = Integer.parseInt(arguments.get(2));
                int b = Integer.parseInt(arguments.get(3));

                module.getModuleInfo().setColor(new Color(r,g,b));
                Managers.getInfoManager().save();
                return "Set color to &a" + r + "&f, &a" + g + "&f, &a" + b + "&f.";
            }catch (Exception e){
                return "invalid_usage";
            }
        }else{
            return "invalid_usage";
        }
    }
}
