package me.valkyrie.vis.command.commands;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.command.Command;
import me.valkyrie.vis.manager.Managers;

import java.util.List;

/**
 * Created by Zeb on 8/10/2016.
 */
public class HudCommand extends Command {

    public HudCommand() {
        super("Hud", new String[]{"tabgui 'visible"});
    }

    @Override
    public String handle(List<String> arguments) {
        if(arguments.size() != 2){
            return "invalid_usage";
        }

        String value = arguments.get(1);

        if(arguments.get(0).equalsIgnoreCase("tabgui")){
            boolean visible = false;

            if(value.equalsIgnoreCase("true")){
                visible = true;
            }else if(value.equalsIgnoreCase("false")){
                visible = false;
            }

            Vis.tabVisible = visible;
            Managers.getValueManager().save();

            return "Tabgui is now &a" + (visible ? "shown" : "hidden") + "&f.";
        }else if(arguments.get(0).equalsIgnoreCase("modinfo")){
            boolean visible = false;

            if(value.equalsIgnoreCase("true")){
                visible = true;
            }else if(value.equalsIgnoreCase("false")){
                visible = false;
            }

            Vis.modInfo = visible;
            Managers.getValueManager().save();

            return "Module Info is now &a" + (visible ? "shown" : "hidden") + "&f.";
        }else{
            return "invalid_usage";
        }
    }
}
