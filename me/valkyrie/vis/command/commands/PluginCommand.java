package me.valkyrie.vis.command.commands;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.command.Command;
import me.valkyrie.vis.manager.Manager;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.manager.managers.PluginManager;
import me.valkyrie.vis.module.ModuleMode;
import me.valkyrie.vis.plugin.Plugin;
import me.valkyrie.vis.plugin.PluginInfo;
import me.valkyrie.vis.util.chat.ChatBuilder;
import me.valkyrie.vis.util.chat.ChatColor;

import java.util.List;

/**
 * Created by Zeb on 8/10/2016.
 */
public class PluginCommand extends Command {

    public PluginCommand() {
        super("Plugin", new String[]{"pl"}, new String[]{"list", "reload", "enable 'plugin'", "disable 'plugin'", "info 'plugin'"});
    }

    @Override
    public String handle(List<String> arguments) {
        if(arguments.isEmpty()) return "invalid_usage";

        PluginManager pluginManager = Managers.getPluginManager();

        if(arguments.size() == 1 && arguments.get(0).equalsIgnoreCase("list")){
            new ChatBuilder().appendPrefix().appendText("Plugins :").send();

            for (Plugin plugin : pluginManager.getPluginInfoMap().keySet()){
                PluginInfo pluginInfo = pluginManager.getPluginInfoMap().get(plugin);

                new ChatBuilder().appendText("  - ", ChatColor.GRAY).appendText(pluginInfo.getName(), pluginManager.getPluginStateMap().get(plugin) ? ChatColor.GREEN : ChatColor.RED).send();
            }

            return "no_message";
        }else if(arguments.size() == 2 && arguments.get(0).equalsIgnoreCase("info")){
            Plugin plugin = pluginManager.getPlugin(arguments.get(1));
            if(plugin == null) return "Plugin not found.";

            PluginInfo pluginInfo = pluginManager.getPluginInfo(arguments.get(1));

            new ChatBuilder().appendPrefix().appendText(pluginInfo.getName(), pluginManager.getPluginStateMap().get(plugin) ? ChatColor.GREEN : ChatColor.RED).appendText(" v" + pluginInfo.getVersion() + " made by " + pluginInfo.getAuthor() + ".").send();
            return "no_message";
        }else if(arguments.size() == 2 && arguments.get(0).equalsIgnoreCase("enable")){
            Plugin plugin = pluginManager.getPlugin(arguments.get(1));
            if(plugin == null) return "Plugin not found.";
            PluginInfo pluginInfo = pluginManager.getPluginInfo(arguments.get(1));

            pluginManager.enable(plugin);
            return "Enabled &a" + pluginInfo.getName() + "&f.";
        }else if(arguments.size() == 2 && arguments.get(0).equalsIgnoreCase("disable")){
            Plugin plugin = pluginManager.getPlugin(arguments.get(1));
            if(plugin == null) return "Plugin not found.";
            PluginInfo pluginInfo = pluginManager.getPluginInfo(arguments.get(1));

            pluginManager.disable(plugin);
            return "Disabled &a" + pluginInfo.getName() + "&f.";
        }else if(arguments.size() == 1 && arguments.get(0).equalsIgnoreCase("reload")){
            pluginManager.reload();
            return "Reloaded.";
        }

            return "invalid_usage";
    }
}
