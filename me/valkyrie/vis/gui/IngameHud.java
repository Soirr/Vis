package me.valkyrie.vis.gui;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.command.CommandBuilder;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventManager;
import me.valkyrie.vis.event.events.render.RenderScreenEvent;
import me.valkyrie.vis.gui.hud.HudTheme;
import me.valkyrie.vis.gui.hud.vis.VisHudTheme;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.RenderPosition;
import me.valkyrie.vis.util.BlurUtil;
import me.valkyrie.vis.util.font.CFontRenderer;
import me.valkyrie.vis.util.font.FontObject;
import me.valkyrie.vis.util.render.RenderUtil;
import me.valkyrie.vis.util.render.ShaderUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.client.shader.Shader;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.*;

/**
 * Created by Zeb on 7/27/2016.
 */
public class IngameHud {

    private HudTheme hudTheme;

    public IngameHud(String theme) {
        for(HudTheme hudTheme : Managers.getHudThemeManager().getContent()){
            if(hudTheme.getName().equalsIgnoreCase(theme)){
                this.hudTheme = hudTheme;
                Vis.tabGui.setTheme(hudTheme.getTabTheme());
                break;
            }
        }

        if(hudTheme == null){
            hudTheme = new VisHudTheme();
        }

        new CommandBuilder("theme")
                .withAliases(new String[]{"th"})
                .withUsages(new String[]{"'theme'"})
                .withCommandExecutor(arguments -> {

                    if(arguments.size() != 1){
                        return "invalid_usage";
                    }

                    String newTheme = arguments.get(0);

                    for(HudTheme hudTheme : Managers.getHudThemeManager().getContent()){
                        if(hudTheme.getName().equalsIgnoreCase(newTheme)){
                            this.hudTheme = hudTheme;
                            Vis.tabGui.setTheme(hudTheme.getTabTheme());
                            Vis.hudTheme = hudTheme.getName();
                            Managers.getValueManager().save();
                            return "Set theme to \247a" + hudTheme.getName() + "\247f.";
                        }
                    }

                    return "no_message";

                }).build();

        EventManager.subscribe(this);
    }

    @EventHandler
    public void onRenderScreen(RenderScreenEvent event) {
        if (Minecraft.getMinecraft().gameSettings.showDebugInfo) return;

        hudTheme.getHudRenderer().render();

        Vis.tabGui.render();
    }
    public static class ModuleSorter implements Comparator<Module> {

        private FontObject fontObject;

        public ModuleSorter(FontObject fontObject) {
            this.fontObject = fontObject;
        }

        @Override
        public int compare(Module o1, Module o2) {
            int w1;
            int w2;

            if(Vis.modInfo) {
                if(o1.getSuffix() != ""){
                    w1 = fontObject.getStringWidth(o1.getModuleInfo().getDisplayName() + " " + o1.getSuffix());
                }else{
                    w1 = fontObject.getStringWidth(o1.getModuleInfo().getDisplayName());
                }

                if(o2.getSuffix() != ""){
                    w2 = fontObject.getStringWidth(o2.getModuleInfo().getDisplayName() + " " + o2.getSuffix());
                }else{
                    w2 = fontObject.getStringWidth(o2.getModuleInfo().getDisplayName());
                }
            }else{
                w1 = fontObject.getStringWidth(o1.getModuleInfo().getDisplayName());
                w2 = fontObject.getStringWidth(o2.getModuleInfo().getDisplayName());
            }
            if(w1 < w2){
                return 1;
            }else if(w2 < w1){
                return -1;
            }else{
                return 0;
            }
        }
    }

}
