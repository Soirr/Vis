package me.valkyrie.vis.gui.tabgui;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventManager;
import me.valkyrie.vis.event.EventType;
import me.valkyrie.vis.event.events.player.PlayerUpdateEvent;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 8/6/2016.
 */
public class TabPanel {

    public static int x = 5, y = 15;

    private TabSelectionBox tabSelectionBox = new TabSelectionBox(0, this);
    private int index = 0;
    private List<TabItem> items = new ArrayList();

    public TabPanel() {
        EventManager.subscribe(this);
    }

    public void addItem(TabItem item){
        items.add(item);
    }

    public void handleKey(int key){
        if(items.size() == 0) return;

        switch (key){
            case Keyboard.KEY_DOWN: index++; break;
            case Keyboard.KEY_UP: index--; break;
            default: getItem().handleKey(key);
        }

        if(index > items.size() -1) index -= (items.size());
        if(index < 0) index = items.size() - 1;
    }

    public TabItem getItem(){
        return items.get(index);
    }

    @EventHandler
    public void onUpdate(PlayerUpdateEvent event){
        if(event.getEventType() == EventType.POST || items.size() == 0) return;

        int dY = (index * Vis.tabGui.getTheme().getHeight());
        float difference = dY - tabSelectionBox.getY();

        difference *= 0.8f;

        tabSelectionBox.setY(tabSelectionBox.getY() + difference);
    }

    public void render(){
        if(items.size() == 0) return;

        GlStateManager.translate((x + getXOffset()),y,0);
        Vis.tabGui.getTheme().dispatchObjectRender(this);
        GlStateManager.translate(-(x + getXOffset()),-y,0);

        GlStateManager.translate((x + getXOffset()),y + tabSelectionBox.getY(),0);
        Vis.tabGui.getTheme().dispatchObjectRender(tabSelectionBox);
        GlStateManager.translate(-(x + getXOffset()),-(y + tabSelectionBox.getY()),0);

        for(TabItem item : items){
            GlStateManager.translate((x + getXOffset()),y + (items.indexOf(item) * Vis.tabGui.getTheme().getHeight()),0);
            item.setSelected(getItem() == item);
            Vis.tabGui.getTheme().dispatchObjectRender(item);
            GlStateManager.translate(-(x + getXOffset()),-(y + (items.indexOf(item) * Vis.tabGui.getTheme().getHeight())),0);
        }
    }

    public int getXOffset(){
        int ox = 0;

        for(TabPanel panel : Vis.tabGui.getPanels()){
            if(Vis.tabGui.getPanels().indexOf(panel) < Vis.tabGui.getPanels().indexOf(this)){
                ox += panel.getWidth() + 4;
            }
        }

        return ox;
    }

    public int getHeight(){
        return items.size() * Vis.tabGui.getTheme().getHeight();
    }

    public int getWidth(){
        int max = 0;
        for(TabItem item : items){
            if(Vis.tabGui.getTheme().getFont().getStringWidth(item.getText()) > max) max = Vis.tabGui.getTheme().getFont().getStringWidth(item.getText());
        }

        return max + 16 + Vis.tabGui.getTheme().getAdditionalWidth();
    }

    public List<TabItem> getItems() {
        return items;
    }
}
