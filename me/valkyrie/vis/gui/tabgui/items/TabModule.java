package me.valkyrie.vis.gui.tabgui.items;

import me.valkyrie.vis.gui.tabgui.TabItem;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Module;
import org.lwjgl.input.Keyboard;

/**
 * Created by Zeb on 8/10/2016.
 */
public class TabModule extends TabItem {

    private Module module;

    public TabModule(Module module) {
        super(module.getModuleInfo().getDisplayName());
        this.module = module;
    }

    @Override
    public void handleKey(int key) {
        switch (key){
            case Keyboard.KEY_RETURN:
                module.setState(!module.getState());
                Managers.getInfoManager().save();
                break;
        }
    }

    public Module getModule() {
        return module;
    }

    @Override
    public String getText() {
        return module.getModuleInfo().getDisplayName();
    }
}
