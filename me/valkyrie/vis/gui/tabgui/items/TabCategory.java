package me.valkyrie.vis.gui.tabgui.items;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.gui.tabgui.TabItem;
import me.valkyrie.vis.gui.tabgui.TabPanel;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import org.lwjgl.input.Keyboard;

/**
 * Created by Zeb on 8/7/2016.
 */
public class TabCategory extends TabItem {

    private Category category;

    public TabCategory(Category category) {
        super(category.getName());
        this.category = category;
    }

    @Override
    public void handleKey(int key) {
        if(key == Keyboard.KEY_RIGHT){
            for(int i = 0; i < Vis.tabGui.getPanels().size(); i++){
                if(i >= 1) Vis.tabGui.getPanels().remove(i);
            }

            TabPanel panel = new TabPanel();

            for(Module module : Managers.getModuleManager().getContent()){
                if(module.getCategory() == category) panel.addItem(new TabModule(module));
            }

            if(panel.getItems().isEmpty()) return;
            Vis.tabGui.getPanels().add(panel);
            Vis.tabGui.next();
        }
    }
}
