package me.valkyrie.vis.gui.tabgui.theme;

import me.valkyrie.vis.gui.theme.Theme;
import me.valkyrie.vis.gui.theme.ThemeType;
import me.valkyrie.vis.util.font.FontObject;

/**
 * Created by Zeb on 8/7/2016.
 */
public class TabTheme extends Theme {

    private int height;
    private int additionalWidth;

    public TabTheme(FontObject font, int height, int additionalWidth) {
        super(ThemeType.TABGUI, font);
        this.height = height;
        this.additionalWidth = additionalWidth;
    }

    public int getHeight(){
        return height;
    }

    public int getAdditionalWidth() {
        return additionalWidth;
    }

    public void setAdditionalWidth(int additionalWidth) {
        this.additionalWidth = additionalWidth;
    }
}
