package me.valkyrie.vis.gui.tabgui;

/**
 * Created by Zeb on 8/9/2016.
 */
public class TabSelectionBox {

    private TabPanel parent;
    private float y;

    public TabSelectionBox(float y, TabPanel parent) {
        this.y = y;
        this.parent = parent;
    }

    public TabPanel getParent() {
        return parent;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

}
