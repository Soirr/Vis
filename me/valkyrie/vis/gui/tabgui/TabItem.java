package me.valkyrie.vis.gui.tabgui;

/**
 * Created by Zeb on 8/6/2016.
 */
public class TabItem {

    private String text;
    private boolean isSelected = false;

    public TabItem(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void handleKey(int key){}

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
