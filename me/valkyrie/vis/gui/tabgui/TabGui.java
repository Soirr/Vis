package me.valkyrie.vis.gui.tabgui;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventManager;
import me.valkyrie.vis.event.events.game.KeyPressEvent;
import me.valkyrie.vis.gui.tabgui.items.TabCategory;
import me.valkyrie.vis.gui.tabgui.theme.TabTheme;
import me.valkyrie.vis.module.Category;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 8/7/2016.
 */
public class TabGui {

    private TabTheme theme;
    private int index = 0;
    private List<TabPanel> panels = new ArrayList();

    public TabGui(TabTheme theme){
        this.theme = theme;
        EventManager.subscribe(this);

        TabPanel mainPanel = new TabPanel();

        for(Category category : Category.values()){
            mainPanel.addItem(new TabCategory(category));
        }

        this.panels.add(mainPanel);
    }

    @EventHandler
    public void onKey(KeyPressEvent event){
        if(!Vis.tabVisible) return;

        if(event.getKey() == Keyboard.KEY_LEFT && index > 0){
            prev();
        }else{
            getTabPanel().handleKey(event.getKey());
        }

        for(int i = 0; i < Vis.tabGui.getPanels().size(); i++){
            if(i > index) Vis.tabGui.getPanels().remove(i);
        }
    }

    public void render(){
        if(Vis.tabVisible) panels.stream().forEach(panel -> panel.render());
    }

    public void next(){
        index++;
    }

    public void prev(){
        index--;
    }

    public TabTheme getTheme() {
        return theme;
    }

    public TabPanel getTabPanel() {
        return getPanels().get(index);
    }

    public int getIndex() {
        return index;
    }

    public List<TabPanel> getPanels() {
        return panels;
    }

    public void setTheme(TabTheme theme) {
        this.theme = theme;
    }
}
