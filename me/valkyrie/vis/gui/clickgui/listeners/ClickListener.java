package me.valkyrie.vis.gui.clickgui.listeners;

/**
 * Created by Zeb on 8/22/2016.
 */
public interface ClickListener<T> {

    void onClick(T object, int button);

}
