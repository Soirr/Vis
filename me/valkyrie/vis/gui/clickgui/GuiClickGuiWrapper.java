package me.valkyrie.vis.gui.clickgui;

import me.valkyrie.vis.Vis;
import net.minecraft.client.gui.GuiScreen;

import java.io.IOException;

/**
 * Created by Zeb on 8/22/2016.
 */
public class GuiClickGuiWrapper extends GuiScreen {

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        Vis.clickGui.render();
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        Vis.clickGui.onClick(mouseX, mouseY, mouseButton);
    }

    @Override
    protected void mouseReleased(int mouseX, int mouseY, int state) {
        Vis.clickGui.onRelease(mouseX, mouseY, state);
    }
}
