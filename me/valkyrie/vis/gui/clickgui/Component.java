package me.valkyrie.vis.gui.clickgui;

import me.valkyrie.vis.gui.clickgui.parts.Container;

/**
 * Created by Zeb on 7/31/2016.
 */
public class Component {

    private String text;

    private int x;
    private int y;
    private int width;
    private int height;

    private Container parent;

    public Component(int x, int y, int width, int height, Container parent) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.parent = parent;
        this.text = "";
    }

    public Component(String text, int x, int y, int width, int height, Container parent) {
        this.text = text;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.parent = parent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void onClick(int x, int y, int button){

    }

    public void onRelease(int x, int y, int button){

    }

    public boolean inMouseOver(int x, int y){
        return (x >= getX() && y >= getY() && x <= getX() + getWidth() && y < getY() + getHeight());
    }

}
