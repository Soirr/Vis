package me.valkyrie.vis.gui.clickgui.parts;

import me.valkyrie.vis.gui.clickgui.Component;
import me.valkyrie.vis.gui.clickgui.listeners.ClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 8/21/2016.
 */
public class Button extends Component {

    private boolean state = false;
    private List<ClickListener<Button>> listeners = new ArrayList();

    public Button(String text, int width, int height, Container parent) {
        super(text, 0, 0, width, height, parent);
    }

    @Override
    public void onClick(int x, int y, int button) {
        if(button == 0) state = !state;

        listeners.stream().forEach(buttonClickListener -> buttonClickListener.onClick(this, button));
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
