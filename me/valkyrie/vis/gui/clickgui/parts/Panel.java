package me.valkyrie.vis.gui.clickgui.parts;

import me.valkyrie.vis.gui.clickgui.ClickGui;
import me.valkyrie.vis.gui.clickgui.Component;

/**
 * Created by Zeb on 7/31/2016.
 */
public class Panel extends Container {

    private boolean maximized = false;

    public Panel(String text, int x, int y, int width, int height) {
        super(text, x, y, width, height, null);
    }

    public boolean isMaximized() {
        return maximized;
    }

    public void setMaximized(boolean maximized) {
        this.maximized = maximized;
    }

    @Override
    public void arrangeChildren(){
        int y = this.getY() + ClickGui.getTheme().getTitleHeight();

        int size = ClickGui.getTheme().getTitleHeight();

        for(Component child : getChildren()){
            child.setY(y);
            child.setX(getX());
            child.setWidth(getWidth());
            y += child.getHeight();
            size += child.getHeight();
        }

        setHeight(size);
    }
}
