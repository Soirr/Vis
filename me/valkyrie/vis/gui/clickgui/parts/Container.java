package me.valkyrie.vis.gui.clickgui.parts;

import me.valkyrie.vis.gui.clickgui.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 7/31/2016.
 */
public class Container extends Component {

    private List<Component> children = new ArrayList();

    public Container(String text, int x, int y, int width, int height, Container parent) {
        super(text, x, y, width, height, parent);
    }

    public void addChild(Component... children){
        for(Component child : children) this.children.add(child);
    }

    public void arrangeChildren(){
       int y = this.getY();

        for(Component child : getChildren()){
            child.setY(y);
            child.setX(getX());
            child.setWidth(getWidth());
            y += child.getHeight();
        }
    }

    public List<Component> getChildren() {
        return children;
    }
}
