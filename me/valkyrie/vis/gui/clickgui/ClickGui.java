package me.valkyrie.vis.gui.clickgui;

import me.valkyrie.vis.gui.clickgui.parts.Button;
import me.valkyrie.vis.gui.clickgui.parts.Panel;
import me.valkyrie.vis.gui.clickgui.theme.ClickTheme;
import me.valkyrie.vis.gui.clickgui.theme.test.TestTheme;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Category;
import me.valkyrie.vis.module.Module;
import net.minecraft.client.renderer.GlStateManager;
import sun.security.pkcs11.Secmod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 7/31/2016.
 */
public class ClickGui {

    private Panel panel;
    private List<Panel> panels = new ArrayList();

    private static ClickTheme theme;

    public ClickGui() {
        theme = new TestTheme();
        init();
        panel = panels.get(0);
    }

    public void init(){

        int x = 10;
        int y = 10;

        panels.clear();

        for(Category category : Category.values()) {
            Panel panel = new Panel(category.getName(), x, y, 100, 140);

            for (Module module : Managers.getModuleManager().getContent()) {
                if (module.getCategory() == category) {

                    Button button = new Button(module.getModuleInfo().getDisplayName(), 75, 16, panel);

                    button.setState(module.getState());

                    panel.addChild(button);
                }
            }

            x += 120;

            panels.add(panel);
        }
    }

    public void onClick(int x, int y, int button){
        panels.stream().forEach(panel -> {
            panel.getChildren().stream().forEach(component -> {
                if(component.inMouseOver(x,y)){
                    component.onClick(x,y,button);
                }
            });
        });
    }

    public void onRelease(int x, int y, int button){
        panels.stream().forEach(panel -> {
            panel.getChildren().stream().forEach(component -> {
                if(component.inMouseOver(x,y)){
                    component.onRelease(x,y,button);
                }
            });
        });
    }

    public void render(){
        panels.stream().forEach(panel -> {
            panel.arrangeChildren();
            GlStateManager.translate(panel.getX(), panel.getY(), 0);
            theme.dispatchObjectRender(panel);
            GlStateManager.translate(-panel.getX(), -panel.getY(), 0);

            panel.getChildren().stream().forEach(component -> {

                GlStateManager.translate(component.getX(), component.getY(), 0);
                theme.dispatchObjectRender(component);
                GlStateManager.translate(-component.getX(), -component.getY(), 0);

            });
        });
    }

    public static ClickTheme getTheme() {
        return theme;
    }
}
