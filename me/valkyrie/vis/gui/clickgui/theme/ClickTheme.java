package me.valkyrie.vis.gui.clickgui.theme;

import me.valkyrie.vis.gui.theme.Theme;
import me.valkyrie.vis.gui.theme.ThemeType;
import me.valkyrie.vis.util.font.FontObject;

/**
 * Created by Zeb on 8/22/2016.
 */
public class ClickTheme extends Theme {

    private int titleHeight;

    public ClickTheme(FontObject font, int titleHeight) {
        super(ThemeType.CLICKGUI, font);
        this.titleHeight = titleHeight;
    }

    public int getTitleHeight() {
        return titleHeight;
    }
}
