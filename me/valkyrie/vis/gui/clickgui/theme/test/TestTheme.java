package me.valkyrie.vis.gui.clickgui.theme.test;

import me.valkyrie.vis.gui.clickgui.parts.*;
import me.valkyrie.vis.gui.clickgui.parts.Panel;
import me.valkyrie.vis.gui.clickgui.theme.ClickTheme;
import me.valkyrie.vis.util.font.CFontRenderer;
import me.valkyrie.vis.util.font.FontObject;

import java.awt.Color;
import java.awt.Font;

/**
 * Created by Zeb on 8/22/2016.
 */
public class TestTheme extends ClickTheme {

    public TestTheme() {
        super(new CFontRenderer(new Font("Arial", Font.PLAIN, 18), true, false), 20);

        registerRenderer(Panel.class, new TestPanelRenderer());
        registerRenderer(Button.class, new TestButtonRenderer());
    }
}
