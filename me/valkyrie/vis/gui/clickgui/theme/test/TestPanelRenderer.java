package me.valkyrie.vis.gui.clickgui.theme.test;

import me.valkyrie.vis.gui.clickgui.ClickGui;
import me.valkyrie.vis.gui.clickgui.parts.Panel;
import me.valkyrie.vis.gui.theme.ObjectRenderer;
import me.valkyrie.vis.util.font.FontObject;
import me.valkyrie.vis.util.render.RenderUtil;
import net.minecraft.client.renderer.entity.Render;

import java.awt.*;

/**
 * Created by Zeb on 8/22/2016.
 */
public class TestPanelRenderer implements ObjectRenderer<Panel> {

    @Override
    public void render(Panel object) {
        FontObject font = ClickGui.getTheme().getFont();

        //RenderUtil.drawRect(-0.8f,-0.8f,object.getWidth() + 0.8f, object.getHeight() + 0.8f, new Color(16,16,16));
        RenderUtil.drawRect(0,0,object.getWidth(), object.getHeight(), new Color(28,28,28));

        RenderUtil.drawRect(0,0,19,19, new Color(35,101,133));

        RenderUtil.drawRect(0,19f, object.getWidth(), 20, Color.WHITE);

        font.drawString(object.getText(), 23, 7, Color.WHITE.getRGB());
    }
}
