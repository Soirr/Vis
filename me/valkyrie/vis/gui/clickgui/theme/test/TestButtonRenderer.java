package me.valkyrie.vis.gui.clickgui.theme.test;

import me.valkyrie.vis.gui.clickgui.ClickGui;
import me.valkyrie.vis.gui.clickgui.parts.Button;
import me.valkyrie.vis.gui.theme.ObjectRenderer;
import me.valkyrie.vis.util.font.FontObject;
import me.valkyrie.vis.util.render.RenderUtil;

import java.awt.*;

/**
 * Created by Zeb on 8/22/2016.
 */
public class TestButtonRenderer implements ObjectRenderer<Button> {

    @Override
    public void render(Button object) {
        FontObject fontObject = ClickGui.getTheme().getFont();

        if(object.getState()){
            RenderUtil.setColor(new Color(35,101,133));
            RenderUtil.drawRect(0,0,object.getWidth(), object.getHeight(), new Color(35,101,133));
        }

        fontObject.drawString(object.getText(), 10, 5, Color.WHITE.getRGB());
    }
}
