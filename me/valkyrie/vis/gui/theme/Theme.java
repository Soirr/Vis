package me.valkyrie.vis.gui.theme;

import me.valkyrie.vis.util.font.FontObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Zeb on 8/6/2016.
 */
public class Theme {

    private ThemeType themeType;
    private FontObject font;
    private Map rendererMap = new HashMap<Class, ObjectRenderer>();

    public Theme(ThemeType themeType, FontObject font) {
        this.themeType = themeType;
        this.font = font;
    }

    protected void registerRenderer(Class objectClass, ObjectRenderer objectRenderer){
        rendererMap.put(objectClass, objectRenderer);
    }

    public void dispatchObjectRender(Object object){
        if(!rendererMap.containsKey(object.getClass())) return;

        ((ObjectRenderer) rendererMap.get(object.getClass())).render(object);
    }

    public FontObject getFont() {
        return font;
    }

    public ThemeType getThemeType() {
        return themeType;
    }
}
