package me.valkyrie.vis.gui.theme;

/**
 * Created by Zeb on 8/6/2016.
 */
public interface ObjectRenderer<T> {

    void render(T object);

}
