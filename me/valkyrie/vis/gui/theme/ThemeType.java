package me.valkyrie.vis.gui.theme;

import me.valkyrie.vis.gui.clickgui.ClickGui;

/**
 * Created by Zeb on 8/6/2016.
 */
public enum ThemeType {

    TABGUI,
    CLICKGUI

}
