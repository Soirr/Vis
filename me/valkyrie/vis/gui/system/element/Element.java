package me.valkyrie.vis.gui.system.element;

/**
 * Created by Zeb on 8/19/2016.
 */
public class Element {

    private int x;
    private int y;

    public Element(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void render(){}

}
