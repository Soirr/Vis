package me.valkyrie.vis.gui.system;

import me.valkyrie.vis.gui.system.element.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 8/19/2016.
 */
public class DisplayScreen {

    private List<Element> elements = new ArrayList();

    public void addElement(Element... elements){
        for(Element element : elements){
            this.elements.add(element);
        }
    }

    public void render(){
        elements.stream().forEach(Element::render);
    }

}
