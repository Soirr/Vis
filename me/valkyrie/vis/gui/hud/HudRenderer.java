package me.valkyrie.vis.gui.hud;

/**
 * Created by Zeb on 8/20/2016.
 */
public interface HudRenderer {

    void render();

}
