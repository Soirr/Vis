package me.valkyrie.vis.gui.hud.custom.tab;

import me.valkyrie.vis.gui.hud.custom.CustomHudTheme;
import me.valkyrie.vis.gui.tabgui.TabPanel;
import me.valkyrie.vis.gui.theme.ObjectRenderer;
import me.valkyrie.vis.util.render.RenderUtil;

import java.awt.*;

/**
 * Created by Zeb on 8/7/2016.
 */
public class CustomTabPanelRenderer implements ObjectRenderer<TabPanel> {

    @Override
    public void render(TabPanel object) {
        float width = CustomHudTheme.getLineWidth();

        RenderUtil.drawRect(-width,-width,object.getWidth() + width, object.getHeight() + width, new Color(10,10,10,200));
    }

}
