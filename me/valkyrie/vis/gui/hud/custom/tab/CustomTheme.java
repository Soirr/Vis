package me.valkyrie.vis.gui.hud.custom.tab;

import me.valkyrie.vis.event.EventHandler;
import me.valkyrie.vis.event.EventManager;
import me.valkyrie.vis.event.events.render.RenderScreenEvent;
import me.valkyrie.vis.gui.hud.custom.CustomHudTheme;
import me.valkyrie.vis.gui.tabgui.TabPanel;
import me.valkyrie.vis.gui.tabgui.TabSelectionBox;
import me.valkyrie.vis.gui.tabgui.items.TabCategory;
import me.valkyrie.vis.gui.tabgui.items.TabModule;
import me.valkyrie.vis.gui.tabgui.theme.TabTheme;
import me.valkyrie.vis.util.font.NormalFontRenderer;

/**
 * Created by Zeb on 8/9/2016.
 */
public class CustomTheme extends TabTheme {

    public CustomTheme(){
        super(new NormalFontRenderer(), new NormalFontRenderer().getStringHeight("H") + 4, CustomHudTheme.getAdditionalWidth());
        registerRenderer(TabPanel.class, new CustomTabPanelRenderer());
        registerRenderer(TabSelectionBox.class, new CustomTabSelectionBoxRenderer());
        registerRenderer(TabCategory.class, new CustomTabCategoryRenderer());
        registerRenderer(TabModule.class, new CustomTabModuleRenderer());
        EventManager.subscribe(this);
    }

    @EventHandler
    public void onRenderScreen(RenderScreenEvent event){
        setAdditionalWidth(CustomHudTheme.getAdditionalWidth());
    }
}
