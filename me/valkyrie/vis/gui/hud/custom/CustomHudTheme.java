package me.valkyrie.vis.gui.hud.custom;

import me.tojatta.api.value.impl.annotations.NumberValue;
import me.valkyrie.vis.Vis;
import me.valkyrie.vis.command.CommandBuilder;
import me.valkyrie.vis.gui.IngameHud;
import me.valkyrie.vis.gui.hud.HudTheme;
import me.valkyrie.vis.gui.hud.custom.tab.CustomTheme;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.RenderPosition;
import me.valkyrie.vis.util.font.FontObject;
import me.valkyrie.vis.util.font.NormalFontRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import org.lwjgl.opengl.Display;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Zeb on 8/20/2016.
 */
public class CustomHudTheme extends HudTheme {

    @NumberValue(label = "Red", minimum = "0", maximum = "255")
    private static int red = 94;
    @NumberValue(label = "Green", minimum = "0", maximum = "255")
    private static int green = 125;
    @NumberValue(label = "Blue", minimum = "0", maximum = "255")
    private static int blue = 212;

    @NumberValue(label = "LineWidth", minimum = "0", maximum = "1")
    private static float lineWidth = 0;
    @NumberValue(label = "ExtraWidth", minimum = "0", maximum = "50")
    private static int additionalWidth = 0;

    public CustomHudTheme() {
        super("Custom", new CustomTheme(), () -> {

            FontObject font = new NormalFontRenderer();

            font.drawStringWithShadow("Vis \247Fr" + Vis.rel, 5, 3, getColor().getRGB());

            font.drawStringWithShadow("\247FBranch: Valkyrie", Display.getWidth() / 2 - font.getStringWidth("\247FBranch: Valkyrie") - 3, Display.getHeight() / 2 - font.getStringHeight("H") - 2, getColor().getRGB());

            int y = 3;
            int y2 = Display.getHeight() / 2 - ((font.getStringHeight("H") + 2) * 2);

            java.util.List<Module> mods = new ArrayList();

            for (Module module : Managers.getModuleManager().getContent()) {
                if (module.getState() && module.getModuleInfo().isVisible()) {
                    mods.add(module);
                }
            }

            Collections.sort(mods, new IngameHud.ModuleSorter(font));

            for (Module module : mods) {
                if (module.getRenderPosition() == RenderPosition.TOP_RIGHT) {
                    if (module.getSuffix().equals("") || !Vis.modInfo) {
                        font.drawStringWithShadow(
                                module.getModuleInfo().getDisplayName(),
                                Display.getWidth() / 2 - font.getStringWidth(module.getModuleInfo().getDisplayName()) - 3, y,
                                module.getModuleInfo().getColor().getRGB()
                        );
                    } else {
                        font.drawStringWithShadow(
                                module.getModuleInfo().getDisplayName() + " " + module.getSuffix(),
                                Display.getWidth() / 2 - font.getStringWidth(module.getModuleInfo().getDisplayName() + " " + module.getSuffix()) - 3, y,
                                module.getModuleInfo().getColor().getRGB()
                        );
                    }

                    y += font.getStringHeight("H") + 2;
                } else {
                    if (module.getSuffix().equals("") || !Vis.modInfo) {
                        font.drawStringWithShadow(
                                module.getModuleInfo().getDisplayName(),
                                Display.getWidth() / 2 - font.getStringWidth(module.getModuleInfo().getDisplayName()) - 3, y2,
                                module.getModuleInfo().getColor().getRGB()
                        );
                    } else {
                        font.drawStringWithShadow(
                                module.getModuleInfo().getDisplayName() + " " + module.getSuffix(),
                                Display.getWidth() / 2 - font.getStringWidth(module.getModuleInfo().getDisplayName() + " " + module.getSuffix()) - 3, y2,
                                module.getModuleInfo().getColor().getRGB()
                        );
                    }

                    y2 -= font.getStringHeight("H") + 2;
                }
            }

        });

        Managers.getValueManager().register(this);

        new CommandBuilder("CustomTheme")
                .withAliases(new String[]{"ctheme", "cth"})
                .withUsages(new String[]{"color 'red' 'green' 'blue'", "lineWidth 'lineWidth'", "extraWidth 'extraWidth'"})
                .withCommandExecutor(arguments -> {

                    if(arguments.size() == 4 && arguments.get(0).equalsIgnoreCase("color")){
                        try{
                            setRed(Integer.parseInt(arguments.get(1)));
                            setGreen(Integer.parseInt(arguments.get(2)));
                            setBlue(Integer.parseInt(arguments.get(3)));
                            Managers.getValueManager().save();
                            return "Set color.";
                        }catch (Exception e){
                            return "Color values must be a number.";
                        }
                    }else if(arguments.size() == 2 && arguments.get(0).equalsIgnoreCase("lineWidth")){
                        try{
                            setLineWidth(Float.parseFloat(arguments.get(1)));
                            Managers.getValueManager().save();
                            return "Set line width to \247a" + getLineWidth() + "\247f.";
                        }catch (Exception e){
                            return "Line width must be a number.";
                        }
                    }else if(arguments.size() == 2 && arguments.get(0).equalsIgnoreCase("extrawidth")){
                        try{
                            setAdditionalWidth(Integer.parseInt(arguments.get(1)));
                            Managers.getValueManager().save();
                            return "Set extra width to \247a" + getAdditionalWidth() + "\247f.";
                        }catch (Exception e){
                            return "Extra width must be a number.";
                        }
                    }

                    return "invalid_usage";
                }).build();
    }

    public static Color getColor(){
        return new Color(red, green, blue);
    }

    public static float getLineWidth() {
        return lineWidth;
    }

    public static int getAdditionalWidth() {
        return additionalWidth;
    }

    public static void setRed(int red) {
        CustomHudTheme.red = red;
    }

    public static void setGreen(int green) {
        CustomHudTheme.green = green;
    }

    public static void setBlue(int blue) {
        CustomHudTheme.blue = blue;
    }

    public static void setLineWidth(float lineWidth) {
        CustomHudTheme.lineWidth = lineWidth;
    }

    public static void setAdditionalWidth(int additionalWidth) {
        CustomHudTheme.additionalWidth = additionalWidth;
    }
}
