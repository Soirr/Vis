package me.valkyrie.vis.gui.hud.vis.tab;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.gui.tabgui.TabSelectionBox;
import me.valkyrie.vis.gui.tabgui.items.TabCategory;
import me.valkyrie.vis.gui.theme.ObjectRenderer;

import java.awt.*;

/**
 * Created by Zeb on 8/9/2016.
 */
public class VisTabCategoryRenderer implements ObjectRenderer<TabCategory> {

    @Override
    public void render(TabCategory object) {
        Vis.tabGui.getTheme().getFont().drawStringWithShadow(object.getText(), 3, 3, Color.WHITE.getRGB());
    }
}
