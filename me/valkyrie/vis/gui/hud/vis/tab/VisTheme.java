package me.valkyrie.vis.gui.hud.vis.tab;

import me.valkyrie.vis.gui.tabgui.TabPanel;
import me.valkyrie.vis.gui.tabgui.TabSelectionBox;
import me.valkyrie.vis.gui.tabgui.items.TabCategory;
import me.valkyrie.vis.gui.tabgui.items.TabModule;
import me.valkyrie.vis.gui.tabgui.theme.TabTheme;
import me.valkyrie.vis.util.font.NormalFontRenderer;

/**
 * Created by Zeb on 8/9/2016.
 */
public class VisTheme extends TabTheme {

    public VisTheme(){
        super(new NormalFontRenderer(), new NormalFontRenderer().getStringHeight("H") + 4, 0);
        registerRenderer(TabPanel.class, new VisTabPanelRenderer());
        registerRenderer(TabSelectionBox.class, new VisTabSelectionBoxRenderer());
        registerRenderer(TabCategory.class, new VisTabCategoryRenderer());
        registerRenderer(TabModule.class, new VisTabModuleRenderer());
    }
}
