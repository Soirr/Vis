package me.valkyrie.vis.gui.hud.vis.tab;

import javafx.scene.control.Tab;
import me.valkyrie.vis.gui.tabgui.TabPanel;
import me.valkyrie.vis.gui.theme.ObjectRenderer;
import me.valkyrie.vis.util.render.RenderUtil;

import java.awt.*;

/**
 * Created by Zeb on 8/7/2016.
 */
public class VisTabPanelRenderer implements ObjectRenderer<TabPanel> {

    @Override
    public void render(TabPanel object) {
        RenderUtil.drawRect(0,0,object.getWidth(), object.getHeight(), new Color(10,10,10,200));
    }

}
