package me.valkyrie.vis.gui.hud.direkt.tab;

import me.valkyrie.vis.gui.tabgui.TabPanel;
import me.valkyrie.vis.gui.tabgui.TabSelectionBox;
import me.valkyrie.vis.gui.tabgui.items.TabCategory;
import me.valkyrie.vis.gui.tabgui.items.TabModule;
import me.valkyrie.vis.gui.tabgui.theme.TabTheme;
import me.valkyrie.vis.util.font.NormalFontRenderer;

/**
 * Created by Zeb on 8/9/2016.
 */
public class DirektTheme extends TabTheme {

    public DirektTheme(){
        super(new NormalFontRenderer(), new NormalFontRenderer().getStringHeight("H") + 2, 32);
        registerRenderer(TabPanel.class, new DirektTabPanelRenderer());
        registerRenderer(TabSelectionBox.class, new DirektTabSelectionBoxRenderer());
        registerRenderer(TabCategory.class, new DirektTabCategoryRenderer());
        registerRenderer(TabModule.class, new DirektTabModuleRenderer());
    }
}
