package me.valkyrie.vis.gui.hud.direkt.tab;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.gui.tabgui.TabSelectionBox;
import me.valkyrie.vis.gui.theme.ObjectRenderer;
import me.valkyrie.vis.util.render.RenderUtil;

import java.awt.*;

/**
 * Created by Zeb on 8/9/2016.
 */
public class DirektTabSelectionBoxRenderer implements ObjectRenderer<TabSelectionBox> {

    @Override
    public void render(TabSelectionBox object) {
        RenderUtil.drawRect(0,0, object.getParent().getWidth(), Vis.tabGui.getTheme().getHeight(), new Color(120, 194, 83));
    }
}
