package me.valkyrie.vis.gui.hud.light;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.gui.IngameHud;
import me.valkyrie.vis.gui.hud.HudTheme;
import me.valkyrie.vis.gui.hud.light.tab.LightTheme;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.module.Module;
import me.valkyrie.vis.module.RenderPosition;
import me.valkyrie.vis.util.font.CFontRenderer;
import me.valkyrie.vis.util.render.RenderUtil;
import org.lwjgl.opengl.Display;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Zeb on 8/20/2016.
 */
public class LightHudTheme extends HudTheme {

    private static CFontRenderer font = new CFontRenderer(new Font("Arail", Font.PLAIN, 18), true, false);

    public LightHudTheme() {
        super("Light", new LightTheme(), () -> {

            font.drawStringWithShadow("Vis \247Fr" + Vis.rel, 5, 3, new Color(117, 183, 216).getRGB());

            font.drawStringWithShadow("\247FBranch: Valkyrie", Display.getWidth() / 2 - font.getStringWidth("\247FBranch: Valkyrie") - 3, Display.getHeight() / 2 - font.getStringHeight("H") - 2, new Color(94, 125, 212).getRGB());

            int y = 3;
            int y2 = Display.getHeight() / 2 - ((font.getStringHeight("H") + 2) * 2);

            java.util.List<Module> mods = new ArrayList();

            for (Module module : Managers.getModuleManager().getContent()) {
                if (module.getState() && module.getModuleInfo().isVisible()) {
                    mods.add(module);
                }
            }

            Collections.sort(mods, new IngameHud.ModuleSorter(font));

            for (Module module : mods) {
                if (module.getRenderPosition() == RenderPosition.TOP_RIGHT) {
                    if (module.getSuffix().equals("") || !Vis.modInfo) {

                        font.drawStringWithShadow(
                                module.getModuleInfo().getDisplayName(),
                                Display.getWidth() / 2 - font.getStringWidth(module.getModuleInfo().getDisplayName()) - 2, y,
                                module.getModuleInfo().getColor().getRGB()
                        );
                    } else {

                        font.drawStringWithShadow(
                                module.getModuleInfo().getDisplayName() + " " + module.getSuffix(),
                                Display.getWidth() / 2 - font.getStringWidth(module.getModuleInfo().getDisplayName() + " " + module.getSuffix()) - 2, y,
                                module.getModuleInfo().getColor().getRGB()
                        );
                    }

                    y += font.getStringHeight(module.getModuleInfo().getDisplayName()) + 2;
                } else {
                    if (module.getSuffix().equals("") || !Vis.modInfo) {
                        font.drawStringWithShadow(
                                module.getModuleInfo().getDisplayName(),
                                Display.getWidth() / 2 - font.getStringWidth(module.getModuleInfo().getDisplayName()) - 2, y2,
                                module.getModuleInfo().getColor().getRGB()
                        );
                    } else {
                        font.drawStringWithShadow(
                                module.getModuleInfo().getDisplayName() + " " + module.getSuffix(),
                                Display.getWidth() / 2 - font.getStringWidth(module.getModuleInfo().getDisplayName() + " " + module.getSuffix()) - 2, y2,
                                module.getModuleInfo().getColor().getRGB()
                        );
                    }

                    y2 -= font.getStringHeight(module.getModuleInfo().getDisplayName()) + 2;
                }
            }

        });
    }
}
