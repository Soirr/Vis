package me.valkyrie.vis.gui.hud.light.tab;

import me.valkyrie.vis.Vis;
import me.valkyrie.vis.gui.tabgui.items.TabModule;
import me.valkyrie.vis.gui.theme.ObjectRenderer;

import java.awt.*;

/**
 * Created by Zeb on 8/10/2016.
 */
public class LightTabModuleRenderer implements ObjectRenderer<TabModule> {

    @Override
    public void render(TabModule object) {
        Vis.tabGui.getTheme().getFont().drawStringWithShadow(object.getText(), object.isSelected() ? 6 : 3, 1, object.getModule().getState() ? Color.WHITE.getRGB() : Color.LIGHT_GRAY.getRGB());
    }
}
