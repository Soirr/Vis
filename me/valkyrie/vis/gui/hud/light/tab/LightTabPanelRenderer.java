package me.valkyrie.vis.gui.hud.light.tab;

import me.valkyrie.vis.gui.tabgui.TabPanel;
import me.valkyrie.vis.gui.theme.ObjectRenderer;
import me.valkyrie.vis.util.render.RenderUtil;

import java.awt.*;

/**
 * Created by Zeb on 8/7/2016.
 */
public class LightTabPanelRenderer implements ObjectRenderer<TabPanel> {

    @Override
    public void render(TabPanel object) {
        RenderUtil.drawRect(-0.7f,-0.7f,object.getWidth()+0.7f, object.getHeight() + 0.7f, new Color(10,10,10,200));
    }

}
