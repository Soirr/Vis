package me.valkyrie.vis.gui.hud.light.tab;

import me.valkyrie.vis.gui.tabgui.TabPanel;
import me.valkyrie.vis.gui.tabgui.TabSelectionBox;
import me.valkyrie.vis.gui.tabgui.items.TabCategory;
import me.valkyrie.vis.gui.tabgui.items.TabModule;
import me.valkyrie.vis.gui.tabgui.theme.TabTheme;
import me.valkyrie.vis.util.font.CFontRenderer;
import me.valkyrie.vis.util.font.NormalFontRenderer;

import java.awt.*;

/**
 * Created by Zeb on 8/9/2016.
 */
public class LightTheme extends TabTheme {

    public LightTheme(){
        super(new CFontRenderer(new Font("Arail", Font.PLAIN, 18), true, false), new CFontRenderer(new Font("Arail", Font.PLAIN, 18), true, false).getStringHeight("H") + 2, 32);
        registerRenderer(TabPanel.class, new LightTabPanelRenderer());
        registerRenderer(TabSelectionBox.class, new LightTabSelectionBoxRenderer());
        registerRenderer(TabCategory.class, new LightTabCategoryRenderer());
        registerRenderer(TabModule.class, new LightTabModuleRenderer());
    }
}
