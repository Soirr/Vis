package me.valkyrie.vis.gui.hud;

import me.valkyrie.vis.gui.tabgui.theme.TabTheme;

/**
 * Created by Zeb on 8/20/2016.
 */
public class HudTheme {

    private String name;

    private TabTheme tabTheme;
    private HudRenderer hudRenderer;

    public HudTheme(String name, TabTheme tabTheme, HudRenderer hudRenderer) {
        this.name = name;
        this.tabTheme = tabTheme;
        this.hudRenderer = hudRenderer;
    }

    public String getName() {
        return name;
    }

    public TabTheme getTabTheme() {
        return tabTheme;
    }

    public HudRenderer getHudRenderer() {
        return hudRenderer;
    }
}
