package me.valkyrie.vis.plugin;

/**
 * Created by Zeb on 8/8/2016.
 */
public interface Plugin {

    void enable();
    void disable();

}
