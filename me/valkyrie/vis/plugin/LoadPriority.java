package me.valkyrie.vis.plugin;

/**
 * Created by Zeb on 8/8/2016.
 */
public enum LoadPriority {

    LOW(0),
    NORMAl(1),
    HIGH(2);

    private int priorityValue;

    LoadPriority(int priorityValue) {
        this.priorityValue = priorityValue;
    }

    public int getPriorityValue() {
        return priorityValue;
    }
}
