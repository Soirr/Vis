package me.valkyrie.vis.plugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.valkyrie.vis.manager.Managers;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Created by Zeb on 8/8/2016.
 */
public class PluginInfo {

    private String name;
    private String author;
    private String version;
    private String mainClass;
    private String loadPriority;


    public PluginInfo(String name, String author, String version, String mainClass, LoadPriority loadPriority) {
        this.name = name;
        this.author = author;
        this.version = version;
        this.mainClass = mainClass;
        this.loadPriority = loadPriority.toString();
    }

    public LoadPriority getLoadPriority() {
        for(LoadPriority loadPriority : LoadPriority.values()){
            if(loadPriority.toString().equalsIgnoreCase(this.loadPriority)) return loadPriority;
        }

        return LoadPriority.NORMAl;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getVersion() {
        return version;
    }

    public String getMainClass() {
        return mainClass;
    }

    @Override
    public String toString() {
        return "PluginInfo{" +
                "loadPriority=" + loadPriority +
                ", mainClass='" + mainClass + '\'' +
                ", version='" + version + '\'' +
                ", author='" + author + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
