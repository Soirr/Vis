package me.valkyrie.vis.event;

import java.lang.reflect.Method;

/**
 * Created by Zeb on 7/11/2016.
 */
public class EventMethodListener {

    private Object parent;
    private Method method;
    private EventHandlerPriority priority;

    public EventMethodListener(Object parent, Method method, EventHandlerPriority priority) {
        this.parent = parent;
        this.method = method;
        this.priority = priority;
    }

    public Object getParent() {
        return parent;
    }

    public Method getMethod() {
        return method;
    }

    public EventHandlerPriority getPriority() {
        return priority;
    }
}
