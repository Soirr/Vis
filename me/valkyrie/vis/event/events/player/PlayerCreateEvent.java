package me.valkyrie.vis.event.events.player;

import me.valkyrie.vis.event.Event;
import net.minecraft.client.entity.EntityPlayerSP;

/**
 * Created by Zeb on 7/28/2016.
 */
public class PlayerCreateEvent extends Event {

    private EntityPlayerSP player;

    public PlayerCreateEvent(EntityPlayerSP player) {
        this.player = player;
    }

    public EntityPlayerSP getPlayer() {
        return player;
    }
}
