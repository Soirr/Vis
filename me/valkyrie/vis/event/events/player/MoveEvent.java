package me.valkyrie.vis.event.events.player;

import me.valkyrie.vis.event.Event;

/**
 * Created by Zeb on 8/1/2016.
 */
public class MoveEvent extends Event {

    public double x,y,z;

    public MoveEvent(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
