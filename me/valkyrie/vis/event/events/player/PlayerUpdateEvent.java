package me.valkyrie.vis.event.events.player;

import me.valkyrie.vis.event.Event;
import me.valkyrie.vis.event.EventType;

/**
 * Created by Zeb on 7/12/2016.
 */
public class PlayerUpdateEvent extends Event {

    private EventType eventType;

    public PlayerUpdateEvent(EventType eventType) {
        this.eventType = eventType;
    }

    public EventType getEventType() {
        return eventType;
    }
}
