package me.valkyrie.vis.event.events.entity;

import me.valkyrie.vis.event.Event;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;

/**
 * Created by Zeb on 7/14/2016.
 */
public class CollisionEvent extends Event {

    private Entity entity;
    private AxisAlignedBB boundingBox;
    private Block block;
    private BlockPos pos;

    public CollisionEvent(Entity entity, AxisAlignedBB boundingBox, Block block, BlockPos pos) {
        this.entity = entity;
        this.boundingBox = boundingBox;
        this.block = block;
        this.pos = pos;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }

    public AxisAlignedBB getBoundingBox() {
        return boundingBox;
    }

    public void setBoundingBox(AxisAlignedBB boundingBox) {
        this.boundingBox = boundingBox;
    }

    public BlockPos getPos() {
        return pos;
    }

    public Block getBlock() {
        return block;
    }
}
