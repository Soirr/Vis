package me.valkyrie.vis.event.events.render;

import me.valkyrie.vis.event.Event;

/**
 * Created by Zeb on 7/12/2016.
 */
public class RenderBrightnessEvent extends Event {

    private float brightness;

    public RenderBrightnessEvent(float brightness) {
        this.brightness = brightness;
    }

    public float getBrightness() {
        return brightness;
    }

    public void setBrightness(float brightness) {
        this.brightness = brightness;
    }
}
