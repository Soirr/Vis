package me.valkyrie.vis.event.events.render;

import me.valkyrie.vis.event.Event;
import net.minecraft.entity.Entity;

/**
 * Created by Zeb on 7/28/2016.
 */
public class NametagRenderEvent extends Event {

    private Entity entity;

    public NametagRenderEvent(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }
}
