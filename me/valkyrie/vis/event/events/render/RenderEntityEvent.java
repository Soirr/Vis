package me.valkyrie.vis.event.events.render;

import me.valkyrie.vis.event.Event;
import me.valkyrie.vis.event.EventType;
import net.minecraft.entity.Entity;

/**
 * Created by Zeb on 8/14/2016.
 */
public class RenderEntityEvent extends Event {

    private Entity entity;
    private EventType type;

    public RenderEntityEvent(Entity entity, EventType type) {
        this.entity = entity;
        this.type = type;
    }

    public Entity getEntity() {
        return entity;
    }

    public EventType getType() {
        return type;
    }
}
