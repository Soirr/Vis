package me.valkyrie.vis.event.events.game;

import me.valkyrie.vis.event.Event;
import net.minecraft.network.Packet;

/**
 * Created by Zeb on 7/12/2016.
 */
public class PacketEvent extends Event {

    private Packet packet;
    private EventPacketType type;

    public PacketEvent(Packet packet, EventPacketType type) {
        this.packet = packet;
        this.type = type;
    }

    public Packet getPacket(){
        return packet;
    }

    public void setPacket(Packet packet) {
        this.packet = packet;
    }

    public EventPacketType getType() {
        return type;
    }

    public enum EventPacketType {

        SEND,
        RECEIVE

    }

}
