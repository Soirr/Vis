package me.valkyrie.vis.event.events.game;

import me.valkyrie.vis.event.Event;

/**
 * Created by Zeb on 7/12/2016.
 */
public class KeyPressEvent extends Event {

    private int key;

    public KeyPressEvent(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }
}
