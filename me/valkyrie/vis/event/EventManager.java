package me.valkyrie.vis.event;

import net.minecraft.entity.EntityLivingBase;

import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by Zeb on 7/11/2016.
 */
public class EventManager {

    private static Map<Class, List<EventMethodListener>> listenerMap = new HashMap();

    public static void subscribe(Object object){
        Class objClass = object.getClass();

        for(Method method : objClass.getDeclaredMethods()){
            if(isMethodValid(method)){
                EventMethodListener listener = generateMethodListener(object, method);

                if(!listenerMap.containsKey(method.getParameterTypes()[0])){
                    List<EventMethodListener> listeners = new ArrayList();

                    listeners.add(listener);

                    listenerMap.put(method.getParameterTypes()[0], listeners);
                }else{
                    List<EventMethodListener> listeners = listenerMap.get(method.getParameterTypes()[0]);

                    listeners.add(listener);
                }
            }
        }

        sort();
    }

    public static void unsubscribe(Object object){
        Class objClass = object.getClass();

        for(Method method : objClass.getDeclaredMethods()){
            if(isMethodValid(method)){
                Class paramClass = method.getParameterTypes()[0];

                if(listenerMap.containsKey(paramClass)){
                    for(int i = 0; i < listenerMap.get(paramClass).size(); i++){
                        EventMethodListener listener = listenerMap.get(paramClass).get(i);

                        if(listener.getParent() == object){
                            listenerMap.get(paramClass).remove(listener);
                        }
                    }
                }
            }
        }

        sort();
    }

    private static void sort(){
        for(Class clas : listenerMap.keySet()){
            List<EventMethodListener> listeners = listenerMap.get(clas);

            listeners.sort((EventMethodListener e1, EventMethodListener e2) ->{
                return e2.getPriority().getPriority() - e1.getPriority().getPriority();
            });
        }
    }

    public static Event fire(Event event){
        if(!listenerMap.containsKey(event.getClass())) return event;

        List<EventMethodListener> listeners = listenerMap.get(event.getClass());

        for(int i = 0; i < listeners.size(); i++){
            EventMethodListener listener = listeners.get(i);

            if(event.isCancelled() || event.isBroken()) break;

            try{
                listener.getMethod().setAccessible(true);
                listener.getMethod().invoke(listener.getParent(), event);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return event;
    }

    private static EventMethodListener generateMethodListener(Object parent, Method method){
        EventHandler handler = method.getDeclaredAnnotation(EventHandler.class);
        return new EventMethodListener(parent, method, handler.priority());
    }

    private static boolean isMethodValid(Method method){
        return (method.isAnnotationPresent(EventHandler.class) && method.getParameterCount() == 1);
    }

}
