package me.valkyrie.vis.event;

/**
 * Created by Zeb on 7/11/2016.
 */
public class Event {

    private boolean broken = false;
    private boolean cancelled = false;

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public void breakEvent(){
        this.broken = true;
    }

    public boolean isBroken() {
        return broken;
    }
}
