package me.valkyrie.vis.event;

/**
 * Created by Zeb on 7/11/2016.
 */
public enum EventHandlerPriority {

    LOW(0),
    STANDARD(1),
    HIGH(2);

    private int priority;

    EventHandlerPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }
}
