package me.valkyrie.vis.util.font;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;

/**
 * Created by Zeb on 8/4/2016.
 */
public class NormalFontRenderer implements FontObject {

    private FontRenderer fontRenderer;

    public NormalFontRenderer(){
        this.fontRenderer = Minecraft.getMinecraft().fontRendererObj;
    }

    @Override
    public void drawString(String text, float x, float y, int color) {
        fontRenderer.drawString(text, x, y, color);
    }

    @Override
    public void drawStringWithShadow(String text, float x, float y, int color) {
        fontRenderer.drawStringWithShadow(text, x, y, color);
    }

    @Override
    public int getStringWidth(String text) {
        return fontRenderer.getStringWidth(text);
    }

    @Override
    public int getStringHeight(String text) {
        return fontRenderer.FONT_HEIGHT;
    }
}
