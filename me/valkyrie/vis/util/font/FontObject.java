package me.valkyrie.vis.util.font;

/**
 * Created by Zeb on 8/4/2016.
 */
public interface FontObject {

    void drawString(String text, float x, float y, int color);

    void drawStringWithShadow(String text, float x, float y, int color);

    int getStringWidth(String text);

    int getStringHeight(String text);

}
