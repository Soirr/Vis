package me.valkyrie.vis.util.font;

import cfont.MinecraftFontRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;

import java.awt.*;

/**
 * Created by Zeb on 8/4/2016.
 */
public class CFontRenderer implements FontObject {

    private MinecraftFontRenderer fontRenderer;

    public CFontRenderer(Font font, boolean antiAlias, boolean fractionalMetrics){
        this.fontRenderer = new MinecraftFontRenderer(font, antiAlias, fractionalMetrics);
    }

    @Override
    public void drawString(String text, float x, float y, int color) {
        fontRenderer.drawString(text, x, y, color);
    }

    @Override
    public void drawStringWithShadow(String text, float x, float y, int color) {
        fontRenderer.drawStringWithShadow(text, x, y, color);
    }

    @Override
    public int getStringWidth(String text) {
        return fontRenderer.getStringWidth(text);
    }

    @Override
    public int getStringHeight(String text) {
        return fontRenderer.getStringHeight(text);
    }
}
