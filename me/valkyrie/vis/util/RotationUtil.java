package me.valkyrie.vis.util;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntitySnowball;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;

/**
 * Created by Zeb on 7/15/2016.
 */
public class RotationUtil {

    public static float[] getRotations(Entity entity){
        Vec3 pos = new Vec3(entity.posX, entity.posY + entity.height/2, entity.posZ);

        return getRotations(pos);
    }

    public static float[] getRotations(Vec3 pos) {
        Minecraft mc = Minecraft.getMinecraft();

        double diffX = pos.xCoord - mc.thePlayer.posX;
        double diffZ = pos.zCoord - mc.thePlayer.posZ;
        double diffY = (pos.yCoord - 1) - mc.thePlayer.posY;
        double hdistance = (double)MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
        float yaw = (float)(Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float)(-(Math.atan2(diffY, hdistance) * 180.0D / Math.PI));
        return new float[]{yaw,pitch};
    }

    public static float getYawChangeToEntity(Entity entity){
        Minecraft mc = Minecraft.getMinecraft();
        double deltaX = entity.posX - mc.thePlayer.posX;
        double deltaZ = entity.posZ - mc.thePlayer.posZ;
        double yawToEntity;
        if((deltaZ < 0.0D) && (deltaX < 0.0D)){
            yawToEntity = 90.0D + Math.toDegrees(Math.atan(deltaZ / deltaX));
        }else{
            if((deltaZ < 0.0D) && (deltaX > 0.0D)){
                yawToEntity = -90.0D
                        + Math.toDegrees(Math.atan(deltaZ / deltaX));
            }else{
                yawToEntity = Math.toDegrees(-Math.atan(deltaX / deltaZ));
            }
        }

        return MathHelper
                .wrapAngleTo180_float(-(mc.thePlayer.rotationYaw - (float) yawToEntity));
    }

    public static float getPitchChangeToEntity(Entity entity){
        Minecraft mc = Minecraft.getMinecraft();
        double deltaX = entity.posX - mc.thePlayer.posX;
        double deltaZ = entity.posZ - mc.thePlayer.posZ;
        double deltaY = entity.posY - 1.6D + entity.getEyeHeight() - 0.24
                - mc.thePlayer.posY;
        double distanceXZ = MathHelper.sqrt_double(deltaX * deltaX + deltaZ
                * deltaZ);

        double pitchToEntity = -Math.toDegrees(Math.atan(deltaY / distanceXZ));

        return -MathHelper.wrapAngleTo180_float(mc.thePlayer.rotationPitch
                - (float) pitchToEntity);
    }

}
