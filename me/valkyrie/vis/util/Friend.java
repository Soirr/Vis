package me.valkyrie.vis.util;

/**
 * Created by Zeb on 7/27/2016.
 */
public class Friend {

    private String name;
    private String nick;

    public Friend(String name, String nick) {
        this.name = name;
        this.nick = nick;
    }

    public String getName() {
        return name;
    }

    public String getNick() {
        return nick;
    }

}
