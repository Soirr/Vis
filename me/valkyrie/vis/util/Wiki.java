package me.valkyrie.vis.util;

import java.net.URL;

/**
 * Created by Zeb on 8/12/2016.
 */
public @interface Wiki {

    String link();

}
