package me.valkyrie.vis.util;

import org.lwjgl.Sys;

/**
 * Created by Zeb on 7/24/2016.
 */
public class TimerUtil {

    private long time = System.currentTimeMillis();

    public boolean hasReached(long time){
        return System.currentTimeMillis() - this.time >= time;
    }

    public long getTime(){
        return this.time;
    }

    public void reset(){
        time = System.currentTimeMillis();
    }

}
