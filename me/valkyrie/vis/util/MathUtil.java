package me.valkyrie.vis.util;

import java.util.Random;

/**
 * Created by Zeb on 7/20/2016.
 */
public class MathUtil {

    public static double roundOff(double x, int position)
    {
        double a = x;
        double temp = Math.pow(10.0, position);
        a *= temp;
        a = Math.round(a);
        return (a / temp);
    }

    public static float getAngleDifference(float alpha, float beta) {
        float phi = Math.abs(beta - alpha) % 360;       // This is either the DistanceCheck or 360 - DistanceCheck
        float distance = phi > 180 ? 360 - phi : phi;
        return distance;
    }

    public static int getRandomInRange(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static double getRandomInRange(double min, double max) {
        Random random = new Random();
        double range = max - min;
        double scaled = random.nextDouble() * range;
        double shifted = scaled + min;
        return shifted;
    }

    public static float getRandomInRange(float min, float max) {
        Random random = new Random();
        float range = max - min;
        float scaled = random.nextFloat() * range;
        float shifted = scaled + min;
        return shifted;
    }

}
