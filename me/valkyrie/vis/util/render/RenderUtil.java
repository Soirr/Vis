package me.valkyrie.vis.util.render;

import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;

import java.awt.*;

/**
 * Created by Zeb on 7/28/2016.
 */
public class RenderUtil {

    public static void setColor(Color color){
        GlStateManager.color(color.getRed()/255f, color.getGreen()/255f, color.getBlue()/255f, color.getAlpha()/255f);
    }

    public static void drawRect(float x, float y, float x1, float y1, Color color){
        Gui.drawRect(x,y,x1,y1,color.getRGB());
    }

}
