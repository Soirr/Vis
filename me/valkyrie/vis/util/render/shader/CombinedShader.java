package me.valkyrie.vis.util.render.shader;

import me.valkyrie.vis.util.render.*;
import org.lwjgl.opengl.GL20;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Zeb on 8/13/2016.
 */
public class CombinedShader extends Shader {

    private int vertexShaderID;
    private int fragmentShaderID;

    public CombinedShader(String vertex, String fragment) {
        super(vertex + ", " + fragment);
        try {
            vertexShaderID = ShaderUtil.createShaderFromName(vertex, ShaderType.VERTEX);
            fragmentShaderID = ShaderUtil.createShaderFromName(fragment, ShaderType.FRAGMENT);
            programId = GL20.glCreateProgram();
            GL20.glAttachShader(programId, vertexShaderID);
            GL20.glAttachShader(programId, fragmentShaderID);
            GL20.glLinkProgram(programId);
            GL20.glValidateProgram(programId);
        }catch (Exception e){}
    }

}
