package me.valkyrie.vis.util.render.shader;

import me.valkyrie.vis.util.render.ShaderUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.io.InputStream;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Zeb on 8/13/2016.
 */
public class Shader {

    protected String name;
    private ShaderType type;
    protected int programId;

    private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);

    public Shader(String name, ShaderType type) {
        this.name = name;
        this.type = type;

        InputStream inputStream = getClass().getResourceAsStream("/shaders/" + name + (type == ShaderType.FRAGMENT ? ".frag" : ".vert"));

        Scanner scanner = new Scanner(inputStream);

        String code = "";

        while (scanner.hasNextLine()){
            code += scanner.nextLine() + "\n";
        }

        try {
            int shader = ShaderUtil.createShaderFromName(name, type);
            programId = GL20.glCreateProgram();
            GL20.glAttachShader(programId, shader);
            GL20.glLinkProgram(programId);
            GL20.glValidateProgram(programId);
        }catch (Exception e){
            this.programId = 0;
            System.out.println("Unable to create shader " + name + ".");
            e.printStackTrace();
        }
    }


    public Shader(String name) {
        this.name = name;
    }

    public void bind(){
        if(programId == 0){
            System.out.println("Unable to bind shader " + name + ".");
            return;
        }

        GL20.glUseProgram(programId);
    }

    public void unbind(){
        GL20.glUseProgram(0);
    }

    public void bindAttribute(int attribute, String variableName) {
        GL20.glBindAttribLocation(this.programId, attribute, variableName);
    }

    public int getUniform(String uniformName) {
        return GL20.glGetUniformLocation(this.programId, uniformName);
    }

    public void setFloat(int location, float value) {
        GL20.glUniform1f(location, value);
    }

    protected void setInt(int location, int value) {
        GL20.glUniform1i(location, value);
    }

    public void setVector3f(int location, Vector3f vector) {
        GL20.glUniform3f(location, vector.x, vector.y, vector.z);
    }

    public void setVector4f(int location, Vector4f vector) {
        GL20.glUniform4f(location, vector.x, vector.y, vector.z, vector.w);
    }

    public void setVector2f(int location, Vector2f vector) {
        GL20.glUniform2f(location, vector.x, vector.y);
    }

    public void setBoolean(int location, boolean value) {
        float toLoad = 0;
        if (value) {
            toLoad = 1;
        }
        GL20.glUniform1f(location, toLoad);
    }

    protected void setMatrix4f(int location, Matrix4f matrix) {
        matrix.store(matrixBuffer);
        matrixBuffer.flip();
        GL20.glUniformMatrix4(location, false, matrixBuffer);
    }
}
