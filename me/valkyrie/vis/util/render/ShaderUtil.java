package me.valkyrie.vis.util.render;

import me.valkyrie.vis.util.render.shader.ShaderType;
import org.lwjgl.opengl.*;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by Zeb on 5/22/2016.
 */
public class ShaderUtil {

    public static void bindShader(int shader){
        int program = 0;

        program = ARBShaderObjects.glCreateProgramObjectARB();

        if(program == 0)
            return;

        /*
        * if the vertex and fragment shaders setup sucessfully,
        * attach them to the shader program, link the sahder program
        * (into the GL context I suppose), and validate
        */
        ARBShaderObjects.glAttachObjectARB(program, shader);

        ARBShaderObjects.glLinkProgramARB(program);
        if (ARBShaderObjects.glGetObjectParameteriARB(program, ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL11.GL_FALSE) {
            System.err.println(getLogInfo(program));
            return;
        }

        ARBShaderObjects.glValidateProgramARB(program);
        if (ARBShaderObjects.glGetObjectParameteriARB(program, ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL11.GL_FALSE) {
            System.err.println(getLogInfo(program));
            return;
        }

        ARBShaderObjects.glUseProgramObjectARB(program);

    }

    public static void unbindShader(){
        ARBShaderObjects.glUseProgramObjectARB(0);
    }

    public static int createShader(String shaderCode, ShaderType shaderType) throws Exception {
        int shader = 0;
        try {

            shader = GL20.glCreateShader(shaderType.getTypeId());
            GL20.glShaderSource(shader, shaderCode);
            GL20.glCompileShader(shader);
            if (GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
                System.out.println(GL20.glGetShaderInfoLog(shader, 500));
                System.err.println("Could not compile shader!");
                throw new RuntimeException("Error creating shader: " + getLogInfo(shader));
            }
            return shader;
        }
        catch(Exception exc) {
            ARBShaderObjects.glDeleteObjectARB(shader);
            throw exc;
        }
    }

    public static int createShaderFromName(String name, ShaderType shaderType) throws Exception {
        InputStream inputStream = Object.class.getResourceAsStream("/shaders/" + name + (shaderType == ShaderType.FRAGMENT ? ".frag" : ".vertex"));

        Scanner scanner = new Scanner(inputStream);

        String code = "";

        while (scanner.hasNextLine()){
            code += scanner.nextLine() + "\n";
        }

        int shader = 0;
        try {

            shader = GL20.glCreateShader(shaderType.getTypeId());
            GL20.glShaderSource(shader, code);
            GL20.glCompileShader(shader);
            if (GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
                System.out.println(GL20.glGetShaderInfoLog(shader, 500));
                System.err.println("Could not compile shader!");
                throw new RuntimeException("Error creating shader: " + getLogInfo(shader));
            }

            return shader;
        }
        catch(Exception exc) {
            ARBShaderObjects.glDeleteObjectARB(shader);
            throw exc;
        }
    }

    public static String getLogInfo(int obj) {
        return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
    }

}
