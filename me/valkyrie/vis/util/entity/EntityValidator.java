package me.valkyrie.vis.util.entity;

import net.minecraft.entity.EntityLivingBase;

/**
 * Created by Zeb on 7/24/2016.
 */
public interface EntityValidator {

    boolean isValid(EntityLivingBase entity);

}
