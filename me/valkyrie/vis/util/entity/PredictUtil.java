package me.valkyrie.vis.util.entity;

import net.minecraft.entity.Entity;
import net.minecraft.util.Vec3;

/**
 * Created by Zeb on 7/20/2016.
 */
public class PredictUtil {

    private static Vec3 lerp(Vec3 pos, Vec3 prev, float time){
        double x = pos.xCoord + ((pos.xCoord - prev.xCoord) * time);
        double y = pos.yCoord + ((pos.yCoord - prev.yCoord) * time);
        double z = pos.zCoord + ((pos.zCoord - prev.zCoord) * time);

        return new Vec3(x,y,z);
    }

    public static Vec3 predictPos(Entity entity, float time){
        return lerp(new Vec3(entity.posX, entity.posY, entity.posZ), new Vec3(entity.prevPosX, entity.prevPosY, entity.prevPosZ), time);
    }

}
