package me.valkyrie.vis.util.entity;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 7/24/2016.
 */
public class EntityUtil {

    public static List<EntityLivingBase> getLivingEntities(){
        List<EntityLivingBase> entites = new ArrayList();

        for(Object object : Minecraft.getMinecraft().theWorld.loadedEntityList){
            if(object instanceof EntityLivingBase){
                EntityLivingBase entity = (EntityLivingBase) object;

                if(entity.isEntityAlive() && entity != Minecraft.getMinecraft().thePlayer){
                    entites.add(entity);
                }
            }
        }

        return entites;
    }

    public static List<EntityLivingBase> getLivingEntitiesInRange(double range){
        List<EntityLivingBase> entites = new ArrayList();

        for(Object object : Minecraft.getMinecraft().theWorld.loadedEntityList){
            if(object instanceof EntityLivingBase){
                EntityLivingBase entity = (EntityLivingBase) object;

                if(entity.isEntityAlive() && entity != Minecraft.getMinecraft().thePlayer && entity.getDistanceToEntity(Minecraft.getMinecraft().thePlayer) <= range){
                    entites.add(entity);
                }
            }
        }

        return entites;
    }

    public static List<EntityLivingBase> getLivingEntitiesInRange(double range, EntityValidator entityValidator){
        List<EntityLivingBase> entites = new ArrayList();

        for(Object object : Minecraft.getMinecraft().theWorld.loadedEntityList){
            if(object instanceof EntityLivingBase){
                EntityLivingBase entity = (EntityLivingBase) object;

                if(entity.isEntityAlive() && entityValidator.isValid(entity) && entity != Minecraft.getMinecraft().thePlayer && entity.getDistanceToEntity(Minecraft.getMinecraft().thePlayer) <= range){
                    entites.add(entity);
                }
            }
        }

        return entites;
    }

    public static EntityLivingBase getClosestEntity(double range){
        EntityLivingBase entityLivingBase = null;

        double distance = range;

        for(Object object : Minecraft.getMinecraft().theWorld.loadedEntityList){
            if(object instanceof EntityLivingBase){
                EntityLivingBase entity = (EntityLivingBase) object;

                if(entity.isEntityAlive() && entity != Minecraft.getMinecraft().thePlayer && entity.getDistanceToEntity(Minecraft.getMinecraft().thePlayer) <= distance){
                    distance = entity.getDistanceToEntity(Minecraft.getMinecraft().thePlayer);
                    entityLivingBase = entity;
                }
            }
        }

        return entityLivingBase;
    }

    public static EntityLivingBase getClosestEntity(double range, EntityValidator entityValidator){
        EntityLivingBase entityLivingBase = null;

        double distance = range;

        for(Object object : Minecraft.getMinecraft().theWorld.loadedEntityList){
            if(object instanceof EntityLivingBase){
                EntityLivingBase entity = (EntityLivingBase) object;

                if(entity.isEntityAlive() && entity != Minecraft.getMinecraft().thePlayer && entity.getDistanceToEntity(Minecraft.getMinecraft().thePlayer) <= distance && entityValidator.isValid(entity)){
                    distance = entity.getDistanceToEntity(Minecraft.getMinecraft().thePlayer);
                    entityLivingBase = entity;
                }
            }
        }

        return entityLivingBase;
    }

}
