package me.valkyrie.vis.util;

/**
 * Created by Zeb on 7/27/2016.
 */
public enum AntiCheat {

    NCP,
    AAC,
    WATCHDOG,
    ALL,
    NONE

}
