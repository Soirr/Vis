package me.valkyrie.vis;

import com.mojang.authlib.Agent;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;
import me.tojatta.api.value.impl.annotations.BooleanValue;
import me.tojatta.api.value.impl.annotations.StringValue;
import me.valkyrie.vis.command.Command;
import me.valkyrie.vis.command.CommandBuilder;
import me.valkyrie.vis.event.EventManager;
import me.valkyrie.vis.gui.IngameHud;
import me.valkyrie.vis.gui.clickgui.ClickGui;
import me.valkyrie.vis.gui.tabgui.TabGui;
import me.valkyrie.vis.gui.hud.vis.tab.VisTheme;
import me.valkyrie.vis.manager.Managers;
import me.valkyrie.vis.util.chat.ChatBuilder;
import me.valkyrie.vis.util.chat.ChatColor;
import net.minecraft.client.Minecraft;
import net.minecraft.util.Session;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;

import java.io.File;
import java.net.Proxy;

/**
 * Created by Zeb on 7/11/2016.
 */
public class Vis {

    private static Vis vis;

    public static String rel = "1.8.4";

    @BooleanValue(label = "tabVisible")
    public static boolean tabVisible = true;

    @BooleanValue(label = "modInfo")
    public static boolean modInfo = true;

    @StringValue(label = "hudTheme")
    public static String hudTheme = "Vis";

    public static TabGui tabGui;
    public static ClickGui clickGui;

    public static File dir;

    public Vis() {
        dir = new File(Minecraft.getMinecraft().mcDataDir + "/Vis");

        if (!dir.exists()) dir.mkdir();

        Managers.init();
        Managers.getValueManager().getBaseValueManager().register(this);

        Managers.getValueManager().load();
        Managers.getValueManager().save();
        Managers.getInfoManager().load();
        Managers.getInfoManager().save();
        Managers.getFriendManager().load();
        Managers.getFriendManager().save();

        tabGui = new TabGui(new VisTheme());

        Display.setTitle("Vis r" + rel + " (1.8)");
        new IngameHud(hudTheme);

        EventManager.subscribe(this);

        new CommandBuilder("Help")
                .withUsages(new String[]{})
                .withAliases(new String[]{"h"})
                .withCommandExecutor(arguments -> {

                    new ChatBuilder().appendPrefix().appendText("Commands : ").send();

                    for(Command command : Managers.getCommandManager().getContent()){
                        new ChatBuilder().appendText("  - ", ChatColor.GRAY).appendText(command.getName()).send();
                    }

                    return "no_message";
                }).build();

        System.out.println("Launched Vis.");

        clickGui = new ClickGui();
    }

    public static void start(){
        vis = new Vis();
    }

    public static Vis getVis() {
        return vis;
    }

    public static void login(String name, String password){
        YggdrasilAuthenticationService authenticationService = new YggdrasilAuthenticationService(
                Proxy.NO_PROXY, "");
        YggdrasilUserAuthentication authentication = (YggdrasilUserAuthentication) authenticationService
                .createUserAuthentication(Agent.MINECRAFT);
        authentication.setUsername(name);
        authentication.setPassword(password);

        try{
            authentication.logIn();
            Minecraft.getMinecraft().session = new Session(authentication
                    .getSelectedProfile().getName(), authentication
                    .getSelectedProfile().getId().toString(),
                    authentication.getAuthenticatedToken(), "mojang");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
