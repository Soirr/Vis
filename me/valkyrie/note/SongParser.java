package me.valkyrie.note;

import me.valkyrie.note.types.NoteSong;

import java.io.File;

/**
 * Created by Zeb on 7/11/2016.
 */
public interface SongParser {

    NoteSong parse(File file) throws Exception;

}
