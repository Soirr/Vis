package me.valkyrie.note.exceptions;

/**
 * Created by Zeb on 7/11/2016.
 */
public class InvalidNoteException extends Exception {

    public InvalidNoteException(int line) {
        super("Song could note get note. Error in song at line " + line + ".");
    }
}
