package me.valkyrie.note.exceptions;

/**
 * Created by Zeb on 7/11/2016.
 */
public class InvalidNoteSongException extends Exception {

    public InvalidNoteSongException(int line) {
        super("Song could note parse song. Invalid syntax at line " + line + ".");
    }
}
