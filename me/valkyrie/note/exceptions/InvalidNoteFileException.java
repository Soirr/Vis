package me.valkyrie.note.exceptions;

/**
 * Created by Zeb on 7/11/2016.
 */
public class InvalidNoteFileException extends Exception {

    public InvalidNoteFileException(String name) {
        super("Song could use the following note file : " + name + ".");
    }
}
