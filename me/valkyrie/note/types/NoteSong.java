package me.valkyrie.note.types;

import me.valkyrie.note.exceptions.InvalidNoteException;
import net.minecraft.util.MathHelper;

/**
 * Created by Zeb on 7/11/2016.
 */
public class NoteSong {

    private String title;
    private String[] raw;

    private int duration = 0;

    public NoteSong(String title, String[] raw) {
        this.title = title;
        this.raw = raw;
    }

    public int[] currentNote() throws InvalidNoteException {
        String line = raw[duration];

        String[] lineParts = line.split(":");

        try {
            if (lineParts[0].equals("NOTE")) {
                int pitch = Integer.parseInt(lineParts[1]);

                pitch = MathHelper.clamp_int(pitch, 0, 24);

                return new int[]{0,pitch};
            } else {
                throw new InvalidNoteException(34);
            }
        }catch(Exception e){
            throw new InvalidNoteException(37);
        }
    }

    public boolean isRest(){
        return (raw[duration].equals("REST"));
    }

    public int getLength(){
        return raw.length;
    }

    public void next(){
        duration++;
        if(duration > raw.length-1) duration = 0;
    }

    public String[] getRaw() {
        return raw;
    }

    public int getDuration() {
        return duration;
    }
}
