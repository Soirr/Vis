package me.valkyrie.note.types;

import me.valkyrie.note.exceptions.InvalidNoteException;
import net.minecraft.util.MathHelper;

/**
 * Created by Zeb on 7/11/2016.
 */
public class LayeredNoteSong extends NoteSong {

    public LayeredNoteSong(String title, String[] raw) {
        super(title, raw);
    }

    public int[] currentNote() throws InvalidNoteException {
        String line = getRaw()[getDuration()];

        String[] lineParts = line.split(":");

        try {
            if (lineParts[0].equals("NOTE")) {
                String[] data = lineParts[1].split("-");

                if(data.length != 2) throw new InvalidNoteException(getDuration());

                int instrument = MathHelper.clamp_int(Integer.parseInt(data[0]), 0, 4);
                int pitch = MathHelper.clamp_int(Integer.parseInt(data[1]), 0, 24);

                pitch = MathHelper.clamp_int(pitch, 0, 24);

                return new int[]{instrument,pitch};
            } else {
                throw new InvalidNoteException(getDuration());
            }
        }catch(Exception e){
           e.printStackTrace();
           throw new InvalidNoteException(getDuration());
        }
    }

}
