package me.valkyrie.note.util;

import net.minecraft.util.BlockPos;

/**
 * Created by Zeb on 7/15/2016.
 */
public class Noteblock {

    private BlockPos pos;
    private int layer;
    private int pitch;

    public Noteblock(BlockPos pos, int layer, int pitch) {
        this.pos = pos;
        this.layer = layer;
        this.pitch = pitch;
    }

    public Noteblock(BlockPos pos, int pitch) {
        this.pos = pos;
        this.pitch = pitch;
        this.layer = 0;
    }

    public Noteblock(BlockPos pos) {
        this.pos = pos;
        this.pitch = 0;
    }

    public void setPitch(int pitch) {
        this.pitch = pitch;
    }

    public BlockPos getPos() {
        return pos;
    }

    public int getPitch() {
        return pitch;
    }

    public int getLayer() {
        return layer;
    }
}
