package me.valkyrie.note.parsers;

import me.valkyrie.note.types.NoteSong;
import me.valkyrie.note.SongParser;
import me.valkyrie.note.exceptions.InvalidNoteFileException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Zeb on 7/15/2016.
 */
public class NoteFileParser implements SongParser {

    @Override
    public NoteSong parse(File file) throws Exception {
        List<String> lines = new ArrayList();

        Scanner scanner = new Scanner(file);

        while(scanner.hasNextLine()){
            String line = scanner.nextLine();

            if(line.replaceAll(" ", "").equalsIgnoreCase("rest")){
                lines.add("REST");
            }

            if(line.contains(" ")){
                String[] parts = line.split("\\s+");

                if(parts.length == 0) throw new InvalidNoteFileException(file.getName());

                if(parts[0].equalsIgnoreCase("note")){
                    if(parts[1].contains("{") && parts[1].contains("}")){
                        String[] notes = parts[1].replaceAll("}", "").replaceAll("\\{", "").split(",");

                        for(String note : notes){
                            int pitch = Integer.parseInt(note);
                            lines.add("NOTE:" + pitch);
                        }
                    }else{
                        int pitch = Integer.parseInt(parts[1]);
                        lines.add("NOTE:" + pitch);
                    }
                }else if(parts[0].equalsIgnoreCase("rest")){
                    int times = Integer.parseInt(parts[1]);
                    for(int i = 0; i < times; i++){
                        lines.add("REST");
                    }
                }
            }
        }

        return new NoteSong("Test", lines.toArray(new String[lines.size()]));
    }
}
