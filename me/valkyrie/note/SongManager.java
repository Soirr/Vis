package me.valkyrie.note;

import com.google.common.io.Files;
import me.valkyrie.note.exceptions.InvalidNoteFileException;
import me.valkyrie.note.parsers.NoteBotFileParser;
import me.valkyrie.note.parsers.NoteFileParser;
import me.valkyrie.note.types.NoteSong;

import java.io.File;
import java.util.*;

/**
 * Created by Zeb on 7/11/2016.
 */
public class SongManager {

    public SongManager() {
        parsers.put(SongType.NOTE, new NoteFileParser());
        parsers.put(SongType.NOTEBOT, new NoteBotFileParser());
    }

    //Just the parsers
    private Map<SongType, SongParser> parsers = new HashMap();

    //Finds the correct parser to parse the file.
    public NoteSong parse(File file) throws InvalidNoteFileException {
        if(!file.getName().contains(".")){
            throw new InvalidNoteFileException(file.getName());
        }

        String ext = Files.getFileExtension(file.getAbsolutePath());

        for(SongType songType : parsers.keySet()){
            if(ext.equals(songType.getExtension())){
                try {
                    return parsers.get(songType).parse(file);
                }catch (Exception e){
                    e.printStackTrace();
                    throw new InvalidNoteFileException(file.getName());
                }
            }
        }

        return null;
    }

}
