package me.valkyrie.note;

/**
 * Created by Zeb on 7/11/2016.
 */
public enum SongType {

    GROOVY("groovy"), //Format that all serenity based players use, Most common.
    NOTE_BLOCK_STUDIO("nbs"), //Common music making program for Minecraft.
    NOTE("note"), //Format this library uses.
    NOTEBOT("notebot"); //Format will all 5 instruments.

    private String extension;

    SongType(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }

}
