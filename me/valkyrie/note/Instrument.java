package me.valkyrie.note;

import net.minecraft.block.material.Material;

/**
 * Created by Zeb on 8/18/2016.
 */
public enum Instrument {

    PIANO,
    BASS_DRUM,
    SNARE_DRUM,
    CLICK,
    BASS

}
